/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GraveViewTest;

import graves.controller.GravesController;
import graves.daos.DAOManager;
import graves.view.GraveListView;
import headquarters.controller.HeadquartersController;
import headquarters.model.Model;
import java.awt.event.ActionEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
/**
 *
 * @author jiri
 */
public class GraveViewTest {
    
    public GraveViewTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void getNameTest()
    {
        GraveListView g = new GraveListView(new GravesController(new HeadquartersController(new Model())), DAOManager.getInstance());
        g.update();
        assertEquals(g.getViewName(), "Graves/GraveList");
    }
    
    @Test
    public void updateTest()
    {
        GraveListView g = new GraveListView(new GravesController(new HeadquartersController(new Model())), DAOManager.getInstance());
        
        //assertEquals(g.getViewName(), "Graves/GraveList");
       // verify(g).dataModel.fireTableDataChanged();
    }
}