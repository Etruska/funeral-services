/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GraveViewTest;

import graves.controller.GravesController;
import graves.daos.DAOManager;
import graves.view.CoffinsView;
import headquarters.controller.HeadquartersController;
import headquarters.model.Model;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jiri
 */
public class CoffinsViewTest {
    
    public CoffinsViewTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void getNameTest()
    {
        CoffinsView t = new CoffinsView(new GravesController(new HeadquartersController(new Model())), DAOManager.getInstance());
        t.update();
        assertEquals(t.getViewName(), "Graves/Coffins");
    }
}