/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesModelTest;

import graves.model.Coffin;
import graves.model.Grave;
import graves.model.Graveyard;
import graves.model.Material;
import graves.model.Storeroom;
import graves.model.Tombstone;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jiri
 */
public class GraveTest {
    
    public GraveTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void gettersTest()
    {
        Material m = new Material(1, "drevo");
        Storeroom s = new Storeroom(1, "sklad", "v dole", 50);
        Tombstone t = new Tombstone(1, m, "zemrel", s);
        Coffin c = new Coffin(1, false, 200, 50, m, null, s);
        Graveyard gd = new Graveyard(1, "hrbitov splnenych prani", "terronska 8", 200);
        Grave g = new Grave(1, t, c, gd);
        
        
        assertEquals(g.getGraveyard().getName(), "hrbitov splnenych prani");
        assertEquals(g.getTombstone().getText(), "zemrel");
        assertEquals(g.getCoffin().toString(), "drevo200x50");
    }
}