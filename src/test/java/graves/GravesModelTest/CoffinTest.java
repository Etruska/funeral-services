/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesModelTest;

import graves.model.Coffin;
import graves.model.Material;
import graves.model.Storeroom;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jiri
 */
public class CoffinTest {
    
    public CoffinTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void getWidthTest()
    {
        Coffin c = new Coffin(1, false, 190, 50, new Material(3,"drevo"), null, new Storeroom(null, "sklad", "terronska 9", 50));
        assertEquals(c.getHeight(), new Integer(190));
    }
    
    @Test
    public void getHeightTest()
    {
        Coffin c = new Coffin(1, false, 190, 50, new Material(3,"drevo"), null, new Storeroom(null, "sklad", "terronska 9", 50));
        assertEquals(c.getWidth(), new Integer(50));
    }
    
    @Test
    public void getStoreroomTest()
    {
        Coffin c = new Coffin(1, false, 190, 50, new Material(3,"drevo"), null, new Storeroom(null, "sklad", "terronska 9", 50));
        assertEquals(c.getStoreroom().getName(), "sklad");
    }
    
    
    
    @Test
    public void getStateTest()
    {
        Coffin c = new Coffin(1, false, 190, 50, new Material(3,"drevo"), null, new Storeroom(null, "sklad", "terronska 9", 50));
        assertEquals(c.getState(), false);
    }
    
    @Test
    public void getMaterialTest()
    {
        Coffin c = new Coffin(1, false, 190, 50, new Material(3,"drevo"), null, new Storeroom(null, "sklad", "terronska 9", 50));
        assertEquals(c.getMaterial().getName(), "drevo");
    }
    
    @Test
    public void getStoreroomIdTest()
    {
        Coffin c = new Coffin(1, false, 190, 50, new Material(3,"drevo"), null, new Storeroom(5, "sklad", "terronska 9", 50));
        assertEquals(c.getStoreroomId(), new Integer(5));
        c = new Coffin(1, false, 190, 50, new Material(3,"drevo"), null, null);
        assertEquals(c.getStoreroomId(), null);   
    }
    
    @Test
    public void toStringTest()
    {
        Coffin c = new Coffin(1, false, 190, 50, new Material(3,"drevo"), null, new Storeroom(5, "sklad", "terronska 9", 50));
        assertEquals(c.toString(), "drevo190x50");
    }
    
    
   
}