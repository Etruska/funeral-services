/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesModelTest;


import graves.model.Property;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jiri
 */
public class PropertyTest {
    
    public PropertyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void toStringTest()
    {
        Property p = new Property(1, "nemovitost", "terronska 9", 50);
        assertEquals(p.toString(), "nemovitost");
    }
    
    @Test
    public void getCapacityTest()
    {
        Property p = new Property(1, "nemovitost", "terronska 9", 50);
        assertEquals(p.getCapacity(), new Integer(50));
    }
    
    @Test
    public void getAddressTest()
    {
        Property p = new Property(1, "nemovitost", "terronska 9", 50);
        assertEquals(p.getAddress(), "terronska 9");
    }
}