/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesModelTest;

import graves.model.Material;
import graves.model.Storeroom;
import graves.model.Tombstone;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jiri
 */
public class TombstoneTest {
    
    public TombstoneTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
     @Test
    public void toStringTest()
    {
        Tombstone t = new Tombstone(1, new Material(1, "drevo"), "zemrel statecne", new Storeroom(1, "sklad", "v dole", 50));
        assertEquals(t.toString(), "zemrel statecne");
    }
     
     
     @Test
    public void gettersTest()
    {
        Tombstone t = new Tombstone(1, new Material(1, "drevo"), "zemrel statecne", new Storeroom(1, "sklad", "v dole", 50));
        assertEquals(t.getText(), "zemrel statecne");
        assertEquals(t.getMaterial().getName(), "drevo");
        assertEquals(t.getStoreroom().getName(), "sklad");
        assertEquals(t.getStoreroomId(), new Integer(1));
        t = new Tombstone(1, new Material(1, "drevo"), "zemrel statecne", null);
        assertEquals(t.getStoreroomId(), null);
    }
     

}