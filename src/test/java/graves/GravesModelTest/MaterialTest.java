/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesModelTest;

import graves.model.Coffin;
import graves.model.Material;
import graves.model.Storeroom;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jiri
 */
public class MaterialTest {
    
    public MaterialTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void toStringTest()
    {
        Material m = new Material(1, "drevo");
        assertEquals(m.toString(), "drevo");
    }
}