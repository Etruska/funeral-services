/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesDAOsTest;

import graves.daos.DAOManager;
import graves.view.IObserver;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author jiri
 */
public class DAOManagerTest {
    
    public DAOManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void notifyObserversTest()
    {
        DAOManager dao = DAOManager.getInstance();
        IObserver o = mock(IObserver.class);
        dao.registerObserver(o);
        dao.notifyObservers();
        verify(o).update();
    }
}