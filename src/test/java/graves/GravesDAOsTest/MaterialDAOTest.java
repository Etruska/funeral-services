/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesDAOsTest;

import graves.daos.DAOManager;
import graves.daos.MaterialDAO;
import graves.model.Material;
import java.lang.String;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.sql.RowSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author jiri
 */
public class MaterialDAOTest {
    
    public MaterialDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void findMaterialITest()
    {
        
        Connection c = mock(Connection.class);
        Statement s = mock(Statement.class);
        ResultSet r = mock(ResultSet.class);
        MaterialDAO m = new MaterialDAO(c, new DAOManager());
        try
        {
            when(c.createStatement()).thenReturn(s);
            when(s.executeQuery("SELECT MATERIAL.* FROM MATERIAL WHERE MATERIAL.MATERIALID = 1")).thenReturn(r);
            when(r.getInt("materialid")).thenReturn(1);
            when(r.getString("name")).thenReturn("drevo");
            
            assertEquals(m.findMaterial(1).getName(), "drevo");
        }
        catch (Exception e)
        {
            
        }
        
    }
    
    @Test
    public void findAllTest()
    {
        try
        {
        ArrayList<Material> list = new ArrayList<Material>();
        list.add(new Material(1, "drevo"));
        
        Connection c = mock(Connection.class);
        Statement s = mock(Statement.class);
        ResultSet r = mock(ResultSet.class);
        
        MaterialDAO m = new MaterialDAO(c, new DAOManager());
        when(r.next()).thenReturn(true).thenReturn(false);
        when(r.getInt("id")).thenReturn(1);
        when(r.getString("name")).thenReturn("drevo");
        
        assertEquals(m.findAll().size(), list.size());
        assertEquals(m.findAll().get(1).toString(), list.get(1).toString());
        }
        catch (Exception e)
        {
            
        }
    }
    
}