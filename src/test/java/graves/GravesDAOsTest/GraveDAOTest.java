/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesDAOsTest;


import graves.daos.DAOManager;
import graves.daos.GraveDAO;
import graves.model.Grave;
import graves.model.Graveyard;
import graves.model.Material;
import graves.model.Storeroom;
import graves.model.Tombstone;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author jiri
 */
public class GraveDAOTest {
    
    public GraveDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void findGraveITest()
    {
        
        Connection c = mock(Connection.class);
        Statement s = mock(Statement.class);
        ResultSet r = mock(ResultSet.class);
        GraveDAO m = new GraveDAO(c, new DAOManager());
        try
        {
            when(c.createStatement()).thenReturn(s);
            when(s.executeQuery("SELECT GRAVE.* FROM GRAVE WHERE GRAVE.GRAVEID = 1")).thenReturn(r);
            when(r.getInt("graveid")).thenReturn(1);
            
            assertEquals(m.findGrave(1).getId(), new Integer(1));
        }
        catch (Exception e)
        {
            
        }
        
    }
    
    
    @Test
    public void findAllTest()
    {
        try
        {
        ArrayList<Grave> list = new ArrayList<Grave>();
        Material mat = new Material(1, "voda");
        Storeroom sr = new Storeroom(1, "sklad", "nekde", 52220);
        Tombstone tomb = new Tombstone(1, mat, "ahoj", sr);
        Graveyard grav = new Graveyard(1, "hrbitov", "nekde jinde", 5500);
        list.add(new Grave(1, tomb,  null, grav));
        
        Connection c = mock(Connection.class);
        Statement s = mock(Statement.class);
        ResultSet r = mock(ResultSet.class);
        
        GraveDAO m = new GraveDAO(c, new DAOManager());
        when(r.next()).thenReturn(true).thenReturn(false);
        when(r.getInt("graveid")).thenReturn(1);
        
        assertEquals(m.findAll().size(), list.size());
        assertEquals(m.findAll().get(1).getId(), list.get(1).getId());
        }
        catch (Exception e)
        {
            
        }
    }
}