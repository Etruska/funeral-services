/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesDAOsTest;

import graves.daos.CoffinDAO;
import graves.daos.DAOManager;
import graves.daos.TombstoneDAO;
import graves.model.Coffin;
import graves.model.Material;
import graves.model.Tombstone;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author jiri
 */
public class CoffinDAOTest {
    
    public CoffinDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void findAllTest()
    {
        try
        {
        ArrayList<Coffin> list = new ArrayList<Coffin>();
        list.add(new Coffin(1, false, 200, 70, new Material(1, "drevo"), null, null));
        
        Connection c = mock(Connection.class);
        Statement s = mock(Statement.class);
        ResultSet r = mock(ResultSet.class);
        
        CoffinDAO m = new CoffinDAO(c, new DAOManager());
        when(r.next()).thenReturn(true).thenReturn(false);
        
        assertEquals(m.findAll().size(), list.size());
        assertEquals(m.findAll().get(1).toString(), list.get(1).toString());
        }
        catch (Exception e)
        {
            
        }
    }
}