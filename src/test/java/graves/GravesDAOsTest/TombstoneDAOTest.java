/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesDAOsTest;

import graves.daos.DAOManager;
import graves.daos.MaterialDAO;
import graves.daos.TombstoneDAO;
import graves.model.Material;
import graves.model.Tombstone;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author jiri
 */
public class TombstoneDAOTest {
    
    public TombstoneDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void findAllTest()
    {
        try
        {
        ArrayList<Tombstone> list = new ArrayList<Tombstone>();
        list.add(new Tombstone(1, new Material(1, "drevo"), "zemrel", null));
        
        Connection c = mock(Connection.class);
        Statement s = mock(Statement.class);
        ResultSet r = mock(ResultSet.class);
        
        TombstoneDAO m = new TombstoneDAO(c, new DAOManager());
        when(r.next()).thenReturn(true).thenReturn(false);
        when(r.getInt("id")).thenReturn(1);
        when(r.getString("text")).thenReturn("zemrel");
        
        assertEquals(m.findAll().size(), list.size());
        assertEquals(m.findAll().get(1).toString(), list.get(1).toString());
        }
        catch (Exception e)
        {
            
        }
    }
}