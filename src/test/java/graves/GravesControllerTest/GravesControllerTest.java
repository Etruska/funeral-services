/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesControllerTest;

import graves.controller.GravesController;
import graves.daos.DAOManager;
import graves.daos.GraveDAO;
import headquarters.controller.HeadquartersController;
import headquarters.model.Model;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author jiri
 */
public class GravesControllerTest {
    
    public GravesControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        GravesController c = new GravesController(new HeadquartersController(new Model()));
	assertEquals(true, true);
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    
    public void testCountGraves()
    {
        GravesController c = new GravesController(new HeadquartersController(new Model()));
        GraveDAO g = mock(GraveDAO.class);
        DAOManager gm = mock(DAOManager.class);
        when(gm.graves()).thenReturn(g);
        when(g.countGraves(false)).thenReturn(3);
        c.setDAO(gm);
        assertEquals(c.countGraves(), new Integer(3));
    }
    
    
}