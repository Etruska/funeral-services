/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesViewDatamodelsTest;

import graves.model.Material;
import graves.model.Storeroom;
import graves.model.Tombstone;
import graves.view.datamodels.StoreroomTable;
import graves.view.datamodels.TombstonesTable;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jiri
 */
public class TombstonesTableTest {
    
    public TombstonesTableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void getValueAtTest()
    {
        ArrayList<Tombstone> list = new ArrayList<Tombstone>();
        list.add(new Tombstone(1, new Material(1, "mramor"), "umrel", new Storeroom(1, "sklad1", "doma 3", 200)));
        list.add(new Tombstone(2, new Material(2, "kamen"), "zemrel", new Storeroom(2, "sklad2", "doma 5", 200)));
        TombstonesTable t = new TombstonesTable();
        t.setTombstonesList(list);
        assertEquals(t.getValueAt(0, 0), "umrel");
        assertEquals(t.getValueAt(0, 1).toString(), "mramor");
        assertEquals(t.getValueAt(0, 2).toString(), "sklad1");
    }
    
    @Test
    public void getRowCountTest()
    {
        ArrayList<Tombstone> list = new ArrayList<Tombstone>();
        list.add(new Tombstone(1, new Material(1, "mramor"), "umrel", new Storeroom(1, "sklad1", "doma 3", 200)));
        list.add(new Tombstone(2, new Material(2, "kamen"), "zemrel", new Storeroom(2, "sklad2", "doma 5", 200)));
        TombstonesTable t = new TombstonesTable();
        t.setTombstonesList(list);
        assertEquals(t.getRowCount(), list.size());
    }
}