/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesViewDatamodelsTest;

import graves.model.Graveyard;
import graves.view.datamodels.GraveyardsTable;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jiri
 */
public class GraveyardsTableTest {
    
    public GraveyardsTableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void getValueAtTest()
    {
        ArrayList<Graveyard> list = new ArrayList<Graveyard>();
        list.add(new Graveyard(1, "superhrbitov", "dlouha 67", 20));
        list.add(new Graveyard(2, "druhy", "kratka 62", 200));
        GraveyardsTable t = new GraveyardsTable();
        t.setGraveyardsList(list);
        assertEquals(t.getValueAt(0, 1), "dlouha 67");
        assertEquals(t.getValueAt(0, 0), "superhrbitov");
        assertEquals(t.getValueAt(0, 2), 20);
    }
    
    @Test
    public void getRowCountTest()
    {
        ArrayList<Graveyard> list = new ArrayList<Graveyard>();
        list.add(new Graveyard(1, "superhrbitov", "dlouha 67", 20));
        list.add(new Graveyard(2, "druhy", "kratka 62", 200));
        GraveyardsTable t = new GraveyardsTable();
        t.setGraveyardsList(list);
        assertEquals(t.getRowCount(), list.size());
    }
}