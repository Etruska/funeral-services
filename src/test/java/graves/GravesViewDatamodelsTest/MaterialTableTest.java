/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesViewDatamodelsTest;

import graves.model.Coffin;
import graves.model.Material;
import graves.view.datamodels.CoffinsTable;
import graves.view.datamodels.MaterialTable;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jiri
 */
public class MaterialTableTest {
    
    public MaterialTableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void getValueAtTest()
    {
        ArrayList<Material> list = new ArrayList<Material>();
        list.add(new Material(1, "drevo"));
        list.add(new Material(2, "mramor"));
        MaterialTable t = new MaterialTable();
        t.setMaterialList(list);
        assertEquals(t.getValueAt(0, 0).toString(), "drevo");
        assertEquals(t.getValueAt(1, 0).toString(), "mramor");
    }
    
    @Test
    public void getRowCountTest()
    {
        ArrayList<Material> list = new ArrayList<Material>();
        list.add(new Material(1, "drevo"));
        list.add(new Material(2, "mramor"));
        MaterialTable t = new MaterialTable();
        t.setMaterialList(list);
        assertEquals(t.getRowCount(), list.size());
    }
}