/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesViewDatamodelsTest;

import graves.model.Coffin;
import graves.model.Graveyard;
import graves.model.Material;
import graves.view.datamodels.CoffinsTable;
import graves.view.datamodels.GraveyardsTable;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jiri
 */
public class CoffinsTableTest {
    
    public CoffinsTableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void getValueAtTest()
    {
        ArrayList<Coffin> list = new ArrayList<Coffin>();
        list.add(new Coffin(1, false, 100,50,new Material(1, "drevo"), null, null));
        list.add(new Coffin(2, false, 200,40,new Material(2, "mramor"), null, null));
        CoffinsTable t = new CoffinsTable();
        t.setCoffinsList(list);
        assertEquals(t.getValueAt(0, 0).toString(), "drevo");
        assertEquals(t.getValueAt(0, 1), 100);
        assertEquals(t.getValueAt(0, 2), 50);
        assertEquals(t.getValueAt(0, 3), null);
    }
    
    @Test
    public void getRowCountTest()
    {
        ArrayList<Coffin> list = new ArrayList<Coffin>();
        list.add(new Coffin(1, false, 100,50,new Material(1, "drevo"), null, null));
        list.add(new Coffin(2, false, 200,40,new Material(2, "mramor"), null, null));
        CoffinsTable t = new CoffinsTable();
        t.setCoffinsList(list);
        assertEquals(t.getRowCount(), list.size());
    }
}