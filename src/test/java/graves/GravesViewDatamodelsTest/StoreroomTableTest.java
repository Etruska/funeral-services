/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesViewDatamodelsTest;

import graves.model.Material;
import graves.model.Storeroom;
import graves.view.datamodels.MaterialTable;
import graves.view.datamodels.StoreroomTable;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jiri
 */
public class StoreroomTableTest {
    
    public StoreroomTableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void getValueAtTest()
    {
        ArrayList<Storeroom> list = new ArrayList<Storeroom>();
        list.add(new Storeroom(1, "sklad1", "nekde 1", 50));
        list.add(new Storeroom(2, "sklad2", "nekde 2", 60));
        StoreroomTable t = new StoreroomTable();
        t.setStoreroomList(list);
        assertEquals(t.getValueAt(0, 0), "sklad1");
        assertEquals(t.getValueAt(0, 1), "nekde 1");
        assertEquals(t.getValueAt(0, 2), 50);
    }
    
    @Test
    public void getRowCountTest()
    {
        ArrayList<Storeroom> list = new ArrayList<Storeroom>();
        list.add(new Storeroom(1, "sklad1", "nekde 1", 50));
        list.add(new Storeroom(2, "sklad2", "nekde 2", 60));
        StoreroomTable t = new StoreroomTable();
        t.setStoreroomList(list);
        assertEquals(t.getRowCount(), list.size());
    }
}