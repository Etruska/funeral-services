/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.GravesViewDatamodelsTest;

import graves.model.Coffin;
import graves.model.Grave;
import graves.model.Graveyard;
import graves.model.Material;
import graves.model.Storeroom;
import graves.model.Tombstone;
import graves.view.datamodels.GraveTable;
import graves.view.datamodels.StoreroomTable;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jiri
 */
public class GraveTableTest {
    
    public GraveTableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void getValueAtTest()
    {
        Material m = new Material(1, "drevo");
        Storeroom s = new Storeroom(1, "sklad", "v dole", 50);
        Tombstone to = new Tombstone(1, m, "zemrel", s);
        Coffin c = new Coffin(1, false, 200, 50, m, null, s);
        Graveyard gd = new Graveyard(1, "hrbitov splnenych prani", "terronska 8", 200);
        ArrayList<Grave> list = new ArrayList<Grave>();
        list.add(new Grave(1, to, c, gd));
        GraveTable t = new GraveTable();
        t.setGraveList(list);
        assertEquals(t.getValueAt(0, 0).toString(), "hrbitov splnenych prani");
        assertEquals(t.getValueAt(0, 1).toString(), "zemrel");
        assertEquals(t.getValueAt(0, 2).toString(), "drevo200x50");
    }
    
    @Test
    public void getRowCountTest()
    {
        Material m = new Material(1, "drevo");
        Storeroom s = new Storeroom(1, "sklad", "v dole", 50);
        Tombstone to = new Tombstone(1, m, "zemrel", s);
        Coffin c = new Coffin(1, false, 200, 50, m, null, s);
        Graveyard gd = new Graveyard(1, "hrbitov splnenych prani", "terronska 8", 200);
        ArrayList<Grave> list = new ArrayList<Grave>();
        list.add(new Grave(1, to, c, gd));
        GraveTable t = new GraveTable();
        t.setGraveList(list);
        assertEquals(t.getRowCount(), list.size());
    }
}