package people;



import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import people.models.CorpseTableModel;
import people.person.Corpse;

/**
 *
 * @author baji
 */
public class CorpseTableModelTest {
    
    CorpseTableModel model;
    ArrayList<Corpse> corpses = new ArrayList<Corpse>();
    
    @Before
    public void setUp() {
        
        model = new CorpseTableModel(1);
        
    }    
    
    @Test
    public void setCorpses() {
        ArrayList<Corpse> corps = new ArrayList<Corpse>();
        Corpse c = new Corpse("Jakub", "Baierl", 23, "1991", "2001");
        this.corpses.add(c);
        c.setName("Petr");
        this.corpses.add(c);
        
        this.model.setCorpses(corpses);
        
        Object o = this.model.getValueAt(0, 2);
       
        assertEquals("Baierl", (String) o);
    }
    
    // ################################## private helpers ##################################
    
    
    private InputStream createInputStream(String content) {
        try {
            return new ByteArrayInputStream(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(CorpseTest.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
