package people;



import people.controllers.PeopleController;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import people.person.*;

/**
 *
 * @author baji
 */
public class RelativeTest {

    Relative relative;
    Contact contact;
    Address address;
    
    @Before
    public void setUp() {
        this.address = new Address("Horká", "Brno", 34523);
        this.contact = new Contact(774486660, "mail3", address);
        relative = new Relative("Jakub", "Baierl", 23, contact, 3,"bratr");
        relative.setRelativeID(2);
       
    }    
    
    @Test
    public void getRelativeID() {
        
        assertEquals(2, relative.getRelativeID());
    }
    
    @Test
    public void getName() {
        
        assertEquals("Jakub", relative.getName());
    }
    
    @Test
    public void getSurname() {
        
        assertEquals("Baierl", relative.getSurname());
    }
    
    @Test
    public void getAge() {
        
        assertEquals(23, relative.getAge());
    }
    
    @Test
    public void getCorpseID() {
        
        assertEquals(3, relative.getCorpseID());
    }
    
    @Test
    public void getContact() {
        Contact c = new Contact(774483339, "mail1", address);
        relative.setContact(c);
        assertEquals("mail1", relative.getContact().getMail());
    }
    
    @Test
    public void getCorpse() {
        Corpse c = new Corpse("Petr", "Barta", 15, "1990", "2013");
        relative.setCorpse(c);
        assertEquals("Petr", relative.getCorpse().getName());
    }
    
    @Test
    public void getRelationalship() {
        
        assertEquals("bratr", relative.getRelationalship());
    }
    
    @Test
    public void setRelationalship() {
        relative.setRelationalship("otec");
        assertEquals("otec", relative.getRelationalship());
    }
    
    @Test
    public void setCorpseID() {
        relative.setCorpseID(1);
        assertEquals(1, relative.getCorpseID());
    }
    
    
    // ################################## private helpers ##################################
    
    
    private InputStream createInputStream(String content) {
        try {
            return new ByteArrayInputStream(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(RelativeTest.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
