package people;



import people.controllers.PeopleController;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import people.person.*;

/**
 *
 * @author baji
 */
public class CorpseTest {

    Corpse corpse;
    
    @Before
    public void setUp() {
        corpse = new Corpse("Jakub", "Baierl", 23, "1991-03-14", "2012-03-13");
        corpse.setCorpseID(23);
       
    }    
    
    @Test
    public void getCorpseID() {
        
        assertEquals(23, corpse.getCorpseID());
    }
    
    @Test
    public void getName() {
        
        assertEquals("Jakub", corpse.getName());
    }
    
    @Test
    public void getSurname() {
        
        assertEquals("Baierl", corpse.getSurname());
    }
    
    @Test
    public void getAge() {
        
        assertEquals(23, corpse.getAge());
    }
    
    @Test
    public void getDateOfBirth() {
        
        assertEquals("1991-03-14", corpse.getDateOfBirth());
    }
    
    @Test
    public void getDateOfExpire() {
        
        assertEquals("2012-03-13", corpse.getDateOfExpire());
    }
    
    @Test
    public void toStringTest() {
        
        assertEquals("Jakub"+" "+"Baierl", corpse.toString());
    }
    
    @Test
    public void setDateOfBirth() {
        this.corpse.setDateOfBirth("1883-02-20");
        assertEquals("1883-02-20", corpse.getDateOfBirth());
    }
    
    @Test
    public void setDateOfExpire() {
        this.corpse.setDateOfExpire("1999-03-20");
        assertEquals("1999-03-20", corpse.getDateOfExpire());
    }
    
    
    // ################################## private helpers ##################################
    
    
    private InputStream createInputStream(String content) {
        try {
            return new ByteArrayInputStream(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(CorpseTest.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
