package people;



import headquarters.model.db.IDatabase;
import headquarters.model.Model;
import java.sql.ResultSet;
import java.util.ArrayList;
import people.models.RelativeDAO;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import people.person.*;

/**
 *
 * @author baji
 */
public class RelativeDAOTest {

    private ArrayList<Relative> relatives = new ArrayList<Relative>();
    Model model;
    IDatabase mockDB;
    RelativeDAO relativeDAO;
    Relative relative;
    
    @Before
    public void setUp() {
        this.model = mock(Model.class, RETURNS_DEEP_STUBS);
        this.mockDB = mock(IDatabase.class, RETURNS_DEEP_STUBS);  
        relativeDAO = mock(RelativeDAO.class);
    }    
    
    @Test
    public void getRelativess() {
        
        ArrayList<Relative> cps = new ArrayList<Relative>();
        Address add = new Address("Horka", "Praha", 34523);
        Contact con = new Contact(6447734,"mail3",add);
        Relative c = new Relative("Jakub", "Baierl", 13, con, 1, "bratr");
        cps.add(c);
        
        ResultSet rsMock = mock(ResultSet.class);
        try{
            when(relativeDAO.getRelatives()).thenReturn(cps);
            when(rsMock.next()).thenReturn(true).thenReturn(false);
            this.relatives = this.relativeDAO.getRelatives();
            createInputStream(this.relatives.get(0).getRelationalship());
            assertEquals("bratr", this.relatives.get(0).getRelationalship());
        }
        catch (Exception e){
        }
        
    }
    
    @Test
    public void getRelativeByID() {
        Address add = new Address("Horka", "Praha", 34523);
        Contact con = new Contact(6447734,"mail3",add);
        Relative c = new Relative("Jakub", "Baierl", 13, con, 1, "bratr");
        
        ResultSet rsMock = mock(ResultSet.class);
        try{
            when(relativeDAO.getRelativeByID(0)).thenReturn(c);
            when(rsMock.next()).thenReturn(true).thenReturn(false);
            Relative corp = relativeDAO.getRelativeByID(0);
            assertEquals(1, corp.getCorpseID());
        }
        catch (Exception e){
        }
        
    }

    @Test
    public void addRelative() {
        
        Address add = new Address("Horka", "Praha", 34523);
        Contact con = new Contact(6447734,"mail3",add);
        Relative c = new Relative("Jakub", "Baierl", 13, con, 1, "bratr");
        
        ResultSet rsMock = mock(ResultSet.class);
        try{
            when(relativeDAO.addRelative(c)).thenReturn(true);
            boolean res = relativeDAO.addRelative(c);
            assertEquals(true,res);
        }
        catch (Exception e){
        }
        
    }
    
    @Test
    public void deleteRelative() {
        
        ResultSet rsMock = mock(ResultSet.class);
        try{
            when(relativeDAO.deleteRelative(0)).thenReturn(true);
            boolean res = relativeDAO.deleteRelative(0);
            assertEquals(true,res);
        }
        catch (Exception e){
        }
        
    }

    
    // ################################## private helpers ##################################
    
    
    private InputStream createInputStream(String content) {
        try {
            return new ByteArrayInputStream(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(this.relatives.get(0).getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
