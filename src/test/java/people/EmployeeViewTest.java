package people;



import people.controllers.PeopleController;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import people.person.*;
import people.views.*;
import headquarters.model.*;
import headquarters.model.db.*;
import headquarters.controller.*;


/**
 *
 * @author baji
 */
public class EmployeeViewTest {

    PeopleController con;
    Model model;
    EmployeeView employeeView;
    HeadquartersController controller;
    AddEmployeeDialog dialog;
    IDatabase mockDB;
   
    /*
    @Before
    public void setUp() {
        
        this.model = mock(Model.class, RETURNS_DEEP_STUBS);
        this.mockDB = mock(IDatabase.class, RETURNS_DEEP_STUBS); 
        controller = mock(HeadquartersController.class);
        con = mock(PeopleController.class);
        this.dialog = mock(AddEmployeeDialog.class);
    }    
    
    @Test
    public void getTest() {
        employeeView = mock(EmployeeView.class);
        assertEquals(true, true);
    }
    
    @Test
    public void testDialog() {
        
        this.dialog = new AddEmployeeDialog(con);
        String res = dialog.getNameLabelValue();
        assertEquals("Jméno:", res);
    }
    */
    
    // ################################## private helpers ##################################
    
    
    private InputStream createInputStream(String content) {
        try {
            return new ByteArrayInputStream(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(EmployeeTest.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
