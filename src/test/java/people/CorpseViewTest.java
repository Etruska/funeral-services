package people;



import people.controllers.PeopleController;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import people.person.*;
import people.views.*;
import headquarters.model.*;
import headquarters.controller.*;

/**
 *
 * @author baji
 */
public class CorpseViewTest {

    PeopleController con;
    Model model;
    CorpseView corpseView;
    HeadquartersController controller;
    AddCorpseDialog dialog;
    
    @Before
    public void setUp() {
        
        model = new Model();
        controller = new HeadquartersController(model);
        con = mock(PeopleController.class);
    }    
    
    @Test
    public void testDialog() {
        
        this.dialog = new AddCorpseDialog(con);
        String res = dialog.getNameLabelValue();
        assertEquals("Jméno:", res);
    }
    

    // ################################## private helpers ##################################
    
    
    private InputStream createInputStream(String content) {
        try {
            return new ByteArrayInputStream(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(CorpseTest.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
