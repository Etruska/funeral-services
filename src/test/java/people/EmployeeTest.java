package people;



import people.controllers.PeopleController;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import people.person.*;

/**
 *
 * @author baji
 */
public class EmployeeTest {

    Employee employee;
    Contact contact;
    Address address;
    
    private static final double DELTA = 1e-15;
    
    @Before
    public void setUp() {
        this.address = new Address("Horká", "Brno", 34523);
        this.contact = new Contact(774486660, "mail3", address);
        employee = new Employee("Jakub", "Baierl", 23, contact, 20000, 910314, "hrobař");
        employee.setEmployeeID(2);
       
    }    
    
    @Test
    public void getEmployeeID() {
        
        assertEquals(2, employee.getEmployeeID());
    }
    
    @Test
    public void getName() {
        
        assertEquals("Jakub", employee.getName());
    }
    
    @Test
    public void getSurname() {
        
        assertEquals("Baierl", employee.getSurname());
    }
    
    @Test
    public void getAge() {
        
        assertEquals(23, employee.getAge());
    }
    
    @Test
    public void getProfession() {
        
        assertEquals("hrobař", employee.getProfession());
    }
    
    @Test
    public void getSecNumber() {
        
        assertEquals(910314, employee.getSecNumber(), DELTA);
    }
    
    @Test
    public void getSalary() {
        
        assertEquals(20000, employee.getSalary());
    }
    
    @Test
    public void setSalary() {
        employee.setSalary(30000);
        assertEquals(30000, employee.getSalary());
    }
    
    @Test
    public void setProfession() {
        employee.setProfession("banker");
        assertEquals("banker", employee.getProfession());
    }
    
    @Test
    public void setSecNumber() {
        employee.setSecNumber(90210);
        assertEquals(90210, employee.getSecNumber(),DELTA);
    }
    
    @Test
    public void setName() {
        employee.setName("Bolek");
        assertEquals("Bolek", employee.getName());
    }
    
    @Test
    public void setSurname() {
        employee.setSurname("Karas");
        assertEquals("Karas", employee.getSurname());
    }
    
    @Test
    public void setAge() {
        employee.setAge(32);
        assertEquals(32, employee.getAge());
    }
    
    // ################################## private helpers ##################################
    
    
    private InputStream createInputStream(String content) {
        try {
            return new ByteArrayInputStream(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(EmployeeTest.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
