package people;



import people.controllers.PeopleController;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import people.person.*;

/**
 *
 * @author baji
 */
public class ContactTest {

    private Contact contact;
    private Address address;
    
    @Before
    public void setUp() {
        this.address = new Address("Nádražní", "Praha", 34523);
        
        contact = new Contact(774486660, "mail@seznam.cz", this.address);
        contact.setContactID(3);
       
    }    
    
    @Test
    public void getContactID() {
        
        assertEquals(3, contact.getContactID());
    }
    
    @Test
    public void getTelNumber() {
        
        assertEquals(774486660, contact.getTelNumber());
    }
    
    @Test
    public void getMail() {
        
        assertEquals("mail@seznam.cz", contact.getMail());
    }
    
    @Test
    public void getAddress() {
        Address a = new Address("Nádražní", "Praha", 34523);
        assertEquals(a, contact.getAddress());
    }
    
    @Test
    public void getAddress2() {
        assertNotSame(null, contact.getAddress());
    }
    
    @Test
    public void getStreet() {
        
        assertEquals("Nádražní", address.getStreet());
    }
    
    @Test
    public void getCity() {
        
        assertEquals("Praha", address.getCity());
    }
    
    @Test
    public void getZip() {
        
        assertEquals(34523, address.getZip());
    }
    
    @Test
    public void setAddress() {
        Address a = new Address("Nádražní", "Praha", 34523);
        contact.setAddress(a);
        
        assertEquals(a, contact.getAddress());
    }
    
    @Test
    public void setMail() {
        
        contact.setMail("mail4");
        
        assertEquals("mail4", contact.getMail());
    }
    
    @Test
    public void setTelNumber() {
        
        contact.setTelNumber(774486660);
        
        assertEquals(774486660, contact.getTelNumber());
    }
    
    @Test
    public void setStreet() {
        
        address.setStreet("Zelená");
        
        assertEquals("Zelená", address.getStreet());
    }
    
    @Test
    public void setCity() {
        
        address.setCity("Opava");
        
        assertEquals("Opava", address.getCity());
    }
    
    @Test
    public void setZip() {
        
        address.setZip(2345);
        
        assertEquals(2345, address.getZip());
    }
    
    
    // ################################## private helpers ##################################
    
    
    private InputStream createInputStream(String content) {
        try {
            return new ByteArrayInputStream(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(CorpseTest.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
