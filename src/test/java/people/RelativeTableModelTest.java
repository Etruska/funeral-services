package people;



import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import people.models.RelativeTableModel;
import people.person.Address;
import people.person.Contact;
import people.person.Relative;

/**
 *
 * @author baji
 */
public class RelativeTableModelTest {
    
    RelativeTableModel model;
    ArrayList<Relative> relatives = new ArrayList<Relative>();
    
    @Before
    public void setUp() {
        
        model = new RelativeTableModel(1);
        
    }    
    
    @Test
    public void setRelatives() {
        ArrayList<Relative> corps = new ArrayList<Relative>();
        Address add = new Address("Horka", "Praha", 34523);
        Contact con = new Contact(6447734,"mail3",add);
        Relative c = new Relative("Jakub", "Baierl", 13, con, 1, "bratr");
        this.relatives.add(c);
        c.setName("Petr");
        this.relatives.add(c);
        
        this.model.setRelatives(relatives);
        
        Object o = this.model.getValueAt(0, 2);
       
        assertEquals("Baierl", (String) o);
    }
    
    // ################################## private helpers ##################################
    
    
    private InputStream createInputStream(String content) {
        try {
            return new ByteArrayInputStream(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(RelativeTest.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
