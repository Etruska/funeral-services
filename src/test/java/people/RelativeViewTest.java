package people;



import people.controllers.PeopleController;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import people.person.*;
import people.views.*;
import headquarters.model.*;
import headquarters.controller.*;

/**
 *
 * @author baji
 */
public class RelativeViewTest {

    PeopleController con;
    Model model;
    RelativeView relativeView;
    HeadquartersController controller;
    AddRelativeDialog dialog;
    
    
    /*
    @Before
    public void setUp() {
        
        this.model = mock(Model.class, RETURNS_DEEP_STUBS); 
        controller = mock(HeadquartersController.class);
        con = mock(PeopleController.class);
        this.dialog = mock(AddRelativeDialog.class);
    }    
    
    @Test
    public void getTest() {
        relativeView = mock(RelativeView.class);
        assertEquals(true, true);
    }
    */
    
    
    // ################################## private helpers ##################################
    
    
    private InputStream createInputStream(String content) {
        try {
            return new ByteArrayInputStream(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(RelativeTest.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
