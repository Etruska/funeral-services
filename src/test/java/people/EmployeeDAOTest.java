package people;



import headquarters.model.db.IDatabase;
import headquarters.model.Model;
import java.sql.ResultSet;
import java.util.ArrayList;
import people.models.EmployeeDAO;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import people.person.*;

/**
 *
 * @author baji
 */
public class EmployeeDAOTest {

    private ArrayList<Employee> employees = new ArrayList<Employee>();
    Model model;
    IDatabase mockDB;
    EmployeeDAO employeeDAO;
    Employee employee;
    
    @Before
    public void setUp() {
        this.model = mock(Model.class, RETURNS_DEEP_STUBS);
        this.mockDB = mock(IDatabase.class, RETURNS_DEEP_STUBS);  
        employeeDAO = mock(EmployeeDAO.class);
    }    
    
    @Test
    public void getEmployeess() {
        
        ArrayList<Employee> cps = new ArrayList<Employee>();
        Address add = new Address("Horka", "Praha", 34523);
        Contact con = new Contact(6447734,"mail3",add);
        Employee c = new Employee("Jakub", "Baierl", 13, con, 2000,929394,"hrobník");
        cps.add(c);
        
        ResultSet rsMock = mock(ResultSet.class);
        try{
            when(employeeDAO.getEmployees()).thenReturn(cps);
            when(rsMock.next()).thenReturn(true).thenReturn(false);
            this.employees = this.employeeDAO.getEmployees();
            createInputStream(this.employees.get(0).getProfession());
            assertEquals("hrobník", this.employees.get(0).getProfession());
        }
        catch (Exception e){
        }
        
    }
    
    @Test
    public void getEmployeeByID() {
        Address add = new Address("Horka", "Praha", 34523);
        Contact con = new Contact(6447734,"mail3",add);
        Employee c = new Employee("Jakub", "Baierl", 13, con, 30000, 91349, "2010");
        
        ResultSet rsMock = mock(ResultSet.class);
        try{
            when(employeeDAO.getEmployeeByID(0)).thenReturn(c);
            when(rsMock.next()).thenReturn(true).thenReturn(false);
            Employee corp = employeeDAO.getEmployeeByID(0);
            assertEquals(30000, corp.getSalary());
        }
        catch (Exception e){
        }
        
    }

    @Test
    public void addEmployee() {
        
        Address add = new Address("Horka", "Praha", 34523);
        Contact con = new Contact(6447734,"mail3",add);
        Employee c = new Employee("Jakub", "Baierl", 13, con, 30000, 91349, "2010");
        
        ResultSet rsMock = mock(ResultSet.class);
        try{
            when(employeeDAO.addEmployee(c)).thenReturn(true);
            boolean res = employeeDAO.addEmployee(c);
            assertEquals(true,res);
        }
        catch (Exception e){
        }
        
    }
    
    @Test
    public void deleteEmployee() {
        
        ResultSet rsMock = mock(ResultSet.class);
        try{
            when(employeeDAO.deleteEmployee(0)).thenReturn(true);
            boolean res = employeeDAO.deleteEmployee(0);
            assertEquals(true,res);
        }
        catch (Exception e){
        }
        
    }

    
    // ################################## private helpers ##################################
    
    
    private InputStream createInputStream(String content) {
        try {
            return new ByteArrayInputStream(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(this.employees.get(0).getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
