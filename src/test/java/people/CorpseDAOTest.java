package people;



import headquarters.model.db.IDatabase;
import headquarters.model.Model;
import java.sql.ResultSet;
import java.util.ArrayList;
import people.models.CorpseDAO;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import people.person.*;

/**
 *
 * @author baji
 */
public class CorpseDAOTest {

    private ArrayList<Corpse> corpses = new ArrayList<Corpse>();
    Model model;
    IDatabase mockDB;
    CorpseDAO corpseDAO;
    Corpse corpse;
    
    @Before
    public void setUp() {
        this.model = mock(Model.class, RETURNS_DEEP_STUBS);
        this.mockDB = mock(IDatabase.class, RETURNS_DEEP_STUBS);  
        corpseDAO = mock(CorpseDAO.class);
    }    
    
    @Test
    public void getCorpsess() {
        
        ArrayList<Corpse> cps = new ArrayList<Corpse>();
        Corpse c = new Corpse("Jakub", "Baierl", 13, "1991", "2010");
        cps.add(c);
        
        ResultSet rsMock = mock(ResultSet.class);
        try{
            when(corpseDAO.getCorpses()).thenReturn(cps);
            when(rsMock.next()).thenReturn(true).thenReturn(false);
            this.corpses = this.corpseDAO.getCorpses();
            createInputStream(this.corpses.get(0).getName());
            assertEquals("Jakub", this.corpses.get(0).getName());
        }
        catch (Exception e){
        }
        
    }
    
    @Test
    public void getCorpseByID() {
        
        Corpse c = new Corpse("Jakub", "Baierl", 13, "1991", "2010");
        
        ResultSet rsMock = mock(ResultSet.class);
        try{
            when(corpseDAO.getCorpseByID(0)).thenReturn(c);
            when(rsMock.next()).thenReturn(true).thenReturn(false);
            Corpse corp = corpseDAO.getCorpseByID(0);
            assertEquals("Baierl", corp.getSurname());
        }
        catch (Exception e){
        }
        
    }

    @Test
    public void addCorpse() {
        
        ArrayList<Corpse> cps = new ArrayList<Corpse>();
        Corpse c = new Corpse("Jakub", "Baierl", 23, "1991", "2010");
        cps.add(c);
        
        ResultSet rsMock = mock(ResultSet.class);
        try{
            when(corpseDAO.addCorpse(c)).thenReturn(true);
            boolean res = corpseDAO.addCorpse(c);
            assertEquals(true,res);
        }
        catch (Exception e){
        }
        
    }
    
    @Test
    public void deleteCorpse() {
        
        ResultSet rsMock = mock(ResultSet.class);
        try{
            when(corpseDAO.deleteCorpse(0)).thenReturn(true);
            boolean res = corpseDAO.deleteCorpse(0);
            assertEquals(true,res);
        }
        catch (Exception e){
        }
        
    }

    
    // ################################## private helpers ##################################
    
    
    private InputStream createInputStream(String content) {
        try {
            return new ByteArrayInputStream(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(this.corpses.get(0).getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
