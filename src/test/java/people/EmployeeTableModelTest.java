package people;



import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import people.models.EmployeeTableModel;
import people.person.Address;
import people.person.Contact;
import people.person.Employee;

/**
 *
 * @author baji
 */
public class EmployeeTableModelTest {
    
    EmployeeTableModel model;
    ArrayList<Employee> employees = new ArrayList<Employee>();
    
    @Before
    public void setUp() {
        
        model = new EmployeeTableModel(1);
        
    }    
    
    @Test
    public void setEmployees() {
        ArrayList<Employee> corps = new ArrayList<Employee>();
        Address add = new Address("Horka", "Praha", 34523);
        Contact con = new Contact(6447734,"mail3",add);
        Employee c = new Employee("Jakub", "Baierl", 13, con, 2000, 929394,"hrobník");
        this.employees.add(c);
        c.setName("Petr");
        this.employees.add(c);
        
        this.model.setEmployees(employees);
        
        Object o = this.model.getValueAt(0, 2);
       
        assertEquals("Baierl", (String) o);
    }
    
    // ################################## private helpers ##################################
    
    
    private InputStream createInputStream(String content) {
        try {
            return new ByteArrayInputStream(content.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(EmployeeTest.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
