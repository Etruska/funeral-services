/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package headquarters;

import headquarters.controller.HeadquartersController;
import headquarters.model.FuneralTableModel;
import headquarters.model.Model;
import headquarters.model.entities.Funeral;
import headquarters.view.AppLayout;
import headquarters.view.FuneralListView;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Vojtěch Petrus
 */
public class AppLayoutTest {

    List<Funeral> funerals = new ArrayList<Funeral>();
    FuneralTableModel tableModel;
    AppLayout layout;
    FuneralListView funeralListView;

    @Before
    public void setUp() {

        funerals = Funerals.getFunerals();

        Model m = mock(Model.class, RETURNS_DEEP_STUBS);
        HeadquartersController c = new HeadquartersController(m);
        when(m.getFuneralDAO().getAllFunerals()).thenReturn(funerals);
        layout = new AppLayout(m, c);
        funeralListView = new FuneralListView(c, m);
        layout.setContentPanel(funeralListView);
    }

    @Test
    public void testContentPanelExists() {
        assertTrue(layout.contentPanelExists("Headquarters/FuneralList"));
    }

    @Test
    public void testContentPanelExists2() {
        assertFalse(layout.contentPanelExists("Headquarters/FictiveView"));
    }

    @Test
    public void testShowStatus() {
        layout.showStatus("status quo", AppLayout.STATUS_INFO);
        assertEquals("status quo", layout.getStatus());
    }

    @Test
    public void testShowStatus2() {
        layout.showStatus("status quos2", AppLayout.STATUS_ERROR);
        assertThat("status quo", not(layout.getStatus()));
    }

    @Test
    public void testShowStatus3() {
        layout.showStatus("statistic", AppLayout.STATUS_OK);
        assertEquals("statistic", layout.getStatus());
    }

    @Test
    public void testShowStatus4() {
        layout.clearStatus();
        assertEquals(" ", layout.getStatus());
    }

}
