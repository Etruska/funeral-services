package headquarters;

import headquarters.controller.HeadquartersController;
import headquarters.model.FuneralTableModel;
import headquarters.model.Model;
import headquarters.model.entities.Funeral;
import headquarters.view.FuneralDialog;
import headquarters.view.FuneralListView;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import people.person.Corpse;

/**
 *
 * @author Vojtěch Petrus
 */
public class FuneralDialogTest {

    List<Funeral> funerals = new ArrayList<>();
    List<Corpse> corpses = new ArrayList<>();
    FuneralTableModel tableModel;
    FuneralDialog dialog;
    FuneralListView funeralListView;

    @Before
    public void setUp() {

        funerals = Funerals.getFunerals();
        corpses = Corpses.getCorpses();
        
        Model m = mock(Model.class, RETURNS_DEEP_STUBS);
        HeadquartersController c = new HeadquartersController(m);
        when(m.getFuneralDAO().getAllFunerals()).thenReturn(funerals);
        dialog = new FuneralDialog(c, new JFrame(), corpses, funerals.get(0));
    }

    @Test
    public void testContentPanelExists() {
        assertEquals("Humpolec", dialog.getPlace());
    }


}
