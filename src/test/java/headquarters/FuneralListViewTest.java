package headquarters;

import headquarters.controller.HeadquartersController;
import headquarters.model.FuneralTableModel;
import headquarters.model.Model;
import headquarters.model.entities.Funeral;
import headquarters.view.FuneralListView;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author Vojtěch Petrus
 */
public class FuneralListViewTest {

    Model m;
    HeadquartersController c;
    FuneralListView view;

    List<Funeral> funerals = new ArrayList<Funeral>();
    FuneralTableModel tableModel;

    @Before
    public void setUp() {

        funerals = Funerals.getFunerals();    
        
        m = mock(Model.class, RETURNS_DEEP_STUBS);
        c = new HeadquartersController(m);
        when(m.getFuneralDAO().getAllFunerals()).thenReturn(funerals);
        view = new FuneralListView(c, m);
        tableModel = new FuneralTableModel(m, c);
    }

    @Test
    public void testName() {
        assertEquals("Headquarters/FuneralList", view.getViewName());
    }

    @Test
    public void testObserver() {
        FuneralListView mockView = mock(FuneralListView.class);
        Model model = new Model();
        model.attachObserver(mockView);
        model.debugNotify();
        verify(mockView).modelUpdated();
    }
}
