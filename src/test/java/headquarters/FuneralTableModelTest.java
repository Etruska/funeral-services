/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package headquarters;

import headquarters.controller.HeadquartersController;
import headquarters.model.FuneralTableModel;
import headquarters.model.Model;
import headquarters.model.entities.Funeral;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import people.person.Corpse;

/**
 *
 * @author etruska
 */
public class FuneralTableModelTest {

    List<Funeral> funerals;
    FuneralTableModel tableModel;

    @Before
    public void setUp() {

        funerals = Funerals.getFunerals();
        Model m = mock(Model.class, RETURNS_DEEP_STUBS);
        HeadquartersController c = new HeadquartersController(m);
        when(m.getFuneralDAO().getAllFunerals()).thenReturn(funerals);
        tableModel = new FuneralTableModel(m, c);
    }

    @Test
    public void testValueAt1() {
        assertEquals(33, tableModel.getValueAt(0, 0));
    }

    @Test
    public void testValueAt2() {
        assertEquals(50, tableModel.getValueAt(1, 0));
    }

    @Test
    public void testValueAt3() {
        assertEquals("Humpolec", tableModel.getValueAt(0, 2));
    }

    @Test
    public void testValueAt4() {
        Corpse c2 = funerals.get(1).getCorpse();
        assertEquals(c2, tableModel.getValueAt(1, 1));
    }

    @Test
    public void testValueAt5() {
        assertEquals("ne", tableModel.getValueAt(1, 5));
    }

}
