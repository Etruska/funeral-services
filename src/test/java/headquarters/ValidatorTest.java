package headquarters;

import headquarters.model.Validator;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import people.person.Corpse;

public class ValidatorTest {

    Corpse c1;
    Corpse c2;
    List<Corpse> corpses;
    Validator v;

    @Before
    public void setUp() {
        corpses = Corpses.getCorpses();
        v = new Validator();
    }

    @Test
    public void testValidateFuneral1() {
        org.junit.Assert.assertTrue(v.validateFuneral(corpses.get(0), "Humpolect", "23", true, new Date()));
    }

    @Test
    public void testValidateFuneral2() {

        org.junit.Assert.assertFalse(v.validateFuneral(corpses.get(0), "", "455", true, new Date()));
    }

    @Test
    public void testValidateFuneral3() {
        org.junit.Assert.assertFalse(v.validateFuneral(corpses.get(0), "Prague", "aaa", true, new Date()));
    }

    @Test
    public void testValidateFuneral4() {
        org.junit.Assert.assertFalse(v.validateFuneral(corpses.get(0), "Prague", "35", true, null));
    }

    @Test
    public void testValidateFuneral5() {
        corpses.get(0).setCorpseID(0);
        v.validateFuneral(corpses.get(0), "", "455", true, new Date());
        assertEquals("Zesnulý nebyl nalezen.", v.getMessage());
    }
}
