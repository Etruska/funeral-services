package headquarters;

import java.util.ArrayList;
import java.util.List;
import people.person.Corpse;

/**
 *
 * @author Vojtěch Petrus
 */
public class Corpses {

    public static List<Corpse> getCorpses() {
        List<Corpse> corpses = new ArrayList<Corpse>();
        Corpse c1 = new Corpse("Vojtěch", "Petrus", 29, "23.23.1991", "23.23.2023");
        c1.setCorpseID(23);
        Corpse c2 = new Corpse("Jim", "Raynor", 45, "23.23.3035", "23.23.3127");
        c2.setCorpseID(99);

        corpses.add(c1);
        corpses.add(c2);
        
        return corpses;
    }

}
