package headquarters;

import headquarters.model.entities.Funeral;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import people.person.Corpse;


/**
 *
 * @author etruska
 */
public class Funerals {

    public static List<Funeral> getFunerals() {
        List<Funeral> funerals = new ArrayList<Funeral>();
        Corpse c1 = new Corpse("Vojtěch", "Petrus", 29, "23.23.1991", "23.23.2023");
        c1.setCorpseID(23);
        Funeral f1 = new Funeral(c1, "Humpolec", 23, true, new Date());
        f1.setId(33);
        Corpse c2 = new Corpse("Jim", "Raynor", 45, "23.23.3035", "23.23.3127");
        c2.setCorpseID(99);
        Funeral f2 = new Funeral(c2, "Aa", 455, false, new Date());
        f2.setId(50);
        funerals.add(f1);
        funerals.add(f2);
        
        return funerals;
    }
}
