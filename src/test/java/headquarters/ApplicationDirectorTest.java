package headquarters;

import AccountingController.IAccounting;
import headquarters.controller.HeadquartersController;
import headquarters.model.ApplicationDirector;
import headquarters.model.Model;
import headquarters.model.ModuleBuilder;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;
import people.interfaces.IPeople;

/**
 *
 * @author Vojtěch Petrus
 */
public class ApplicationDirectorTest {

    Model model;
    HeadquartersController controller;
    ModuleBuilder builder;
    ApplicationDirector app;

    @Before
    public void setUp() {
        this.model = new Model();
        this.controller = new HeadquartersController(model);

        this.builder = new ModuleBuilder(model, controller);
        this.app = new ApplicationDirector(model, controller, builder);
        app.run();
    }

    @Test
    public void testApplicationDirector1() {
        Object module = model.getPeopleModule();
        assertThat(module, instanceOf(IPeople.class));
    }
    
    @Test
    public void testApplicationDirector2() {
        Object module = model.getPRModule();
        assertThat(module, not(instanceOf(IPeople.class)));
    }
    
    @Test
    public void testApplicationDirector3() {
        Object module = model.getAccountingModule();
        assertThat(module, instanceOf(IAccounting.class));
    }
    
    @Test
    public void testApplicationDirector4() {
        Object module = model.getGravesModule();
        assertThat(module, not(instanceOf(IAccounting.class)));
    }
    

}
