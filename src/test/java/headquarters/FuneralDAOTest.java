package headquarters;

import headquarters.model.Model;
import headquarters.model.db.FuneralDAO;
import headquarters.model.db.IDatabase;
import headquarters.model.entities.Funeral;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import people.person.Corpse;

/**
 *
 * @author etruska
 */
public class FuneralDAOTest {

    Model model;
    FuneralDAO funeralDAO;
    IDatabase mockDb;
    List<Corpse> corpses;
    List<Funeral> funerals = new ArrayList<Funeral>();

    @Before
    public void setUp() {
        this.funerals = Funerals.getFunerals();
        this.model = mock(Model.class, RETURNS_DEEP_STUBS);
        this.mockDb = mock(IDatabase.class, RETURNS_DEEP_STUBS);
        this.funeralDAO = new FuneralDAO(model, mockDb);
    }

    @Test
    public void testGetAllFunerals() {

        try {
            List<Funeral> list = new ArrayList<>();

            Funeral f = funerals.get(0);
            list.add(f);

            when(model.getPeopleModule().getCorpseByID(anyInt())).thenReturn(f.getCorpse());

            ResultSet resultSetMock = mock(ResultSet.class);

            when(resultSetMock.next()).thenReturn(true).thenReturn(false);

            when(resultSetMock.getInt(1)).thenReturn(f.getId());
            when(resultSetMock.getString(2)).thenReturn(f.getPlace());
            when(resultSetMock.getInt(3)).thenReturn(f.getGuestCount());
            when(resultSetMock.getBoolean(4)).thenReturn(f.isDecoration());
            when(resultSetMock.getInt(5)).thenReturn(f.getCorpse().getCorpseID());
            when(resultSetMock.getDate(6)).thenReturn(new java.sql.Date(f.getDate().getTime()));

            when(mockDb.getConnection().prepareStatement(anyString()).executeQuery()).thenReturn(resultSetMock);

            assertEquals(list, funeralDAO.getAllFunerals());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddFuneral() {
        Funeral f = funerals.get(0);
        assertTrue(funeralDAO.addFuneral(f));
    }
    
    @Test
    public void testDeleteFuneral() {
        Funeral f = funerals.get(0);
        assertTrue(funeralDAO.deleteFuneral(f));
    }
    
    @Test
    public void testEditFuneral() {
        Funeral f = funerals.get(0);
        assertTrue(funeralDAO.editFuneral(f));
    }

}
