/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package headquarters;

import headquarters.controller.HeadquartersController;
import headquarters.model.Model;
import headquarters.model.db.FuneralDAO;
import headquarters.model.entities.Funeral;
import headquarters.view.AppLayout;
import headquarters.view.FuneralDialog;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import people.person.Corpse;

/**
 *
 * @author etruska
 */
public class HeadquartersControllerTest {

    Model model;
    FuneralDAO mockDAO;
    FuneralDialog mockDialog;
    AppLayout mockLayout;
    HeadquartersController controller;
    List<Corpse> corpses;
    List<Funeral> funerals;
    Date funeralDate;

    @Before
    public void setUp() {
        this.model = new Model();
        this.mockDAO = mock(FuneralDAO.class);
        this.mockLayout = mock(AppLayout.class);
        this.model.setFuneralDAO(mockDAO);
        this.funerals = Funerals.getFunerals();

        this.controller = new HeadquartersController(model);
        this.controller.setAppLayout(mockLayout);
        doNothing().when(mockLayout).showStatus(anyString(), anyInt());
        this.corpses = Corpses.getCorpses();

        this.mockDialog = mock(FuneralDialog.class);

        String startDateString = "06/27/2007";
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        try {
            funeralDate = df.parse(startDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddFuneral() {
        this.controller.addFuneralFormSubmitted(this.mockDialog, corpses.get(0), "Prague", "100", true, this.funeralDate);
        Funeral targetFuneral = new Funeral(corpses.get(0), "Prague", 100, true, funeralDate);
        verify(mockDAO).addFuneral(targetFuneral);
    }

    @Test
    public void testAddFuneral2() {
        this.controller.addFuneralFormSubmitted(this.mockDialog, corpses.get(0), "Prague", "100", true, this.funeralDate);
        Funeral targetFuneral = new Funeral(corpses.get(0), "London", 200, false, funeralDate);
        verify(mockDAO, never()).addFuneral(targetFuneral);
    }

    @Test
    public void testEditFuneral() {
        this.controller.editFuneralFormSubmitted(this.mockDialog, corpses.get(0), "Prague", "100", true, this.funeralDate, 55);
        Funeral targetFuneral = new Funeral(corpses.get(0), "Prague", 100, true, funeralDate);
        targetFuneral.setId(55);
        verify(mockDAO).editFuneral(targetFuneral);
    }

    @Test
    public void testEditFuneral2() {
        this.controller.editFuneralFormSubmitted(this.mockDialog, corpses.get(0), "Prague", "100", true, this.funeralDate, 55);
        Funeral targetFuneral = new Funeral(corpses.get(0), "London", 200, false, funeralDate);
        targetFuneral.setId(55);
        verify(mockDAO, never()).editFuneral(targetFuneral);
    }

    @Test
    public void testDelete() {
        Funeral targetFuneral = new Funeral(corpses.get(0), "London", 200, false, funeralDate);
        targetFuneral.setId(55);
        this.controller.btnDeleteFuneralPressed(targetFuneral);
        verify(mockDAO).deleteFuneral(targetFuneral);
    }

}
