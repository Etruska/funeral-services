/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package accounting;

import AccountingModel.AnnualReport;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class AnnualReportsMock {
    public static List<AnnualReport> getAnnualReports() {
        List<AnnualReport> list = new ArrayList<>();
        list.add(new AnnualReport("Výroční zpráva 2011", "Vážení obchodní přátelé"));
        list.add(new AnnualReport("Výroční zpráva 2012", "Vážení pozůstalí"));
        list.add(new AnnualReport("Výroční zpráva 2013", "Milí nebožtíci"));
        return list;
    }    
}
