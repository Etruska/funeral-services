/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accounting;

import AccountingController.Accounting;
import AccountingModel.Constants;
import AccountingModel.TextDAO;
import AccountingView.AddTransaction;
import headquarters.controller.IHeadquarters;
import headquarters.model.db.Database;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Václav
 */
public class AccountingTest {

    Accounting controller;
    IHeadquarters mockHQ;
    AddTransaction addAdView;
    TextDAO textdao;
    ResultSet mockRS;
    Database mockDB;
    Connection mockConn;
    Statement mockStatement;

    @Before
    public void setUp() {
        mockHQ = mock(IHeadquarters.class);
        mockDB = mock(Database.class);
        mockRS = mock(ResultSet.class);
        mockConn = mock(Connection.class);
        mockStatement = mock(Statement.class);
        when(mockHQ.getDatabase()).thenReturn(mockDB);
        when(mockHQ.getDatabase().getConnection()).thenReturn(mockConn);
        try {
            when(mockStatement.executeQuery(anyString())).thenReturn(mockRS);
        } catch (SQLException ex) {
            Logger.getLogger(MenuTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            when(mockConn.createStatement()).thenReturn(mockStatement);
        } catch (SQLException ex) {
            Logger.getLogger(MenuTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        controller = new Accounting(mockHQ);        
        addAdView = new AddTransaction(controller);
        textdao = new TextDAO(mockHQ);
    }

    

//    @Test
//    public void testMenuSelected() {
//        controller.menuSelected(new ActionEvent(this, 0, null), Constants.ADD_ANNUALREPORT);
//        assertEquals(2, controller.getContentPanels().size());
//        controller.menuSelected(new ActionEvent(this, 0, null), Constants.LIST_ANNUALREPORTS);
//        assertEquals(3, controller.getContentPanels().size());
//        controller.menuSelected(new ActionEvent(this, 0, null), Constants.ADD_TRANSACTION);
//        assertEquals(4, controller.getContentPanels().size());
//        controller.menuSelected(new ActionEvent(this, 0, null), Constants.LIST_TRANSACTIONS);
//        assertEquals(5, controller.getContentPanels().size());
//        controller.menuSelected(new ActionEvent(this, 0, null), Constants.ADD_BALANCESHEET);
//        assertEquals(6, controller.getContentPanels().size());
//        controller.menuSelected(new ActionEvent(this, 0, null), Constants.LIST_BALANCESHEETS);
//        assertEquals(7, controller.getContentPanels().size());
//    }
}
