/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package accounting;

import AccountingModel.AnnualReport;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Václav
 */
public class AnnualReportTest {
    AnnualReport annualreport;
    
    @Before
    public void setUp() {
        annualreport = new AnnualReport("Rok 2012", "Dobrý den");
    }
    
    @Test
    public void testGetTitle() {
        assertEquals("Rok 2012", annualreport.getTitle());
    }
    
    @Test
    public void testGetText() {
        assertEquals("Dobrý den", annualreport.getText());
    }
    
    @Test
    public void testSetText() {
        annualreport.setText("Rok 2048");
        assertEquals("Rok 2048", annualreport.getText());
    }
    
    @Test
    public void testSetTitle() {
        annualreport.setTitle("Ahoj ahoj");
        assertEquals("Ahoj ahoj", annualreport.getTitle());
    }
}
