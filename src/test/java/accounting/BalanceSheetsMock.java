/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accounting;

import AccountingModel.BalanceSheets;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Václav
 */
public class BalanceSheetsMock {

    public static List<BalanceSheets> getNews() {
        List<BalanceSheets> list = new ArrayList<>();
        list.add(new BalanceSheets("Rozvaha1", "Vlastní a cizí kapitál"));
        list.add(new BalanceSheets("Rozvaha2", "Informace o závazcích"));
        list.add(new BalanceSheets("Rozvaha42", "Seznam věřitelů..."));
        return list;
    }
}
