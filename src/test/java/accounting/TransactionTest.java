/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package accounting;

import AccountingModel.Transaction;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Václav
 */
public class TransactionTest {
    Transaction transaction;
    
    @Before
    public void setUp() {
        transaction = new Transaction("7");
    }
   
    @Test
    public void testGetText() {
        assertEquals("7", transaction.getText());
    }
    
    @Test
    public void testSetText() {
        transaction.setText("-42");
        assertEquals("-42", transaction.getText());
    }
}
