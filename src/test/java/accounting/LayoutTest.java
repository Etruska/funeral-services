/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package accounting;


import AccountingController.Accounting;
import AccountingModel.TextDAO;
import AccountingView.AddAnnualReport;
import AccountingView.AddBalanceSheet;
import AccountingView.AddTransaction;
import AccountingView.ListAnnualReports;
import AccountingView.ListBalanceSheets;
import AccountingView.ListTransactions;
import AccountingView.Menu;
import headquarters.controller.IHeadquarters;
import headquarters.model.db.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Václav
 */
public class LayoutTest {
    Accounting controller;
    IHeadquarters mockHQ;
    AddTransaction addTransactionView;  
    TextDAO dao;
    ResultSet mockRS;
    Database mockDB;
    Connection mockConn;
    Statement mockStatement;
    Menu menu;

    @Before
    public void setUp() {
        mockHQ = mock(IHeadquarters.class);
        mockDB = mock(Database.class);
        mockRS = mock(ResultSet.class);
        mockConn = mock(Connection.class);
        mockStatement = mock(Statement.class);
        when(mockHQ.getDatabase()).thenReturn(mockDB);
        when(mockHQ.getDatabase().getConnection()).thenReturn(mockConn);
        try {
            when(mockStatement.executeQuery(anyString())).thenReturn(mockRS);
        } catch (SQLException ex) {
            Logger.getLogger(MenuTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            when(mockConn.createStatement()).thenReturn(mockStatement);
        } catch (SQLException ex) {
            Logger.getLogger(MenuTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        controller = new Accounting(mockHQ);        
        addTransactionView = new AddTransaction(controller);
        dao = new TextDAO(mockHQ);
        menu = new Menu(controller);
    }
    
//    @Test
//    public void testContentPanelExists() {
//        assertTrue(controller.contentPanelExists("Accounting/View"));
//    }
//    
    @Test
    public void creationOK() {
        AddTransaction addTransaction = new AddTransaction(controller);
        AddAnnualReport addAnnualReport = new AddAnnualReport(controller);        
        AddBalanceSheet addBalanceSheet = new AddBalanceSheet(controller);
        ListTransactions listTransactions = new ListTransactions(controller);
        ListAnnualReports listAnnualReports = new ListAnnualReports(controller);        
        ListBalanceSheets listBalanceSheets = new ListBalanceSheets(controller);
        assertTrue(true);
    }
}
