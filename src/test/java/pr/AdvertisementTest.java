/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pr;

import PR.Model.Advertisement;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Filip Vondrasek
 */
public class AdvertisementTest {
    Advertisement ad;
    
    @Before
    public void setUp() {
        ad = new Advertisement("Reklama");
    }
   
    @Test
    public void testGetText() {
        assertEquals("Reklama", ad.getText());
    }
    
    @Test
    public void testSetText() {
        ad.setText("Nova Reklama");
        assertEquals("Nova Reklama", ad.getText());
    }
}
