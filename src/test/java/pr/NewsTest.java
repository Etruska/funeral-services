/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pr;

import PR.Model.News;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Asus
 */
public class NewsTest {
    News news;
    
    @Before
    public void setUp() {
        news = new News("Titulek", "Telo");
    }
    
    @Test
    public void testGetTitle() {
        assertEquals("Titulek", news.getTitle());
    }
    
    @Test
    public void testGetText() {
        assertEquals("Telo", news.getText());
    }
    
    @Test
    public void testSetText() {
        news.setText("Nove Telo");
        assertEquals("Nove Telo", news.getText());
    }
    
    @Test
    public void testSetTitle() {
        news.setTitle("Novy Titulek");
        assertEquals("Novy Titulek", news.getTitle());
    }    
}
