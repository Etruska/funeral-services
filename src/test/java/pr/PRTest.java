/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr;


import PR.Controller.PR;
import PR.Model.CompanyMeeting;
import PR.Model.Constants;
import PR.Model.DAO;
import PR.Model.Model;
import PR.View.AddAd;
import headquarters.controller.IHeadquarters;
import headquarters.model.db.Database;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Filip Vondrasek
 */
public class PRTest {

    PR controller;
    IHeadquarters mockHQ;
    AddAd addAdView;
    Model model;
    DAO dao;
    ResultSet mockRS;
    Database mockDB;
    Connection mockConn;
    Statement mockStatement;

    @Before
    public void setUp() {
        mockHQ = mock(IHeadquarters.class);
        mockDB = mock(Database.class);
        mockRS = mock(ResultSet.class);
        mockConn = mock(Connection.class);
        mockStatement = mock(Statement.class);
        when(mockHQ.getDatabase()).thenReturn(mockDB);
        when(mockHQ.getDatabase().getConnection()).thenReturn(mockConn);
        try {
            when(mockStatement.executeQuery(anyString())).thenReturn(mockRS);
        } catch (SQLException ex) {
            Logger.getLogger(ModelTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            when(mockConn.createStatement()).thenReturn(mockStatement);
        } catch (SQLException ex) {
            Logger.getLogger(ModelTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        controller = new PR(mockHQ);
        model = new Model(controller);
        addAdView = new AddAd(controller, model);
        dao = new DAO(mockHQ);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddCompanyMeeting() {
        controller.addCompanyMeeting(new ActionEvent(this, 0, null), new CompanyMeeting("", ""));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddText() {
        controller.addText(new ActionEvent(this, 0, null), "", "", Constants.ARTICLE);
    }

    @Test
    public void testMenuSelected() {
        controller.menuSelected(new ActionEvent(this, 0, null), Constants.ADD_ARTICLE);
        assertEquals(2, controller.getContentPanels().size());
        controller.menuSelected(new ActionEvent(this, 0, null), Constants.LIST_ARTICLES);
        assertEquals(3, controller.getContentPanels().size());
        controller.menuSelected(new ActionEvent(this, 0, null), Constants.ADD_AD);
        assertEquals(4, controller.getContentPanels().size());
        controller.menuSelected(new ActionEvent(this, 0, null), Constants.LIST_ADS);
        assertEquals(5, controller.getContentPanels().size());
        controller.menuSelected(new ActionEvent(this, 0, null), Constants.ADD_NEWS);
        assertEquals(6, controller.getContentPanels().size());
        controller.menuSelected(new ActionEvent(this, 0, null), Constants.LIST_NEWS);
        assertEquals(7, controller.getContentPanels().size());
        controller.menuSelected(new ActionEvent(this, 0, null), Constants.ADD_MEETING);
        assertEquals(8, controller.getContentPanels().size());
        controller.menuSelected(new ActionEvent(this, 0, null), Constants.LIST_MEETINGS);
        assertEquals(9, controller.getContentPanels().size());
        controller.menuSelected(new ActionEvent(this, 0, null), Constants.ADD_ARTICLE);
        assertEquals(9, controller.getContentPanels().size());
    }
}
