/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pr;

import PR.Model.Article;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Filip Vondrasek
 */
public class ArticleTest {
    Article article;
    
    @Before
    public void setUp() {
        article = new Article("Titulek", "Telo");
    }
    
    @Test
    public void testGetTitle() {
        assertEquals("Titulek", article.getTitle());
    }
    
    @Test
    public void testGetText() {
        assertEquals("Telo", article.getText());
    }
    
    @Test
    public void testSetText() {
        article.setText("Nove Telo");
        assertEquals("Nove Telo", article.getText());
    }
    
    @Test
    public void testSetTitle() {
        article.setTitle("Novy Titulek");
        assertEquals("Novy Titulek", article.getTitle());
    }
}
