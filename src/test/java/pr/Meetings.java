/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pr;

import PR.Model.CompanyMeeting;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class Meetings {
    public static List<CompanyMeeting> getMeetings() {
        List<CompanyMeeting> list = new ArrayList<>();
        list.add(new CompanyMeeting("2012-12-12", "Filip, Vojta, Kuba"));
        list.add(new CompanyMeeting("2012-12-24", "Filip, Stromecek, Kapr, Darky"));
        list.add(new CompanyMeeting("2012-12-31", "Kubik, Toluen, Krabicak"));
        return list;
    }
}
