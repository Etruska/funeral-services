/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pr;

import PR.Model.Article;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class Articles {
    public static List<Article> getArticles() {
        List<Article> list = new ArrayList<>();
        list.add(new Article("Bezva clanek", "Filip, Vojta, Kuba"));
        list.add(new Article("Bezva clanek2", "Filip, Stromecek, Kapr, Darky"));
        list.add(new Article("Bezva clanek3", "Kubik, Toluen, Krabicak"));
        return list;
    }    
}
