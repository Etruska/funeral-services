/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr;

import PR.Controller.PR;
import PR.Model.Advertisement;
import PR.Model.Article;
import PR.Model.CompanyMeeting;
import PR.Model.DAO;
import PR.Model.Model;
import PR.Model.News;
import PR.View.AddAd;
import headquarters.controller.IHeadquarters;
import headquarters.model.db.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Filip Vondrasek
 */
public class ModelTest {

    PR controller;
    IHeadquarters mockHQ;
    AddAd addAdView;
    Model model;
    DAO dao;
    ResultSet mockRS;
    Database mockDB;
    Connection mockConn;
    Statement mockStatement;

    @Before
    public void setUp() {
        mockHQ = mock(IHeadquarters.class);
        mockDB = mock(Database.class);
        mockRS = mock(ResultSet.class);
        mockConn = mock(Connection.class);
        mockStatement = mock(Statement.class);
        when(mockHQ.getDatabase()).thenReturn(mockDB);
        when(mockHQ.getDatabase().getConnection()).thenReturn(mockConn);
        try {
            when(mockStatement.executeQuery(anyString())).thenReturn(mockRS);
        } catch (SQLException ex) {
            Logger.getLogger(ModelTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            when(mockConn.createStatement()).thenReturn(mockStatement);
        } catch (SQLException ex) {
            Logger.getLogger(ModelTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        controller = new PR(mockHQ);
        model = new Model(controller);
        addAdView = new AddAd(controller, model);
        dao = new DAO(mockHQ);
    }

    @Test
    public void testGetMeetings() {
        List<CompanyMeeting> meetings = new ArrayList<>();

        CompanyMeeting meeting = Meetings.getMeetings().get(0);
        meetings.add(meeting);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed = new Date();
        try {
            parsed = format.parse(meeting.getDate());
        } catch (ParseException ex) {
            Logger.getLogger(ModelTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            when(mockRS.next()).thenReturn(true).thenReturn(false);
            when(mockRS.getDate(1)).thenReturn(new java.sql.Date(parsed.getTime()));
            when(mockRS.getString(2)).thenReturn(meeting.getAttendees());
        } catch (SQLException ex) {
            Logger.getLogger(ModelTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<CompanyMeeting> returnedMeetings = model.getMeetingsFromDB();
        assertEquals(meetings, returnedMeetings);
    }

    @Test
    public void testGetArticles() {
        List<Article> articles = new ArrayList<>();

        Article article = Articles.getArticles().get(0);
        articles.add(article);
        try {
            when(mockRS.next()).thenReturn(true).thenReturn(false);
            when(mockRS.getString(1)).thenReturn(article.getTitle());
            when(mockRS.getString(2)).thenReturn(article.getText());
        } catch (SQLException ex) {
            Logger.getLogger(ModelTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<Article> returnedArticles = model.getArticlesFromDB();
        assertEquals(articles, returnedArticles);
    }

    @Test
    public void testGetNews() {
        List<News> newsList = new ArrayList<>();

        News news = NewsMock.getNews().get(0);
        newsList.add(news);
        try {
            when(mockRS.next()).thenReturn(true).thenReturn(false);
            when(mockRS.getString(1)).thenReturn(news.getTitle());
            when(mockRS.getString(2)).thenReturn(news.getText());
        } catch (SQLException ex) {
            Logger.getLogger(ModelTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<News> returnedNews = model.getNewsFromDB();
        assertEquals(newsList, returnedNews);
    }

    @Test
    public void testGetAds() {
        List<Advertisement> ads = new ArrayList<>();

        Advertisement ad = Advertisements.getAdvertisements().get(0);
        ads.add(ad);
        try {
            when(mockRS.next()).thenReturn(true).thenReturn(false);
            when(mockRS.getString(1)).thenReturn(ad.getText());
        } catch (SQLException ex) {
            Logger.getLogger(ModelTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<Advertisement> returnedAdvertisements = model.getAdsFromDB();
        assertEquals(ads, returnedAdvertisements);
    }
}
