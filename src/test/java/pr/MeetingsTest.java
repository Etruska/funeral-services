/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pr;

import PR.Model.CompanyMeeting;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Asus
 */
public class MeetingsTest {
    CompanyMeeting meeting;
    
    @Before
    public void setUp() {
        meeting = new CompanyMeeting("2013-12-24", "Filip");
    }
    
    @Test
    public void testGetDate() {
        assertEquals("2013-12-24", meeting.getDate());
    }
    
    @Test
    public void testGetAttendees() {
        assertEquals("Filip", meeting.getAttendees());
    }
    
    @Test
    public void testSetDate() {
        meeting.setDate("2013-12-12");
        assertEquals("2013-12-12", meeting.getDate());
    }
    
    @Test
    public void testSetAttendees() {
        meeting.setAttendees("Vojta");
        assertEquals("Vojta", meeting.getAttendees());
    }    
}
