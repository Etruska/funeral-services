/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pr;

import PR.Model.News;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class NewsMock {

    public static List<News> getNews() {
        List<News> list = new ArrayList<>();
        list.add(new News("Bezva clanek", "Filip, Vojta, Kuba"));
        list.add(new News("Bezva clanek2", "Filip, Stromecek, Kapr, Darky"));
        list.add(new News("Bezva clanek3", "Kubik, Toluen, Krabicak"));
        return list;
    }
}
