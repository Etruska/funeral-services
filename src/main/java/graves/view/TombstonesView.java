/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.view;

import graves.controller.GravesController;
import graves.daos.DAOManager;
import graves.model.Material;
import graves.model.Storeroom;
import graves.model.Tombstone;
import graves.view.datamodels.TombstonesTable;
import headquarters.view.AppLayout;
import headquarters.view.View;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author jiri
 */
public class TombstonesView  extends View implements ActionListener, IObserver{
    GravesController c;
    JTextField nametext;
    JLabel label, matlabel, storeroomlabel, textlabel;
    JButton send;
    TombstonesTable dataModel;
    JTable table;
    JScrollPane scrollpane;
    JComboBox materialscb, storeroomscb;
    
    @Override
    public void update()
    {
        this.storeroomscb.removeAllItems();
        for (Storeroom sr: this.c.getStorerooms())
        {
            this.storeroomscb.addItem(sr);
        }
        this.materialscb.removeAllItems();
        for (Material m: this.c.getMaterials())
        {
            this.materialscb.addItem(m);
        }
        this.dataModel.setTombstonesList(c.getTombstones());
        this.dataModel.fireTableDataChanged();
    }
    
    @Override
    public String getViewName() {
        return "Graves/Tombstones";
    }
    
    @Override
    public void actionPerformed(ActionEvent event)
    {
        if (event.getSource() == this.send)
        {
            Storeroom storeroom = (Storeroom)this.storeroomscb.getSelectedItem();
            Tombstone tombstone = new Tombstone(null, (Material)this.materialscb.getSelectedItem(), this.nametext.getText(), storeroom); 
            this.c.getHq().showStatus("Přidán náhrobek " + this.c.addTombstone(tombstone), AppLayout.STATUS_OK);
        }
    }
    
    public TombstonesView(GravesController c, DAOManager m) {
        this.c = c;
        this.setLayout(new GridBagLayout());
        
        GridBagConstraints gc = new GridBagConstraints();
        m.registerObserver(this);
        label = new JLabel("Přidat nový náhrobek.");
        matlabel = new JLabel("Zvolte materiál");
        storeroomlabel = new JLabel("Zvolte sklad");
        textlabel = new JLabel("Zvolte text náhrobku");
        materialscb = new JComboBox(c.getMaterials().toArray());
        storeroomscb = new JComboBox(c.getStorerooms().toArray());
        nametext = new JTextField(20);
        send = new JButton("Přidat");
        send.addActionListener(this);
        this.dataModel = new TombstonesTable();
        this.dataModel.setTombstonesList(this.c.getTombstones());
        
        this.table = new JTable(this.dataModel);
        this.table.setPreferredScrollableViewportSize(new Dimension(200, 200));
        this.scrollpane = new JScrollPane(this.table);
       
        
                
        gc.fill = GridBagConstraints.HORIZONTAL;
        gc.weightx = 0.5;
        
        
        gc.gridx = 0;
        gc.gridy = 0;
        gc.gridwidth = 3;
        this.add(this.scrollpane, gc);
        gc.gridwidth = 1;
        
        gc.gridx = 0;
        gc.gridy = 1;
        this.add(textlabel, gc);
        gc.gridx = 2;
        gc.gridy = 1;
        this.add(nametext, gc);
        gc.gridx = 0;
        gc.gridy = 2;
        this.add(matlabel, gc);
        gc.gridx = 2;
        gc.gridy = 2;
        this.add(materialscb, gc);
        gc.gridx = 0;
        gc.gridy = 3;
        this.add(storeroomlabel, gc);
        gc.gridx = 2;
        gc.gridy = 3;
        this.add(storeroomscb, gc);
        gc.gridx = 0;
        gc.gridy = 4;
        this.add(label, gc);
        gc.gridx = 2;
        gc.gridy = 4;
        this.add(send, gc);
    }
}
