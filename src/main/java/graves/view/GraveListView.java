/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package graves.view;

import graves.view.datamodels.GraveTable;
import graves.controller.GravesController;
import graves.daos.DAOManager;
import graves.model.Coffin;
import graves.model.Grave;
import graves.model.Graveyard;
import graves.model.Tombstone;
import headquarters.view.*;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author jiri
 */
public class GraveListView extends View implements ActionListener, IObserver{

    GravesController c;
    JLabel label, tombstoneslb, coffinslb, graveyardslb;
    JComboBox tombstonescb, coffinscb, graveyardscb;
    JButton send;
    GraveTable dataModel;
    JTable table;
    JScrollPane scrollpane;
    
    
    public void update()
    {
        this.tombstonescb.removeAllItems();
        for (Tombstone t: this.c.getTombstones())
        {
            this.tombstonescb.addItem(t);
        }
        
        this.graveyardscb.removeAllItems();
        for (Graveyard g: this.c.getGraveyards())
        {
            this.graveyardscb.addItem(g);
        }
        
        this.coffinscb.removeAllItems();
        for (Coffin t: this.c.getCoffins())
        {
            this.coffinscb.addItem(t);
        }
        this.dataModel.setGraveList(c.getGraves());
        this.dataModel.fireTableDataChanged();
    }
    
    @Override
    public void actionPerformed(ActionEvent event)
    {
        if (event.getSource() == this.send)
        {
            Grave grave = new Grave(null, (Tombstone)this.tombstonescb.getSelectedItem(), (Coffin)this.coffinscb.getSelectedItem(), (Graveyard)this.graveyardscb.getSelectedItem());
            this.c.getHq().showStatus("Přidán hrob " + this.c.addGrave(grave), AppLayout.STATUS_OK);
        }
    }
    
    public String getViewName() {
        return "Graves/GraveList";
    }
    
    public GraveListView(GravesController c, DAOManager m) {
        this.c = c;
        this.setLayout(new GridBagLayout());
        
        GridBagConstraints gc = new GridBagConstraints();
        
        
        m.registerObserver(this);
        label = new JLabel("Přidat nový hrob.");
        tombstonescb = new JComboBox(c.getTombstones().toArray());
        tombstoneslb =  new JLabel("Vyberte náhrobek.");
        coffinscb = new JComboBox(c.getCoffins().toArray());
        coffinslb =  new JLabel("Vyberte rakev.");
        graveyardscb = new JComboBox(c.getGraveyards().toArray());
        graveyardslb =  new JLabel("Vyberte hřbitov.");
        send = new JButton("Přidat");
        send.addActionListener(this);
        
        this.dataModel = new GraveTable();
        this.dataModel.setGraveList(this.c.getGraves());
        
        this.table = new JTable(this.dataModel);
        this.table.setPreferredScrollableViewportSize(new Dimension(200, 200));
        this.scrollpane = new JScrollPane(this.table);
        

        gc.fill = GridBagConstraints.BOTH;
        gc.weightx = 0.5;
        
        
        gc.gridx = 0;
        gc.gridy = 0;
        gc.gridwidth = 3;
        this.add(this.scrollpane, gc);
        gc.gridwidth = 1;
        
        gc.gridx = 0;
        gc.gridy = 1;
        this.add(coffinslb, gc);
        gc.gridx = 2;
        gc.gridy = 1;
        this.add(coffinscb, gc);
        gc.gridx = 0;
        gc.gridy = 2;
        this.add(graveyardslb, gc);
        gc.gridx = 2;
        gc.gridy = 2;
        this.add(graveyardscb, gc);
        gc.gridx = 0;
        gc.gridy = 3;
        this.add(tombstoneslb, gc);
        gc.gridx = 2;
        gc.gridy = 3;
        this.add(tombstonescb, gc);
        gc.gridx = 0;
        gc.gridy = 4;
        this.add(label, gc);
        gc.gridx = 2;
        gc.gridy = 4;
        this.add(send, gc);
        
    }
    
}
