/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.view;

import graves.controller.GravesController;
import graves.daos.DAOManager;
import graves.model.Coffin;
import graves.model.Material;
import graves.model.Storeroom;
import graves.view.datamodels.CoffinsTable;
import headquarters.view.AppLayout;
import headquarters.view.View;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author jiri
 */
public class CoffinsView extends View implements ActionListener, IObserver {
    GravesController c;
    JTextField heighttext, widthtext;
    JLabel label, heightlabel, widthlabel, storeroomlabel, materiallabel;
    JButton send;
    CoffinsTable dataModel;
    JTable table;
    JScrollPane scrollpane;
    JComboBox storeroomcb, materialscb;
    
    @Override
    public void update()
    {
        this.storeroomcb.removeAllItems();
        for (Storeroom sr: this.c.getStorerooms())
        {
            this.storeroomcb.addItem(sr);
        }
        
        this.materialscb.removeAllItems();
        for (Material m: this.c.getMaterials())
        {
            this.materialscb.addItem(m);
        }
        this.dataModel.setCoffinsList(c.getCoffins());
        this.dataModel.fireTableDataChanged();
    }
    
    @Override
    public String getViewName() {
        return "Graves/Coffins";
    }
    
    @Override
    public void actionPerformed(ActionEvent event)
    {
        if (event.getSource() == this.send)
        {
            Storeroom storeroom = (Storeroom)this.storeroomcb.getSelectedItem();
            
           
            Coffin coffin = new Coffin(null, false, Integer.parseInt(this.heighttext.getText()), Integer.parseInt(this.widthtext.getText()), (Material)this.materialscb.getSelectedItem(), null, storeroom);
                this.c.getHq().showStatus("Rakev přidána " + this.c.addCoffin(coffin), AppLayout.STATUS_OK);
        }
    }
    
    public CoffinsView(GravesController c, DAOManager m) {
        this.c = c;
        this.setLayout(new GridBagLayout());
        
        GridBagConstraints gc = new GridBagConstraints();
        m.registerObserver(this);
        label = new JLabel("Přidat novou rakev.");
        heightlabel = new JLabel("Zadejte výšku.");
        widthlabel = new JLabel("Zadejte šířku.");
        storeroomlabel = new JLabel("Vyberte sklad.");
        materiallabel = new JLabel("Vyberte material.");
        heighttext = new JTextField(20);
        widthtext = new JTextField(20);
        storeroomcb =  new JComboBox(c.getStorerooms().toArray());
        materialscb =  new JComboBox(c.getMaterials().toArray());
        
        
        send = new JButton("Přidat");
        send.addActionListener(this);
        this.dataModel = new CoffinsTable();
        this.dataModel.setCoffinsList(this.c.getCoffins());
        
        this.table = new JTable(this.dataModel);
        this.table.setPreferredScrollableViewportSize(new Dimension(200, 200));
        this.scrollpane = new JScrollPane(this.table);
        
       
             
        
        gc.fill = GridBagConstraints.HORIZONTAL;
        gc.weightx = 0.5;
        
        
        gc.gridx = 0;
        gc.gridy = 0;
        gc.gridwidth = 3;
        this.add(this.scrollpane, gc);
        gc.gridwidth = 1;
        
        gc.gridx = 0;
        gc.gridy = 1;
        this.add(heightlabel, gc);
        gc.gridx = 2;
        gc.gridy = 1;
        this.add(heighttext, gc);
        gc.gridx = 0;
        gc.gridy = 2;
        this.add(widthlabel, gc);
        gc.gridx = 2;
        gc.gridy = 2;
        this.add(widthtext, gc);
        gc.gridx = 0;
        gc.gridy = 3;
        this.add(storeroomlabel, gc);
        gc.gridx = 2;
        gc.gridy = 3;
        this.add(storeroomcb, gc);
        gc.gridx = 0;
        gc.gridy = 4;
        this.add(materiallabel, gc);
        gc.gridx = 2;
        gc.gridy = 4;
        this.add(materialscb, gc);
        gc.gridx = 0;
        gc.gridy = 5;
        this.add(label, gc);
        gc.gridx = 2;
        gc.gridy = 5;
        this.add(send, gc);
    }
}
