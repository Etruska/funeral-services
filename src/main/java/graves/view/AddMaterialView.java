/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.view;

import graves.controller.GravesController;
import graves.daos.DAOManager;
import graves.model.Material;
import graves.view.datamodels.MaterialTable;
import headquarters.view.AppLayout;
import headquarters.view.View;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author jiri
 */
public class AddMaterialView extends View implements ActionListener, IObserver{
    GravesController c;
    JTextField nametext;
    JLabel label, textlabel;
    JButton send;
    MaterialTable dataModel;
    JTable table;
    JScrollPane scrollpane;
    
    @Override
    public void update()
    {
        this.dataModel.setMaterialList(c.getMaterials());
        this.dataModel.fireTableDataChanged();
    }
    
    @Override
    public String getViewName() {
        return "Graves/AddMaterial";
    }
    
    @Override
    public void actionPerformed(ActionEvent event)
    {
        if (event.getSource() == this.send)
        {
            Material material = new Material(null, this.nametext.getText());
            Integer i = this.c.addMaterial(material);
            if (i != null)
            {
                this.c.getHq().showStatus("Materiál přidán " + i, AppLayout.STATUS_OK);
            }
            else
            {
                this.c.getHq().showStatus("Materiál již existuje " + i, AppLayout.STATUS_ERROR);
            }
        }
    }
    
    public AddMaterialView(GravesController c, DAOManager m) {
        this.c = c;
        this.setLayout(new GridBagLayout());
        
        GridBagConstraints gc = new GridBagConstraints();
        m.registerObserver(this);
        label = new JLabel("Přidat nový material.");
        textlabel = new JLabel("Zadejte název nového materiálu.");
        nametext = new JTextField(20);
        send = new JButton("Přidat");
        send.addActionListener(this);
        this.dataModel = new MaterialTable();
        this.dataModel.setMaterialList(this.c.getMaterials());
        
        this.table = new JTable(this.dataModel);
        this.table.setPreferredScrollableViewportSize(new Dimension(200, 200));
        this.scrollpane = new JScrollPane(this.table);
        
        

        gc.fill = GridBagConstraints.HORIZONTAL;
        gc.weightx = 0.5;
        
        
        gc.gridx = 0;
        gc.gridy = 0;
        gc.gridwidth = 3;
        this.add(this.scrollpane, gc);
        gc.gridwidth = 1;
        
        gc.gridx = 0;
        gc.gridy = 1;
        this.add(textlabel, gc);
        gc.gridx = 2;
        gc.gridy = 1;
        this.add(nametext, gc);
        
        gc.gridx = 0;
        gc.gridy = 2;
        this.add(label, gc);
        gc.gridx = 2;
        gc.gridy = 2;
        this.add(send, gc);
    }
    
    
}
