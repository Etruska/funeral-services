/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.view.datamodels;

import graves.controller.GravesController;
import graves.model.Grave;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author jiri
 */
public class GraveTable extends AbstractTableModel {
    private ArrayList<Grave> list;
    
    public GraveTable()
    {
    }
    
    @Override
    public Object getValueAt (int x, int y)
    {
        switch(y)
        {
            case(0): return this.list.get(x).getGraveyard();
            case(1): return this.list.get(x).getTombstone();
            case(2): return this.list.get(x).getCoffin();
        }
        
         return "N/A";
    }
    
    @Override
    public int getColumnCount()
    {
        return 3;
    }
    
    @Override
    public int getRowCount()
    {
        return this.list.size();
    }
    
    @Override
    public String getColumnName(int column)
    {
        switch(column)
        {
            case(0): return "Hřbitov";
            case(1): return "Nápis na náhrobku";
            case(2): return "Rakev";
        }
        return "Grave";
    }
    
    public void setGraveList(ArrayList<Grave> list)
    {
        this.list = list;
    }
}
