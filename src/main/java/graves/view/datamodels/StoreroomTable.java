/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.view.datamodels;


import graves.model.Storeroom;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author jiri
 */
public class StoreroomTable extends AbstractTableModel {
    private ArrayList<Storeroom> list;
    
    public StoreroomTable()
    {
    }
    
    @Override
    public Object getValueAt (int x, int y)
    {
        switch(y)
        {
            case(0): return this.list.get(x).getName();
            case(1): return this.list.get(x).getAddress();
            case(2): return this.list.get(x).getCapacity();
        }
        
         return "N/A";
    }
    
    @Override
    public int getColumnCount()
    {
        return 3;
    }
    
    @Override
    public String getColumnName(int column)
    {
        switch(column)
        {
            case(0): return "Jméno";
            case(1): return "Adresa";
            case(2): return "Kapacita";
        }
        return "Sklady";
    }
    
    @Override
    public int getRowCount()
    {
        return this.list.size();
    }
    
    public void setStoreroomList(ArrayList<Storeroom> list)
    {
        this.list = list;
    }
    
}
