/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.view.datamodels;

import graves.controller.GravesController;
import graves.model.Grave;
import graves.model.Material;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author jiri
 */
public class MaterialTable extends AbstractTableModel {
    private ArrayList<Material> list;
    
    public MaterialTable()
    {
    }
    
    @Override
    public Object getValueAt (int x, int y)
    {
        return this.list.get(x).getName();
    }
    
    @Override
    public int getColumnCount()
    {
        return 1;
    }
    
    @Override
    public String getColumnName(int column)
    {
        return "Materiály";
    }
    
    @Override
    public int getRowCount()
    {
        return this.list.size();
    }
    
    public void setMaterialList(ArrayList<Material> list)
    {
        this.list = list;
    }
}
