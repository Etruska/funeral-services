/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.view.datamodels;


import graves.model.Tombstone;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author jiri
 */
public class TombstonesTable extends AbstractTableModel {
    private ArrayList<Tombstone> list;
    
    public TombstonesTable()
    {
    }
    
    @Override
    public Object getValueAt (int x, int y)
    {
        switch(y)
        {
            case(0): return this.list.get(x).getText();
            case(1): return this.list.get(x).getMaterial();
            case(2): return this.list.get(x).getStoreroom();
        }
        
         return "N/A";
    }
    
    @Override
    public int getColumnCount()
    {
        return 3;
    }
    
    @Override
    public String getColumnName(int column)
    {
        switch(column)
        {
            case(0): return "Text";
            case(1): return "Materiál";
            case(2): return "Sklad";
        }

        return "Náhrobky";
    }
    
    @Override
    public int getRowCount()
    {
        return this.list.size();
    }
    
    public void setTombstonesList(ArrayList<Tombstone> list)
    {
        this.list = list;
    }
}