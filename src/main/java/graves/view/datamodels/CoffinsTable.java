/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.view.datamodels;

import graves.controller.GravesController;
import graves.model.Coffin;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author jiri
 */
public class CoffinsTable  extends AbstractTableModel {
    private ArrayList<Coffin> list;
    
    public CoffinsTable()
    {
    }
    
    @Override
    public Object getValueAt (int x, int y)
    {
        switch(y)
        {
            case(1): return this.list.get(x).getHeight();
            case(0): return this.list.get(x).getMaterial();
            case(2): return this.list.get(x).getWidth();
            case(3): return this.list.get(x).getStoreroom();
                
        }
        
         return "N/A";
    }
    
    @Override
    public int getColumnCount()
    {
        return 4;
    }
    
    @Override
    public String getColumnName(int column)
    {
        switch(column)
        {
            case(1): return "Výška";
            case(0): return "Materiál";
            case(2): return "Šířka";
            case(3): return "Sklad";
        }

        return "Rakve";
    }
    
    @Override
    public int getRowCount()
    {
        return this.list.size();
    }
    
    public void setCoffinsList(ArrayList<Coffin> list)
    {
        this.list = list;
    }
}
