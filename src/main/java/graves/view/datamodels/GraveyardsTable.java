/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.view.datamodels;


import graves.model.Graveyard;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author jiri
 */
public class GraveyardsTable extends AbstractTableModel {
    
    private ArrayList<Graveyard> list;
    
    public GraveyardsTable()
    {
    }
    
    @Override
    public Object getValueAt (int x, int y)
    {
        switch(y)
        {
            case(0): return this.list.get(x).getName();
            case(1): return this.list.get(x).getAddress();
            case(2): return this.list.get(x).getCapacity();
        }
        
         return "N/A";
    }
    
    @Override
    public int getColumnCount()
    {
        return 3;
    }
    
    @Override
    public String getColumnName(int column)
    {
        switch(column)
        {
            case(0): return "Jméno";
            case(1): return "Adresa";
            case(2): return "Kapacita";
        }
        return "Hrbitovy";
    }
    
    @Override
    public int getRowCount()
    {
        return this.list.size();
    }
    
    public void setGraveyardsList(ArrayList<Graveyard> list)
    {
        this.list = list;
    }
    
}