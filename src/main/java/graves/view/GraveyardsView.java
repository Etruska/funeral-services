/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.view;

import graves.controller.GravesController;
import graves.daos.DAOManager;
import graves.model.Graveyard;
import graves.view.datamodels.GraveyardsTable;
import headquarters.view.AppLayout;
import headquarters.view.View;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author jiri
 */
public class GraveyardsView extends View implements ActionListener, IObserver{
    GravesController c;
    JTextField nametext, capacitytext, addresstext;
    JLabel label, caplabel, namelabel, addresslabel;
    JButton send;
    GraveyardsTable dataModel;
    JTable table;
    JScrollPane scrollpane;
    
    @Override
    public void update()
    {
        this.dataModel.setGraveyardsList(c.getGraveyards());
        this.dataModel.fireTableDataChanged();
    }
    
    @Override
    public String getViewName() {
        return "Graves/Graveyards";
    }
    
    @Override
    public void actionPerformed(ActionEvent event)
    {
        if (event.getSource() == this.send)
        {
            Graveyard graveyard = new Graveyard(null, this.nametext.getText(), this.addresstext.getText(), Integer.parseInt(this.capacitytext.getText()));
                this.c.getHq().showStatus("Přidán hřbitov " + this.c.addGraveyard(graveyard), AppLayout.STATUS_OK);
        }
    }
    
    public GraveyardsView(GravesController c, DAOManager m) {
        this.c = c;
        this.setLayout(new GridBagLayout());
        
        GridBagConstraints gc = new GridBagConstraints();
        m.registerObserver(this);
        label = new JLabel("Přidat nový hřbitov.");
        caplabel = new JLabel("Zadejte kapacitu.");
        namelabel = new JLabel("Zadejte jmeno.");
        addresslabel = new JLabel("Zadejte adresu.");
        nametext = new JTextField(20);
        addresstext = new JTextField(20);
        capacitytext = new JTextField(20);
        send = new JButton("Přidat");
        send.addActionListener(this);
        this.dataModel = new GraveyardsTable();
        this.dataModel.setGraveyardsList(c.getGraveyards());
        
        this.table = new JTable(this.dataModel);
        this.table.setPreferredScrollableViewportSize(new Dimension(200, 200));
        this.scrollpane = new JScrollPane(this.table);
        

        
                
                
        gc.fill = GridBagConstraints.HORIZONTAL;
        gc.weightx = 0.5;
        
        
        gc.gridx = 0;
        gc.gridy = 0;
        gc.gridwidth = 3;
        this.add(this.scrollpane, gc);
        gc.gridwidth = 1;
        
        gc.gridx = 0;
        gc.gridy = 1;
        this.add(namelabel, gc);
        gc.gridx = 2;
        gc.gridy = 1;
        this.add(nametext, gc);
        gc.gridx = 0;
        gc.gridy = 2;
        this.add(addresslabel, gc);
        gc.gridx = 2;
        gc.gridy = 2;
        this.add(addresstext, gc);
        gc.gridx = 0;
        gc.gridy = 3;
        this.add(caplabel, gc);
        gc.gridx = 2;
        gc.gridy = 3;
        this.add(capacitytext, gc);
        gc.gridx = 0;
        gc.gridy = 4;
        this.add(label, gc);
        gc.gridx = 2;
        gc.gridy = 4;
        this.add(send, gc);
    }
}
