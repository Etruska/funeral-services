/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.daos;

import graves.view.IObserver;
import java.sql.Connection;
import java.util.ArrayList;

/**
 *
 * @author jiri
 */
public class DAOManager {
    private static DAOManager instance;
    private static Connection connection;
    protected ArrayList<IObserver> observers;
    
    private ArrayList<AbstractDAO> daolist;
    private CoffinDAO coffin;
    private GraveDAO grave;
    private GraveyardDAO graveyard;
    private MaterialDAO material;
    private StoreroomDAO storeroom;
    private TombstoneDAO tombstone;

    
    public DAOManager()
    {
        this.daolist = new ArrayList<AbstractDAO>();
    }
    
    public static void setConnection(Connection conn)
    {
        if (connection == null)
        {
            connection = conn;
        }
    }
    public static DAOManager getInstance()
    {
        if (instance == null)
        {
            instance = new DAOManager();
        }
        return instance;
    }
    
    public GraveyardDAO graveyards()
    {
        if (graveyard == null)
        {
            graveyard = new GraveyardDAO(connection, this);
            daolist.add(graveyard);
        }
        return graveyard;
    }
    public CoffinDAO coffins()
    {
        if (coffin == null)
        {
            coffin = new CoffinDAO(connection, this);
            daolist.add(coffin);
        }
        return coffin;
    }
    
    public GraveDAO graves()
    {
        if (grave == null)
        {
            grave = new GraveDAO(connection, this);
            daolist.add(grave);
        }
        return grave;
    }
    
    public MaterialDAO materials()
    {
        if (material == null)
        {
            material = new MaterialDAO(connection, this);
            daolist.add(material);
        }
        return material;
    }
    
    public TombstoneDAO tombstones()
    {
        if (tombstone == null)
        {
            tombstone = new TombstoneDAO(connection, this);
            daolist.add(tombstone);
        }
        return tombstone;
    }
    
    public StoreroomDAO storerooms()
    {
        if (storeroom == null)
        {
            storeroom = new StoreroomDAO(connection, this);
            daolist.add(storeroom);
        }
        return storeroom;
    }
    
    public void notifyObservers()
    {
       for (IObserver a:observers)
        {
            a.update();
        }
    }
    
    public void registerObserver(IObserver o)
    {
        if (this.observers == null)
        {
            this.observers = new ArrayList<IObserver>();
        }
        this.observers.add(o);
    }
    
}
