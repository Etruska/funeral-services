/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.daos;

import graves.model.Grave;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author jiri
 */
public class GraveDAO extends AbstractDAO{
    
    public GraveDAO(Connection conn, DAOManager boss)
    {
        super(conn, boss);
    }
     
    public ArrayList<Grave> findAll()
    {
        ArrayList<Grave> list = new ArrayList<Grave>();
        try
        {
            ResultSet rs = this.query("SELECT GRAVE.* FROM GRAVE");
            while(rs.next())
            {
                list.add(new Grave(rs.getInt("graveid"), manager.tombstones().findTombstone(rs.getInt("tombstoneid")), manager.coffins().findCoffin(rs.getInt("coffinid")), manager.graveyards().findGraveyard(rs.getInt("graveyardid"))));
            }
            return list;
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
    
    
    
    public Grave findGrave(Integer id)
    {
        if (id == null)
            return null;
        try
        {
            ResultSet rs = this.query("SELECT GRAVE.* FROM GRAVES WHERE GRAVE.GRAVEID = " + id);
            if (rs.next())
            {
                return new Grave(rs.getInt("graveid"), manager.tombstones().findTombstone(rs.getInt("tombstoneid")), manager.coffins().findCoffin(rs.getInt("coffinid")), manager.graveyards().findGraveyard(rs.getInt("graveyardid")));
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
    
    
    
    public Integer countGraves(boolean onlyempty)
    {
        Integer count = 0;
        try
        {
        ResultSet rs = this.query("SELECT GRAVE.GRAVEID FROM GRAVE");
            while (rs.next()) 
            {
                if (onlyempty == false || rs.getBoolean("state") == Grave.EMPTY)
                {
                count++;
                }
            }
        }
        catch (Exception e)
        {
            System.out.println(e.toString());  
        }
        return count;
    }
    
    
    public Integer addGrave(Grave grave)
    {
        if (this.findGrave(grave.getId()) != null)
        {
            return null;
        }
        try
        {
            Integer ret = this.update("INSERT INTO GRAVE (TOMBSTONEID, COFFINID, GRAVEYARDID)" + 
                    "VALUES (" + grave.getTombstone().getId() + ", " + grave.getCoffin().getId() + ", " + grave.getGraveyard().getId() + ")", Statement.RETURN_GENERATED_KEYS);
            this.manager.notifyObservers();
            return ret;
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;  
    }
    
    public boolean deleteGrave(Grave grave)
    {
        if (grave == null || this.findGrave(grave.getId()) == null)
        {
            return false;
        }
        try
        {
           this.update("DELETE FROM GRAVE WHERE (GRAVEID=" + grave.getId() + ")", Statement.RETURN_GENERATED_KEYS);
           this.manager.notifyObservers();
           return true;
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return false;
    }
}
