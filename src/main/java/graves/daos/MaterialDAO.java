/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.daos;

import graves.model.Material;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author jiri
 */
public class MaterialDAO extends AbstractDAO{
    
    public MaterialDAO(Connection conn, DAOManager boss)
    {
        super(conn, boss);
    }
    
    public Material findMaterial(Integer id)
    {
        if (id == null)
            return null;
        try
        {
            ResultSet rs = this.query("SELECT MATERIAL.* FROM MATERIAL WHERE MATERIAL.MATERIALID = " + id);
            if (rs.next())
            {
                return new Material(rs.getInt("materialid"), rs.getString("name"));
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
    
    public ArrayList<Material> findAll()
    {
        ArrayList<Material> list = new ArrayList<Material>();
        try
        {
            ResultSet rs = this.query("SELECT MATERIAL.* FROM MATERIAL");
            while(rs.next())
            {
                list.add(new Material(rs.getInt("materialid"), rs.getString("name")));
            }
            return list;
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
     
    public Material findMaterial(String text)
    {
        try
        {
            ResultSet rs = this.query("SELECT MATERIAL.* FROM MATERIAL WHERE MATERIAL.NAME = '" + text + "'");
            if (rs.next())
            {
                return new Material(rs.getInt("materialid"), rs.getString("name"));
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
    
    public Integer addMaterial(Material material)
    {
        if (this.findMaterial(material.getName()) != null)
        {
            System.out.println("found already");
            return null;
        }
        try
        {
            Integer ret = this.update("INSERT INTO MATERIAL (NAME)" + 
                    "VALUES ('" +material.getName() +"')", Statement.RETURN_GENERATED_KEYS);
            this.manager.notifyObservers();
            return ret;
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;  
    }
}
