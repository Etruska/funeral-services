/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.daos;

import graves.model.Tombstone;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author jiri
 */
public class TombstoneDAO extends AbstractDAO{
    
    public TombstoneDAO(Connection conn, DAOManager boss)
    {
        super(conn, boss);
    }
    
    
    public ArrayList<Tombstone> findAll()
    {
        ArrayList<Tombstone> list = new ArrayList<Tombstone>();
        try
        {
            ResultSet rs = this.query("SELECT TOMBSTONE.* FROM TOMBSTONE");
            while(rs.next())
            {
                list.add(new Tombstone(rs.getInt("tombstoneid"), manager.materials().findMaterial(rs.getInt("materialid")), rs.getString("text"), manager.storerooms().findStoreroom(rs.getInt("storeroomid"))));
            }
            return list;
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
    
    public Tombstone findTombstone(Integer id)
    {
        if (id == null)
            return null;
        try
        {
            ResultSet rs = this.query("SELECT TOMBSTONE.* FROM TOMBSTONE WHERE TOMBSTONE.TOMBSTONEID = " + id);
            if (rs.next())
            {
                    return new Tombstone(rs.getInt("tombstoneid"), manager.materials().findMaterial(rs.getInt("materialid")), rs.getString("text"), manager.storerooms().findStoreroom(rs.getInt("storeroomid")));
            }
            else
            {
                    return null;
            }
        }
            
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
    
    public ArrayList<Tombstone> findTombstones(String text)
    {
        ArrayList<Tombstone> list = new ArrayList<Tombstone>();
        try
        {
            ResultSet rs = this.query("SELECT TOMBSTONE.* FROM TOMBSTONE WHERE TOMBSTONE.TEXT= '" + text +"'");
            while (rs.next())
            {
                    list.add(new Tombstone(rs.getInt("tombstoneid"), manager.materials().findMaterial(rs.getInt("materialid")), rs.getString("text"), manager.storerooms().findStoreroom(rs.getInt("storeroomid"))));
            }
            return list;
        }
            
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
    
    
    public Integer addTombstone(Tombstone tombstone)
    {
        if (this.findTombstone(tombstone.getId()) != null)
        {
            return null;
        }
        try
        {
            Integer ret = this.update("INSERT INTO TOMBSTONE (TEXT, MATERIALID, STOREROOMID)" + 
                    "VALUES ('" + tombstone.getText() +"'," + tombstone.getMaterial().getId() + ", " + tombstone.getStoreroomId() + ")", Statement.RETURN_GENERATED_KEYS);
            this.manager.notifyObservers();
            return ret;
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;  
    }
          
    
}
