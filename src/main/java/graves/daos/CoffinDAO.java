/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.daos;

import graves.model.Coffin;
import graves.model.Grave;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author jiri
 */
public class CoffinDAO extends AbstractDAO{
    private static CoffinDAO instance;
    
    public CoffinDAO(Connection conn, DAOManager boss)
    {
        super(conn, boss);
    }
  
    
    public ArrayList<Coffin> findAll()
    {
        ArrayList<Coffin> list = new ArrayList<Coffin>();
        try
        {
            ResultSet rs = this.query("SELECT COFFIN.* FROM COFFIN");
            while(rs.next())
            {
                list.add(new Coffin(rs.getInt("coffinid"), rs.getBoolean("state"), rs.getInt("height"), rs.getInt("width"), manager.materials().findMaterial(rs.getInt("materialid")), null, manager.storerooms().findStoreroom(rs.getInt("storeroomid"))));
            }
            return list;
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
    
    public Coffin findCoffin(Integer id)
    {
        if (id == null)
            return null;
        try
        {
            ResultSet rs = this.query("SELECT COFFIN.* FROM COFFIN WHERE COFFIN.COFFINID = " + id);
            if (rs.next())
            {
                return new Coffin(rs.getInt("coffinid"), rs.getBoolean("state"), rs.getInt("height"), rs.getInt("width"), manager.materials().findMaterial(rs.getInt("materialid")), null, manager.storerooms().findStoreroom(rs.getInt("storeroomid")));
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
    
    public Integer addCoffin(Coffin coffin)
    {
        if (this.findCoffin(coffin.getId()) != null)
        {
            return null;
        }
        try
        {
            Integer ret = this.update("INSERT INTO COFFIN (STATE, HEIGHT, WIDTH, CORPSEID, MATERIALID, STOREROOMID)" + 
                    "VALUES ('" + coffin.getState() +"',"  + coffin.getHeight() + ", " +coffin.getWidth() + ", " + coffin.getCorpseId() + ", " + coffin.getMaterial().getId() + ", " + coffin.getStoreroomId() + ")", Statement.RETURN_GENERATED_KEYS);
            this.manager.notifyObservers();
            return ret;
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;  
    }
}
