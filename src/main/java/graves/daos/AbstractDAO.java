/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.daos;

import graves.model.Grave;
import graves.view.IObserver;
import headquarters.view.View;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author jiri
 */
public abstract class AbstractDAO {
    protected Connection connection;
    protected DAOManager manager;
    
    protected AbstractDAO(Connection conn, DAOManager boss)
    {
        this.connection = conn;
        this.manager = boss;
    }
    
    protected ResultSet query(String query) throws Exception
    {
            Statement st = this.connection.createStatement();
            ResultSet rs = st.executeQuery(query);
            return rs;
    }
    
    protected Integer update(String update, Integer keys) throws Exception
    {
            ResultSet rs;
            PreparedStatement st = this.connection.prepareStatement(update, keys);
            st.executeUpdate();
            rs = st.getGeneratedKeys();
            rs.next();
            return rs.getInt(1);
    }
    
    protected void notifyObservers()
    {
        manager.notifyObservers();
    }
    


}
