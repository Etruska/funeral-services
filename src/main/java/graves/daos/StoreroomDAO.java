/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.daos;

import graves.model.Storeroom;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author jiri
 */
public class StoreroomDAO extends AbstractDAO{
    StoreroomDAO(Connection conn, DAOManager boss)
    {
        super(conn, boss);
    }
    
    public ArrayList<Storeroom> findAll()
    {
        ArrayList<Storeroom> list = new ArrayList<Storeroom>();
        try
        {
            ResultSet rs = this.query("SELECT STOREROOM.* FROM STOREROOM");
            while(rs.next())
            {
                list.add(new Storeroom(rs.getInt("storeroomid"), rs.getString("name"), rs.getString("address"), rs.getInt("capacity")));
            }
            return list;
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
    
    
    public Storeroom findStoreroom(Integer id)
    {
        if (id == null)
            return null;
        try
        {
            ResultSet rs = this.query("SELECT STOREROOM.* FROM STOREROOM WHERE STOREROOM.STOREROOMID = " + id);
            if (rs.next())
            {
                return new Storeroom(rs.getInt("storeroomid"), rs.getString("name"), rs.getString("address"), rs.getInt("capacity"));
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
    
    public Integer addStoreroom(Storeroom storeroom)
    {
        if (this.findStoreroom(storeroom.getId()) != null)
        {
            System.out.println("Already found.");
            return null;
        }
        try
        {
            Integer ret = this.update("INSERT INTO STOREROOM (ADDRESS, CAPACITY, NAME)" + 
                    "VALUES ('" +storeroom.getAddress() +"', " + storeroom.getCapacity() + ", '" + storeroom.getName() +"')", Statement.RETURN_GENERATED_KEYS);
            this.manager.notifyObservers();
            return ret;
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;  
    }
}
