/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.daos;

import graves.model.Graveyard;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author jiri
 */
public class GraveyardDAO extends AbstractDAO{
    GraveyardDAO(Connection conn, DAOManager boss)
    {
        super(conn, boss);
    }
    
    public Graveyard findGraveyard(Integer id)
    {
        if (id == null)
        {
            return null;
        }
        try
        {
            ResultSet rs = this.query("SELECT GRAVEYARD.* FROM GRAVEYARD WHERE GRAVEYARD.GRAVEYARDID = " + id);
            if (rs.next())
            {
                return new Graveyard(rs.getInt("graveyardid"), rs.getString("name"), rs.getString("address"), rs.getInt("capacity"));
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
    
    public ArrayList<Graveyard> findAll()
    {
        ArrayList<Graveyard> list = new ArrayList<Graveyard>();
        try
        {
            ResultSet rs = this.query("SELECT GRAVEYARD.* FROM GRAVEYARD");
            while(rs.next())
            {
                list.add(new Graveyard(rs.getInt("graveyardid"), rs.getString("name"), rs.getString("address"), rs.getInt("capacity")));
            }
            return list;
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;
    }
    
    
    public Integer addGraveyard(Graveyard graveyard)
    {
        if (this.findGraveyard(graveyard.getId()) != null)
        {
            return null;
        }
        try
        {
            Integer ret = this.update("INSERT INTO GRAVEYARD (NAME, ADDRESS, CAPACITY)" + 
                    "VALUES ('" +graveyard.getName() +"', '" + graveyard.getAddress() + "', " + graveyard.getCapacity() + ")", Statement.RETURN_GENERATED_KEYS);
            this.manager.notifyObservers();
            return ret;
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
        return null;  
    }
}
