/*
 * 
 * 
 */
package graves.controller;

import graves.model.*;
import headquarters.controller.IController;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author jiri
 */
public interface IGraves extends IController{
    public Integer countGraves();
    
    public ArrayList<Grave> getGraves();
    public ArrayList<Tombstone> findTombstones(String text);
    public ArrayList<Storeroom> getStorerooms();
    public ArrayList<Tombstone> getTombstones();
    public ArrayList<Graveyard> getGraveyards();
    public ArrayList<Coffin> getCoffins();
    public ArrayList<Material> getMaterials();
    public Material findMaterial(String name);
    
    public Material findMaterial(Integer id);
    public Storeroom findStoreroom(Integer id);
    public Grave findGrave(Integer id);
    public Graveyard findGraveyard(Integer id);
    public Coffin findCoffin (Integer id);
    
    public Integer addGraveyard(Graveyard graveyard);
    public Integer addGrave(Grave grave);
    public Integer addTombstone(Tombstone tombstone);
    public Integer addCoffin(Coffin coffin);
    public Integer addMaterial(Material material);
    public Integer addStoreroom(Storeroom storeroom);
    
    public boolean deleteGraveyard(Graveyard graveyard);
    public boolean deleteGrave(Grave grave);
    public boolean deleteTombstone(Tombstone tombstone);
    public boolean deleteCoffin(Coffin coffin);
    public boolean deleteMaterial(Material material);
    public boolean deleteStoreroom(Storeroom storeroom);
    
    
   
}
