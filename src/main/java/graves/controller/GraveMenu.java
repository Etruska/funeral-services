package graves.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class GraveMenu extends JMenu {

    private final GravesController controller;
    private JMenuItem list;
    private JMenuItem addMaterial;
    private JMenuItem tombstones;
    private JMenuItem storerooms;
    private JMenuItem graveyards;
    private JMenuItem coffins;
    public static final int LIST = 1;
    public static final int ADD_MATERIAL = 3;
    public static final int TOMBSTONES = 4;
    public static final int STOREROOMS = 5;
    public static final int GRAVEYARDS = 6;
    public static final int COFFINS = 7;

    public GraveMenu(GravesController c) {
        super("Hroby");
        this.controller = c;
        this.list = new JMenuItem("Hroby");
        this.addMaterial = new JMenuItem("Materiály");
        this.tombstones = new JMenuItem("Náhrobky");
        this.storerooms = new JMenuItem("Sklady");
        this.graveyards = new JMenuItem("Hřbitovy");
        this.coffins = new JMenuItem("Rakve");

        this.list.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.graveMenuSelected(ae, LIST);
            }
        });

        
        this.addMaterial.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.graveMenuSelected(ae, ADD_MATERIAL);
            }
        });
        
        this.tombstones.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.graveMenuSelected(ae, TOMBSTONES);
            }
        });
        
        this.storerooms.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.graveMenuSelected(ae, STOREROOMS);
            }
        });
        
        this.graveyards.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.graveMenuSelected(ae, GRAVEYARDS);
            }
        });
        
        this.coffins.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.graveMenuSelected(ae, COFFINS);
            }
        });

        this.add(list);
        this.add(graveyards);
        this.add(coffins);
        this.add(tombstones);
        this.add(addMaterial);
        this.add(storerooms);
    }
}
