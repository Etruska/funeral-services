/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.controller;

import graves.daos.DAOManager;
import graves.model.*;
import graves.view.AddMaterialView;
import graves.view.CoffinsView;
import graves.view.GraveListView;
import graves.view.GraveyardsView;
import graves.view.StoreroomsView;
import graves.view.TombstonesView;
import headquarters.controller.*;
import headquarters.view.View;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.JMenu;

/**
 *
 * @author jiri
 */
public class GravesController implements IGraves{
    private DAOManager dao;
    IHeadquarters hq;
    GraveListView listView;
    AddMaterialView addMaterialView;
    TombstonesView tombstones;
    StoreroomsView storerooms;
    GraveyardsView graveyards;
    CoffinsView coffins;
    GraveMenu graveMenu;

   
    
    public GravesController(IHeadquarters hq)
    {
        DAOManager.setConnection(hq.getDatabase().getConnection());
        this.setDAO(new DAOManager());
        this.hq = hq;
        this.addMaterialView = new AddMaterialView(this, dao);
        this.listView = new GraveListView(this, dao);
        this.tombstones = new TombstonesView(this, dao);
        this.storerooms = new StoreroomsView(this, dao);
        this.graveyards = new GraveyardsView(this, dao);
        this.coffins = new CoffinsView(this, dao);
        this.graveMenu = new GraveMenu(this);
    }
    
    public IHeadquarters getHq()
    {
        return this.hq;
    }
    public void setDAO(DAOManager dao)
    {
        this.dao = dao;
    }

    public View getContentPanel() {
        return this.listView;
    }

    public JMenu getMenu() {
        return this.graveMenu;
    }

    public void graveMenuSelected(ActionEvent ae, int option) {
        switch (option) {
            case GraveMenu.LIST:
                hq.setContentPanel(listView);
                break;
            case GraveMenu.ADD_MATERIAL:
                hq.setContentPanel(addMaterialView);
                break;
            case GraveMenu.TOMBSTONES:
                hq.setContentPanel(tombstones);
                break;
            case GraveMenu.STOREROOMS:
                hq.setContentPanel(storerooms);
                break;
            case GraveMenu.GRAVEYARDS:
                hq.setContentPanel(graveyards);
                break;
            case GraveMenu.COFFINS:
                hq.setContentPanel(coffins);
                break;
        }
    }
    
    @Override
    public Integer countGraves()
    {
            return this.dao.graves().countGraves(false);
    }
    
    @Override
    public ArrayList<Grave> getGraves()
    {
        ArrayList<Grave> list = dao.graves().findAll();
        if (list == null) 
        {
            list = new ArrayList<Grave>();
        }
        return list;
    }
   
    @Override
    public ArrayList<Coffin> getCoffins()
    {
        ArrayList<Coffin> list = dao.coffins().findAll();
        if (list == null) 
        {
            list = new ArrayList<Coffin>();
        }
        return list;
    }
    
    @Override
    public ArrayList<Tombstone> getTombstones()
    {
        ArrayList<Tombstone> list = dao.tombstones().findAll();
        if (list == null) 
        {
            list = new ArrayList<Tombstone>();
        }
        return list;
    }
    
    @Override
    public ArrayList<Tombstone> findTombstones(String text)
    {
        return dao.tombstones().findTombstones(text);
    }
    
    @Override
    public ArrayList<Storeroom> getStorerooms()
    {
        ArrayList<Storeroom> list = dao.storerooms().findAll();
        if (list == null) 
        {
            list = new ArrayList<Storeroom>();
        }
        return list;
    }
    
    @Override
    public ArrayList<Graveyard> getGraveyards()
    {
        ArrayList<Graveyard> list = dao.graveyards().findAll();
        if (list == null) 
        {
            list = new ArrayList<Graveyard>();
        }
        return list;
    }
    
    @Override
    public ArrayList<Material> getMaterials()
    {
        ArrayList<Material> list = dao.materials().findAll();
        if (list == null) 
        {
            list = new ArrayList<Material>();
        }
        return list;
    }
    
    
    @Override
    public Material findMaterial(String name)
    {
        return dao.materials().findMaterial(name);
    }
    
    @Override 
    public Material findMaterial (Integer id)
    {
        return dao.materials().findMaterial(id);
    }
    
    @Override 
    public Grave findGrave (Integer id)
    {
        return dao.graves().findGrave(id);
    }
    
    @Override 
    public Coffin findCoffin (Integer id)
    {
        return dao.coffins().findCoffin(id);
    }
    
      
    @Override 
    public Storeroom findStoreroom (Integer id)
    {
        return dao.storerooms().findStoreroom(id);
    }
    
    @Override 
    public Graveyard findGraveyard (Integer id)
    {
        return dao.graveyards().findGraveyard(id);
    }
    
    @Override
    public Integer addGraveyard(Graveyard graveyard)
    {
        return dao.graveyards().addGraveyard(graveyard);
    }
    
    @Override
    public Integer addGrave(Grave grave)
    {
        return dao.graves().addGrave(grave);
    }
    
    @Override
    public Integer addTombstone(Tombstone tombstone)
    {
        return dao.tombstones().addTombstone(tombstone);
    }
    
    @Override
    public Integer addCoffin(Coffin coffin)
    {
        return dao.coffins().addCoffin(coffin);
    }
    
    @Override
    public Integer addMaterial(Material material)
    {
        return this.dao.materials().addMaterial(material);
    }
    
    @Override
    public Integer addStoreroom(Storeroom storeroom)
    {
        return this.dao.storerooms().addStoreroom(storeroom);
    }
     
    @Override
    public boolean deleteGraveyard(Graveyard graveyard)
    {
        return false;
    }
    
    @Override
    public boolean deleteGrave(Grave grave)
    {
        return dao.graves().deleteGrave(grave);
    }
    
    @Override
    public boolean deleteTombstone(Tombstone tombstone)
    {
        return false;
    }
    
    @Override
    public boolean deleteCoffin(Coffin coffin)
    {
        return false;
    }
    
    @Override
    public boolean deleteMaterial(Material material)
    {
        return false;
    }
    
    @Override
    public boolean deleteStoreroom(Storeroom storeroom)
    {
        return false;
    }

}
