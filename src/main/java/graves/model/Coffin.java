/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.model;

import people.person.Corpse;

/**
 *
 * @author jiri
 */
public class Coffin extends Object{
    private boolean m_state;
    private Corpse m_corpse;
    private Material m_material;
    private Storeroom m_storeroom;
    private Integer m_height;
    private Integer m_width;
    public static Integer default_width = 70;
    public static Integer default_height = 200;
        
    
    public Coffin(Integer id, boolean state, Integer height, Integer width, Material material, Corpse corpse, Storeroom storeroom)
    {
        this.m_id = id;
        this.m_state = state;
        this.m_height = height;
        this.m_width = width;
        this.m_material = material;
        this.m_corpse = corpse;
        this.m_storeroom = storeroom;
    }
    
    public boolean getState()
    {
        return this.m_state;
    }
    
    public Integer getCorpseId()
    {
        if (this.m_corpse == null)
        {
            return null;
        }
        else
        {
            return this.m_corpse.getCorpseID();
        }
    }
    
    public Integer getHeight()
    {
        return this.m_height;
    }
    
    public Integer getWidth()
    {
        return this.m_width;
    }
    
    public Material getMaterial()
    {
        return this.m_material;
    }
    
    public Integer getStoreroomId()
    {
        if (this.m_storeroom != null)
        {
            return this.m_storeroom.getId();
        }
        else
        {
            return null;
        }          
    }
    
    public Storeroom getStoreroom()
    {
            return this.m_storeroom;         
    }
    
    public String toString()
    {
        return (this.m_material.getName() + this.getHeight() + "x" + this.getWidth());
    }
    
    
}
