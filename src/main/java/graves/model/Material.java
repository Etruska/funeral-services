/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.model;

import graves.daos.DAOManager;

/**
 *
 * @author jiri
 */
public class Material extends Object{
    private String m_name;
    
    public Material(Integer id, String name)
    {
        this.m_id = id;
        this.m_name = name;
    }

    public String getName() {
        return m_name;
    }
    
    @Override
    public String toString()
    {
        return this.m_name;
    }
    
}
