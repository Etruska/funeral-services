/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.model;


/**
 *
 * @author jiri
 */
public class Tombstone extends Object{
    private Material m_material;
    private String m_text;
    private Storeroom m_storeroom;
    
    public Tombstone(Integer id, Material material, String text, Storeroom storeroom)
    {
        this.m_id = id;
        this.m_text = text;
        this.m_material = material;
        this.m_storeroom = storeroom;
    }
    
    public String getText()
    {
        return this.m_text;
    }
    
    public Material getMaterial()
    {
        return this.m_material;
    }
    
    public Storeroom getStoreroom()
    {
        return this.m_storeroom;
    }
    
    public Integer getStoreroomId()
    {
        if (this.m_storeroom != null)
        {
            return this.m_storeroom.getId();
        }
        else
        {
            return null;
        }
    }
    
    public String toString()
    {
        return this.m_text;
    }
}
