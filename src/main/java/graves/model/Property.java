/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.model;

/**
 *
 * @author jiri
 */
public class Property extends Object{
    protected String m_name;
    protected Integer m_capacity;
    protected String m_address;
    
    public Property(Integer id, String name, String address, Integer capacity)
    {
        this.m_id = id;
        this.m_name = name;
        this.m_address = address;
        this.m_capacity = capacity;
    }
    
    @Override
    public String toString()
    {
        return this.m_name;
    }
    
    public String getAddress()
    {
        return this.m_address;
    }
    
    public String getName()
    {
        return this.m_name;
    }
    
    public Integer getCapacity()
    {
        return this.m_capacity;
    }
}
