/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graves.model;


import graves.daos.DAOManager;

/**
 *
 * @author jiri
 */
public class Grave extends Object{
    private Tombstone m_tombstone;
    private Coffin m_coffin;
    private Graveyard m_graveyard;
    
    public static boolean EMPTY = false;
    public static boolean FULL = true;
    
    
    public Grave(Integer id, Tombstone tombstone, Coffin coffin, Graveyard graveyard)
    {
        this.m_id = id;
        this.m_tombstone = tombstone;
        this.m_coffin = coffin;
        this.m_graveyard = graveyard;
    }
    

    public Coffin getCoffin() {
        return m_coffin;
    }

    public Tombstone getTombstone() {
        return m_tombstone;
    }

    public Graveyard getGraveyard()
    {
        return this.m_graveyard;
    }
    
}
