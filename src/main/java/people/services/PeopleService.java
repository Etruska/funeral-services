package people.services;

import headquarters.controller.HeadquartersController;
import headquarters.model.ApplicationDirector;
import javax.swing.SwingUtilities;
import headquarters.model.Model;
import headquarters.model.ModuleBuilder;
import headquarters.view.AppLayout;

/**
 * Hello world!
 *
 */
public class PeopleService 
{
    public static void main(String[] args) {
        final Model model = new Model();
        final HeadquartersController controller = new HeadquartersController(model);
        
        final ModuleBuilder builder = new ModuleBuilder(model,controller);
        final ApplicationDirector app = new ApplicationDirector(model, controller, builder);
        app.run();
        
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
               new AppLayout(model, controller).setVisible(true);
            }
        });
        
    }
}
