package people.controllers;

import people.controllers.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class PeopleMenu extends JMenu {

    private final PeopleController controller;
    private JMenuItem relativeView;
    private JMenuItem corpseView;
    private JMenuItem employeeView;
    
    public static final int CORPSEVIEW = 1;
    public static final int RELATIVEVIEW = 2;
    public static final int EMPLOYEEVIEW = 3;

    public PeopleMenu(PeopleController c) {
        super("Osoby");
        this.controller = c;
        this.corpseView = new JMenuItem("Seznam zesnulých");
        this.relativeView = new JMenuItem("Seznam příbuzných");
        this.employeeView = new JMenuItem("Seznam zaměstnanců");

        this.corpseView.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.peopleMenuSelected(ae, CORPSEVIEW);
            }
        });
        
        this.relativeView.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.peopleMenuSelected(ae, RELATIVEVIEW);
            }
        });
        
        this.employeeView.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.peopleMenuSelected(ae, EMPLOYEEVIEW);
            }
        });

        this.add(corpseView);
        this.add(relativeView);
        this.add(employeeView);
    }
}
