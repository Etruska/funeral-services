/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.controllers;

import people.views.*;
import people.person.Address;
import people.person.Relative;
import people.person.Contact;
import people.person.Corpse;
import people.person.Employee;
import people.models.EmployeeDAO;
import people.models.CorpseDAO;
import people.models.RelativeDAO;
import people.interfaces.IPeople;
import java.util.ArrayList;
import java.util.List;

import people.views.CorpseView;
import headquarters.controller.IHeadquarters;
import headquarters.view.View;
import java.awt.event.ActionEvent;
import javax.swing.JMenu;
import people.interfaces.ICorpseObserver;
import people.models.Events;

/**
 *
 * @author baji
 */
public class PeopleController implements IPeople{

    private IHeadquarters hq;
    private CorpseView corpseView;
    private RelativeView relativeView;
    private EmployeeView employeeView;
    private PeopleMenu peopleMenu;
    private List<ICorpseObserver> corpseObservers;
    
    public PeopleController(IHeadquarters headquarters) {
        this.hq = headquarters;
        this.corpseObservers = new ArrayList<>();
        this.corpseView = new CorpseView(this);
        this.corpseView = new CorpseView(this);
        this.relativeView = new RelativeView(this);
        this.employeeView = new EmployeeView(this);
        this.peopleMenu = new PeopleMenu(this);
    }

    @Override
    public View getContentPanel() {
        return this.corpseView;
    }

    @Override
    public JMenu getMenu() {
        return this.peopleMenu;
    }

    public void peopleMenuSelected(ActionEvent ae, int option) {
        switch (option) {
            case PeopleMenu.CORPSEVIEW:
                hq.setContentPanel(corpseView);
                break;
            case PeopleMenu.RELATIVEVIEW:
                hq.setContentPanel(relativeView);
                break;
            case PeopleMenu.EMPLOYEEVIEW:
                hq.setContentPanel(employeeView);
                break;
        }
    }
    
    @Override
    public ArrayList<Corpse> getCorpses() {
        ArrayList<Corpse> corpses = new ArrayList<Corpse>();
        
        CorpseDAO corpseDAO = new CorpseDAO();
        corpses = corpseDAO.getCorpses();
        
        return corpses;
    }
    
    @Override
    public ArrayList<Relative> getRelatives() {
        ArrayList<Relative> relatives = new ArrayList<Relative>();
        
        RelativeDAO relativeDAO = new RelativeDAO();
        relatives = relativeDAO.getRelatives();
        
        return relatives;
    }
    
    @Override
    public ArrayList<Employee> getEmployees() {
        ArrayList<Employee> employees = new ArrayList<Employee>();
        
        EmployeeDAO employeeDAO = new EmployeeDAO();
        employees = employeeDAO.getEmployees();
        
        return employees;
    }
    
    @Override
    public Corpse getCorpseByID(int id) {
        Corpse corpse;

        CorpseDAO corpseDAO = new CorpseDAO();
        corpse = corpseDAO.getCorpseByID(id);
       
        return corpse;
    }
    
    @Override
    public Relative getRelativeByID(int id) {
        Relative relative;

        RelativeDAO relativeDAO = new RelativeDAO();
        relative = relativeDAO.getRelativeByID(id);
       
        return relative;
    }
    
    @Override
    public Employee getEmployeeByID(int id) {
        Employee employee;

        EmployeeDAO employeeDAO = new EmployeeDAO();
        employee = employeeDAO.getEmployeeByID(id);
       
        return employee;
    }

    @Override
    public void addCorpse(String name, String surname, int age, String dateOfBirth, String dateOfExpire) {
        Corpse corpse = new Corpse(name,surname,age,dateOfBirth,dateOfExpire);
        CorpseDAO corpseDAO = new CorpseDAO();
        corpseDAO.addCorpse(corpse);
    }
    
    @Override
    public void addRelative(String name, String surname, int age, int telNumber, String mail, String street, String city, int zip, int corpseID, String relationalship)  {
        Address address = new Address(street,city,zip);
        Contact contact = new Contact(telNumber,mail,address);
        Relative relative = new Relative(name,surname,age,contact,corpseID,relationalship);
        RelativeDAO relativeDAO = new RelativeDAO();
        relativeDAO.addRelative(relative);
    }
    
    @Override
    public void addEmployee(String name, String surname, int age, int telNumber, String mail, String street, String city, int zip, int salary, double secNumber, String profession)  {
        Address address = new Address(street,city,zip);
        Contact contact = new Contact(telNumber,mail,address);
        Employee employee = new Employee(name,surname,age,contact,salary, secNumber, profession);
        EmployeeDAO employeeDAO = new EmployeeDAO();
        employeeDAO.addEmployee(employee);
    }

    @Override
    public void deleteCorpse(int id)  {
        this.notifyCorpseObservers(id, Events.CORPSE_DELETED);
        CorpseDAO corpseDAO = new CorpseDAO();
        System.out.println(id);
        corpseDAO.deleteCorpse(id);
    }
    
    @Override
    public void deleteRelative(int id)  {
        RelativeDAO relativeDAO = new RelativeDAO();
        relativeDAO.deleteRelative(id);
    }
    
    @Override
    public void deleteEmployee(int id)  {
        EmployeeDAO employeeDAO = new EmployeeDAO();
        employeeDAO.deleteEmployee(id);
    }

    @Override
    public void attachCorpseObserver(ICorpseObserver o) {
        this.corpseObservers.add(o);
    }
    
    private void notifyCorpseObservers(int id, int event){
        for(ICorpseObserver o: corpseObservers){
            o.corpseUpdated(id, event);
        }
    }
    
}
