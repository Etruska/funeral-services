/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.person;

import java.util.Date;

/**
 *
 * @author baji
 */
public class Corpse extends Person{
    String dateOfExpire;
    String dateOfBirth;
    int corpseID;

    public Corpse(String name, String surname, int age, String dateOfBirth, String dateOfExpire) {
        super(name, surname, age);
        this.dateOfExpire = dateOfExpire;
        this.dateOfBirth = dateOfBirth;
    }
    
    public int getCorpseID() {
        return corpseID;
    }

    public String getDateOfExpire() {
        return dateOfExpire;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfExpire(String dateOfExpire) {
        this.dateOfExpire = dateOfExpire;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setCorpseID(int corpseID) {
        this.corpseID = corpseID;
    }
    
    @Override
    public String toString(){
        return this.name + " " + this.surname;
    }
   
    
}
