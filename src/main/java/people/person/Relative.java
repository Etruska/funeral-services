/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.person;

/**
 *
 * @author baji
 */
public class Relative extends Alive{
    int corpseID;
    String relationalship;
    int relativeID;
    Corpse corpse;

    public Relative(String name, String surname, int age, Contact contact, int corpseID, String relationalship) {
        super(name, surname, age, contact);
        this.corpseID = corpseID;
        this.relationalship = relationalship;
    }

    public Corpse getCorpse(){
        return corpse;
    }
    
    public int getCorpseID() {
        return corpseID;
    }

    public int getRelativeID() {
        return relativeID;
    }

    public String getRelationalship() {
        return relationalship;
    }
    
    public void setCorpseID(int corpseID) {
        this.corpseID = corpseID;
    }
    
    public void setCorpse(Corpse corpse){
        this.corpse = corpse;
    }

    public void setRelationalship(String relationalship) {
        this.relationalship = relationalship;
    }

    public void setRelativeID(int relativeID) {
        this.relativeID = relativeID;
    }
    
    
    
}
