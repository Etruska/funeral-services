/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.person;

/**
 *
 * @author baji
 */
public class Address {
    String street;
    String city;
    int zip;

    public Address(String street, String city, int zip) {
        this.street = street;
        this.city = city;
        this.zip = zip;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public int getZip() {
        return zip;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }
    
    @Override
    public boolean equals(Object o){
        if (o == null) {
            return false;
        }
        if (o instanceof Address) 
        {
            Address add = (Address)o;
            
            if (this.city.equals(add.city) && this.street.equals(add.street) && this.zip==add.zip)
                return true;
        }
        
        return false;
    }
    
}
