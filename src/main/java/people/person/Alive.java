/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.person;

/**
 *
 * @author baji
 */
public abstract class Alive extends Person{
    
    Contact contact;

    public Alive(String name, String surname, int age, Contact contact) {
        super(name, surname, age);
        this.contact = contact;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
     
}
