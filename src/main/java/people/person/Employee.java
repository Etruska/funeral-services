/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.person;

/**
 *
 * @author baji
 */
public class Employee extends Alive{
    int salary;
    double secNumber;
    String profession;
    int employeeID;

    public Employee(String name, String surname, int age, Contact contact, int salary, double secNumber, String profession) {
        super(name, surname, age, contact);
        this.salary = salary;
        this.secNumber = secNumber;
        this.profession = profession;
    }
    
    public int getEmployeeID() {
        return employeeID;
    }

    public int getSalary() {
        return salary;
    }

    public double getSecNumber() {
        return secNumber;
    }

    public String getProfession() {
        return profession;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void setSecNumber(double secNumber) {
        this.secNumber = secNumber;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }    

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }
    
    
}
