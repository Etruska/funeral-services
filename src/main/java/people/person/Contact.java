/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.person;

/**
 *
 * @author baji
 */
public class Contact {
    int telNumber;
    String mail;
    Address address;
    int contactID;

    public Contact(int telNumber, String mail, Address address) {
        this.telNumber = telNumber;
        this.mail = mail;
        this.address = address;
    }
    
    public int getContactID() {
        return contactID;
    }

    public int getTelNumber() {
        return telNumber;
    }

    public String getMail() {
        return mail;
    }

    public Address getAddress() {
        return address;
    }

    public void setTelNumber(int telNumber) {
        this.telNumber = telNumber;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setContactID(int contactID) {
        this.contactID = contactID;
    }
    
    
    
}
