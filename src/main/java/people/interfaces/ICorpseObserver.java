package people.interfaces;

/**
 * Little bit creeeepy class
 * @author Vojtěch Petrus
 */
public interface ICorpseObserver {
    
    public void corpseUpdated(int id, int event);
    
}
