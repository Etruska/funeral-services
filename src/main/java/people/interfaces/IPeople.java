/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.interfaces;

import people.person.Relative;
import people.person.Corpse;
import people.person.Employee;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import headquarters.controller.IController;

/**
 *
 * @author baji
 */
public interface IPeople extends IController{
    
    public ArrayList<Corpse> getCorpses();
    public Corpse getCorpseByID(int id);
    public void addCorpse(String name, String surname, int age, String dateOfBirth, String dateOfExpire);
    public void deleteCorpse(int id);
    
    public ArrayList<Relative> getRelatives();
    public Relative getRelativeByID(int id);
    public void addRelative(String name, String surname, int age, int telNumber, String mail, String street, String city, int zip, int corpseID, String relationalship);
    public void deleteRelative(int id);
    
    public ArrayList<Employee> getEmployees();
    public Employee getEmployeeByID(int id);
    public void addEmployee(String name, String surname, int age, int telNumber, String mail, String street, String city, int zip, int salary, double secNumber, String profession);
    public void deleteEmployee(int id);
    
    public void attachCorpseObserver(ICorpseObserver o);
}
