/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.views;

import headquarters.controller.HeadquartersController;
import headquarters.model.Model;
import headquarters.model.ModelObserver;
import headquarters.view.*;
import people.person.Corpse;
import people.models.*;
import people.controllers.PeopleController;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author baji
 */
public class CorpseView extends View implements ModelObserver {

    private JButton addCorpseButton;
    private JButton editCorpseButton;
    private JButton deleteCorpseButton;
    //private AppLayout appLayout;
    private JTable table;
    private ArrayList<Corpse> corpses;
    private JComboBox corpse;
    private PeopleController controller;
    private CorpseTableModel tableModel;
    private Model model;
    private CorpseDAO o;
    private DAO ob_model;

    //public void setAppLayout(AppLayout layout) {
      //  this.appLayout = layout;
    //}

    public String getViewName() {
        return "Person/Corpse";
    }

    public CorpseView(PeopleController controller) {
        this.controller = controller;
        this.model = model;
        this.addCorpseButton = new JButton("Přidat");
        this.deleteCorpseButton = new JButton("Smazat vybraný");
        this.setLayout(new GridBagLayout());
        this.o = new CorpseDAO();
        this.ob_model = new DAO();
        this.corpses = o.getCorpses();
        this.corpse = new JComboBox(corpses.toArray());
        this.tableModel = new CorpseTableModel();
        this.table = new JTable(this.tableModel);

        initComponents();
        setHandlers();
        ob_model.attachObserver(this);
    }

    @Override
    public void modelUpdated() {
        this.corpses = this.o.getCorpses();

        JComboBox ar_corpses = new JComboBox(corpses.toArray());
        ar_corpses.removeAllItems();

        for (Corpse c : corpses) {
            this.corpse.addItem(c);
        }

        this.tableModel.setCorpses(corpses);
        this.tableModel.fireTableDataChanged();
        this.revalidate();
    }

    private void hideButtons() {
        this.corpse.setVisible(false);
        this.editCorpseButton.setVisible(false);
        this.deleteCorpseButton.setVisible(false);
    }

    private void showButtons() {
        this.corpse.setVisible(true);
        this.editCorpseButton.setVisible(true);
        this.deleteCorpseButton.setVisible(true);
    }

    private void setHandlers() {
        this.addCorpseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                addCorpseDialog();
            }
        });
        this.deleteCorpseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int row_id = table.getSelectedRow();
                Corpse c = tableModel.getRow(row_id);
                int corpseID = c.getCorpseID();
                controller.deleteCorpse(corpseID);
            }
        });
    }

    private void initComponents() {
        GridBagConstraints c = new GridBagConstraints();

        c.insets = new Insets(10, 10, 10, 10);
        c.gridx = 0;
        c.gridy = 0;
        this.add(new JLabel("Seznam zesnulých"), c);

        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 3;

        this.table = new JTable(this.tableModel);
        
        this.table.setPreferredScrollableViewportSize(new Dimension(200, 200));
        this.table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(this.table);
        c.weighty = 1.0;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.BOTH;
        this.add(scrollPane, c);

        c.weightx = 0.5;
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 1;
        this.add(this.deleteCorpseButton, c);
        
        c.weightx = 0.5;
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 1;
        this.add(this.addCorpseButton, c);
        
        
        /*
        if(this.corpses.isEmpty())
            hideButtons(); */
    }

    public void addCorpseDialog() {

        JDialog c;
        c = new AddCorpseDialog(this.controller);
        c.setVisible(true);
    }
    

}
