/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package people.views;

import headquarters.controller.HeadquartersController;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import people.models.CorpseDAO;
import people.person.Corpse;
import people.controllers.*;
/**
 *
 * @author baji
 */
public class AddRelativeDialog extends JDialog {
    
    private JButton cancelButton;
    private JButton okButton;
    private JLabel labelName;
    private JTextField name;
    private JLabel labelSurname;
    private JTextField surname;
    private JLabel labelAge;
    private JTextField age;
    private JLabel labelRelationalship;
    private JTextField relationalShip;
    private JLabel labelCorpse;
    private JComboBox corpse;
    private JLabel labelMail;
    private JTextField mail;
    private JLabel labelTelephone;
    private JTextField telephone;
    private JLabel labelStreet;
    private JTextField street;
    private JLabel labelCity;
    private JTextField city;
    private JLabel labelZip;
    private JTextField zip;
    private int width;
    private int height;
    PeopleController controller;

    public AddRelativeDialog(PeopleController controller) {
        
        this.corpse = null;
        this.setLayout(new GridBagLayout());
        this.cancelButton = new JButton("Zrušit");
        this.okButton = new JButton("OK");
        this.controller = controller;
        this.initComponents();
        this.setHandlers();
    }

    private void initComponents() {
        this.width = 400;
        this.height = 500;
        setSize(width, height);
        int[] position = {0,0};
        this.setLocation(position[0], position[1]);

        this.labelName = new JLabel("Jméno:");
        this.name = new JTextField();
        
        this.labelSurname = new JLabel("Příjmení:");
        this.surname = new JTextField();
        
        this.labelAge = new JLabel("Věk:");
        this.age = new JTextField();
        
        this.labelRelationalship = new JLabel("Vztah:");
        this.relationalShip = new JTextField();
        
        this.labelCorpse = new JLabel("Zesnulí:");
        CorpseDAO corpseDAO = new CorpseDAO();
        ArrayList<Corpse> corpses = corpseDAO.getCorpses();
        this.corpse = new JComboBox(corpses.toArray());
        
        this.labelMail = new JLabel("Mail:");
        this.mail = new JTextField();
        
        this.labelTelephone = new JLabel("Telefon:");
        this.telephone = new JTextField();
        
        this.labelStreet = new JLabel("Ulice:");
        this.street = new JTextField();
        
        this.labelCity = new JLabel("Město:");
        this.city = new JTextField();
        
        this.labelZip = new JLabel("PSČ:");
        this.zip = new JTextField();

        GridBagConstraints c = new GridBagConstraints();
        
        c.insets = new Insets(5,5,5,5);
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labelName, c);

        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.name, c);
        
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labelSurname, c);

        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.surname, c);
        
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labelAge, c);

        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.age, c);
        
        c.gridx = 0;
        c.gridy = 3;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labelRelationalship, c);

        c.gridx = 1;
        c.gridy = 3;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.relationalShip, c);
        
        c.gridx = 0;
        c.gridy = 4;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labelCorpse, c);

        c.gridx = 1;
        c.gridy = 4;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.corpse, c);
        
        c.gridx = 0;
        c.gridy = 5;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labelMail, c);

        c.gridx = 1;
        c.gridy = 5;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.mail, c);
        
        c.gridx = 0;
        c.gridy = 6;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labelTelephone, c);

        c.gridx = 1;
        c.gridy = 6;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.telephone, c);
        
        c.gridx = 0;
        c.gridy = 7;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labelStreet, c);

        c.gridx = 1;
        c.gridy = 7;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.street, c);
        
        c.gridx = 0;
        c.gridy = 8;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labelCity, c);

        c.gridx = 1;
        c.gridy = 8;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.city, c);
        
        c.gridx = 0;
        c.gridy = 9;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labelZip, c);

        c.gridx = 1;
        c.gridy = 9;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.zip, c);
        
        c.weightx = 0.5;
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 10;
        c.gridwidth = 1;
        this.add(this.okButton, c);
        
        c.weightx = 0.5;
        c.fill = GridBagConstraints.NONE;
        c.gridx = 1;
        c.gridy = 10;
        c.gridwidth = 1;
        this.add(this.cancelButton, c);
        
        
    }
    
    public String getNameLabelValue(){
        return this.labelName.getText();
    }

    private void setHandlers() {
        this.cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                setVisible(false);
            }
        });
        this.okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int iage = Integer.parseInt(age.getText());
                int itelephone = Integer.parseInt(telephone.getText());
                int izip = Integer.parseInt(zip.getText());
                Corpse c = (Corpse) corpse.getSelectedItem();
                controller.addRelative(name.getText(), surname.getText(), iage, itelephone, mail.getText(), street.getText(), city.getText(), izip, c.getCorpseID(), relationalShip.getText());
                setVisible(false);
            }
        });

    }

}

