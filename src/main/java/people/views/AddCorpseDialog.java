/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package people.views;

import headquarters.controller.HeadquartersController;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import people.person.Corpse;
import people.controllers.*;
/**
 *
 * @author baji
 */
public class AddCorpseDialog extends JDialog {
    
    private JButton cancelButton;
    private JButton okButton;
    private JLabel labelName;
    private JTextField name;
    private JLabel labelSurname;
    private JTextField surname;
    private JLabel labelAge;
    private JTextField age;
    private JLabel labeldateOfBirth;
    private JTextField dateOfBirth;
    private JLabel labeldateOfExpire;
    private JTextField dateOfExpire;
    private Corpse corpse;
    private int width;
    private int height;
    private PeopleController controller;

    public AddCorpseDialog(PeopleController controller) {
        
        this.corpse = null;
        this.setLayout(new GridBagLayout());
        this.cancelButton = new JButton("Zrušit");
        this.okButton = new JButton("Ok");
        this.controller = controller;
        this.initComponents();
        this.setHandlers();
    }

    public void initComponents() {
        this.width = 400;
        this.height = 300;
        setSize(width, height);
        int[] position = {0,0};
        this.setLocation(position[0], position[1]);

        this.labelName = new JLabel("Jméno:");
        this.name = new JTextField();
        
        this.labelSurname = new JLabel("Příjmení:");
        this.surname = new JTextField();
        
        this.labelAge = new JLabel("Věk:");
        this.age = new JTextField();
        
        this.labeldateOfBirth = new JLabel("Datum narození:");
        this.dateOfBirth = new JTextField();
        
        this.labeldateOfExpire = new JLabel("Datum úmrtí:");
        this.dateOfExpire = new JTextField();

        GridBagConstraints c = new GridBagConstraints();
        
        c.insets = new Insets(5,5,5,5);
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labelName, c);

        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.name, c);
        
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labelSurname, c);

        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.surname, c);
        
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labelAge, c);

        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.age, c);
        
        c.gridx = 0;
        c.gridy = 3;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labeldateOfBirth, c);

        c.gridx = 1;
        c.gridy = 3;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.dateOfBirth, c);
        
        c.gridx = 0;
        c.gridy = 4;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.labeldateOfExpire, c);

        c.gridx = 1;
        c.gridy = 4;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.BOTH;
        this.add(this.dateOfExpire, c);
        
        c.weightx = 0.5;
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 5;
        c.gridwidth = 1;
        this.add(this.okButton, c);
        
        c.weightx = 0.5;
        c.fill = GridBagConstraints.NONE;
        c.gridx = 1;
        c.gridy = 5;
        c.gridwidth = 1;
        this.add(this.cancelButton, c);
        
        
    }
    
    public String getNameLabelValue(){
        return this.labelName.getText();
    }

    public void setHandlers() {
        this.cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                setVisible(false);
            }
        });
        this.okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int iage = Integer.parseInt(age.getText());
                controller.addCorpse(name.getText(), surname.getText(), iage, dateOfBirth.getText(), dateOfExpire.getText());
                setVisible(false);
            }
        });

    }

}

