/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.views;

import headquarters.controller.HeadquartersController;
import headquarters.model.Model;
import headquarters.view.*;
import people.person.*;
import people.models.*;
import people.controllers.PeopleController;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author baji
 */
public class RelativeView extends View {

    private JButton addRelativeButton;
    private JButton deleteRelativeButton;
    //private AppLayout appLayout;
    private JTable table;
    private List<Relative> relatives;
    private JComboBox relative;
    private PeopleController controller;
    private RelativeTableModel tableModel;

    //public void setAppLayout(AppLayout layout) {
      //  this.appLayout = layout;
    //}

    public String getViewName() {
        return "Person/Relative";
    }

    public RelativeView(PeopleController controller) {
        this.controller = controller;
        this.addRelativeButton = new JButton("Přidat");
        this.deleteRelativeButton = new JButton("Smazat vybraný");
        this.setLayout(new GridBagLayout());
        RelativeDAO o = new RelativeDAO();
        this.relatives = o.getRelatives();
        this.relative = new JComboBox(relatives.toArray());
        this.tableModel = new RelativeTableModel();

        initComponents();
        setHandlers();
    }

    private void hideButtons() {
        this.relative.setVisible(false);
        this.deleteRelativeButton.setVisible(false);
    }

    private void showButtons() {
        this.relative.setVisible(true);
        this.deleteRelativeButton.setVisible(true);
    }

    private void setHandlers() {
        this.addRelativeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                addRelativeDialog();
            }
        });
        this.deleteRelativeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int row_id = table.getSelectedRow();
                Relative r = tableModel.getRow(row_id);
                int relativeID = r.getRelativeID();
                controller.deleteRelative(relativeID);
            }
        });
    }

    private void initComponents() {
        GridBagConstraints c = new GridBagConstraints();

        c.insets = new Insets(10, 10, 10, 10);
        c.gridx = 0;
        c.gridy = 0;
        this.add(new JLabel("Seznam příbuzných"), c);

        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 3;
 
        //final JTable table = new JTable(data, columnNames);
        
        this.table = new JTable(this.tableModel);
        
        this.table.setPreferredScrollableViewportSize(new Dimension(200, 200));
        table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(table);
        c.weighty = 1.0;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.BOTH;
        this.add(scrollPane, c);
        
        c.weightx = 0.5;
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 1;
        this.add(this.deleteRelativeButton, c);

        c.weightx = 0.5;
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 1;
        this.add(this.addRelativeButton, c);
        
        if(this.relatives.isEmpty())
            hideButtons(); 
    }

    public void addRelativeDialog() {

        JDialog r = new AddRelativeDialog(this.controller);
        r.setVisible(true);
    }
    

}
