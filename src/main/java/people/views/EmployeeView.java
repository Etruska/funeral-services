/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.views;

import headquarters.model.ModelObserver;
import headquarters.view.*;
import people.person.*;
import people.models.*;
import people.controllers.PeopleController;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author baji
 */
public class EmployeeView extends View {

    private JButton addEmployeeButton;
    private JButton editEmployeeButton;
    private JButton deleteEmployeeButton;
    //private AppLayout appLayout;
    private JTable table;
    private ArrayList<Employee> employees;
    private JComboBox employee;
    private PeopleController controller;
    private EmployeeTableModel tableModel;
    private DAO ob_model;
    private EmployeeDAO o;

    //public void setAppLayout(AppLayout layout) {
      //  this.appLayout = layout;
    //}

    public String getViewName() {
        return "Person/Employee";
    }
    

    public EmployeeView(PeopleController controller) {
        this.controller = controller;
        this.addEmployeeButton = new JButton("Přidat");
        this.editEmployeeButton = new JButton("Upravit");
        this.deleteEmployeeButton = new JButton("Smazat vybraný");
        this.setLayout(new GridBagLayout());
        this.o = new EmployeeDAO();
        this.ob_model = new DAO();
        this.employees = o.getEmployees();
        this.employee = new JComboBox(employees.toArray());
        this.tableModel = new EmployeeTableModel();

        initComponents();
        setHandlers();
    }

    private void hideButtons() {
        this.employee.setVisible(false);
        this.deleteEmployeeButton.setVisible(false);
    }

    private void showButtons() {
        this.employee.setVisible(true);
        this.deleteEmployeeButton.setVisible(true);
    }

    private void setHandlers() {
        this.addEmployeeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                addEmployeeDialog();
            }
        });
        this.deleteEmployeeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int row_id = table.getSelectedRow();
                Employee e = tableModel.getRow(row_id);
                int employeeID = e.getEmployeeID();
                controller.deleteEmployee(employeeID);
            }
        });
    }

    private void initComponents() {
        GridBagConstraints c = new GridBagConstraints();

        c.insets = new Insets(10, 10, 10, 10);
        c.gridx = 0;
        c.gridy = 0;
        this.add(new JLabel("Seznam zaměstnanců"), c);

        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 3;
 
        //final JTable table = new JTable(data, columnNames);
        
        this.table = new JTable(this.tableModel);
        
        this.table.setPreferredScrollableViewportSize(new Dimension(200, 200));
        table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(table);
        c.weighty = 1.0;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.BOTH;
        this.add(scrollPane, c);
        
        c.weightx = 0.5;
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 1;
        this.add(this.deleteEmployeeButton, c);

        c.weightx = 0.5;
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 1;
        this.add(this.addEmployeeButton, c);
        /*
        if(this.employees.isEmpty())
            hideButtons(); */
    }

    public void addEmployeeDialog() {

        JDialog e = new AddEmployeeDialog(this.controller);
        e.setVisible(true);
    }
    

}
