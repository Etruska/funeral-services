/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.models;

import headquarters.controller.HeadquartersController;
import headquarters.model.db.IDatabase;
import headquarters.model.Model;
import people.person.Address;
import people.person.Relative;
import people.person.Contact;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author baji
 */
public class RelativeDAO extends DAO{

    HeadquartersController controller;
    Model model;
    IDatabase database;
    Connection connection;
    Statement st;
    
    public RelativeDAO(){
        this.model = new Model();
        this.controller = new HeadquartersController(model);
        this.database = this.controller.getDatabase();
        this.connection = this.database.getConnection();
        try{
            this.st = this.connection.createStatement();
        }catch (Exception e){}
    }
    
    public RelativeDAO(Model model, IDatabase database){
        this.model = model;
        this.controller = new HeadquartersController(model);
        this.database = database;
        this.connection = this.database.getConnection();
        try{
            this.st = this.connection.createStatement();
        }catch (Exception e){}
    }
    
    public ArrayList<Relative> getRelatives(){
        
        try {
            ArrayList<Relative> relatives = new ArrayList<Relative>();
            ResultSet rs = st.executeQuery(
                    "SELECT * FROM RELATIVEPERSON ");

            while (rs.next()) {
                DAO o = new DAO();
                Contact contact = o.getContactByID(rs.getInt(7));
                Relative r = new Relative(rs.getString(2), rs.getString(3), rs.getInt(4), contact, rs.getInt(6), rs.getString(5));
                r.setRelativeID(rs.getInt(1));
                relatives.add(r);
            }

            return relatives;
            
        }
        catch (Exception e){
            return null;
        }
        
    }
    
    public Relative getRelativeByID(int id){
        
        try {
            ResultSet rs = st.executeQuery(
                    "SELECT * FROM RELATIVEPERSON WHERE RELATIVEPERSONID = "+id+"");

            rs.next();
                DAO o = new DAO();
                Contact contact = o.getContactByID(rs.getInt(7));
                Relative r = new Relative(rs.getString(2), rs.getString(3), rs.getInt(4), contact, rs.getInt(6), rs.getString(5));
                r.setRelativeID(rs.getInt(1));

            return r;
            
        }
        catch (Exception e){
            return null;
        }
        
    }
    
    public boolean addRelative(Relative relative){
        
        try {
            Contact contact = relative.getContact();
            Address address = contact.getAddress();
            
            int rs_contact = st.executeUpdate(
                    "INSERT INTO CONTACT (TELEPHONE,MAIL,STREET,CITY,ZIP) VALUES ("+contact.getTelNumber()+", '"+contact.getMail()+"', '"+address.getStreet()+"', '"+address.getCity()+"', "+address.getZip()+") ");

            int rs = st.executeUpdate(
                    "INSERT INTO RELATIVEPERSON (NAME,SURNAME,AGE,RELATIONSHIP,CORPSEID,CONTACTID) VALUES ('"+relative.getName()+"','"+relative.getSurname()+"',"+relative.getAge()+",'"+relative.getRelationalship()+"',"+relative.getCorpseID()+", "+rs_contact+") ");
            return true;
        }
        catch (Exception e){
            return false;
        }
        
    }

    public boolean deleteRelative(int id){
        
        try {
            int rs = st.executeUpdate(
                    "DELETE FROM RELATIVEPERSON WHERE RELATIVEPERSONID="+id+"");
            return true;
        }
        catch (Exception e){
            return false;
        }
        
    }
    
}
