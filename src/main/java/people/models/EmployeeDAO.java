/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.models;

import headquarters.controller.HeadquartersController;
import headquarters.model.db.IDatabase;
import headquarters.model.Model;
import people.person.Address;
import people.person.Contact;
import people.person.Employee;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author baji
 */
public class EmployeeDAO extends DAO{

    HeadquartersController controller;
    Model model;
    IDatabase database;
    Connection connection;
    Statement st;
    
    public EmployeeDAO(){
        this.model = new Model();
        this.controller = new HeadquartersController(model);
        this.database = this.controller.getDatabase();
        this.connection = this.database.getConnection();
        try{
            this.st = this.connection.createStatement();
        }catch (Exception e){}
    }
    
    public EmployeeDAO(Model model, IDatabase database){
        this.model = model;
        this.controller = new HeadquartersController(model);
        this.database = database;
        this.connection = this.database.getConnection();
        try{
            this.st = this.connection.createStatement();
        }catch (Exception e){}
    }
    
    public ArrayList<Employee> getEmployees(){
        
        try {
            ArrayList<Employee> employees = new ArrayList<Employee>();
            ResultSet rs = st.executeQuery(
                    "SELECT * FROM EMPLOYEE ");

            while (rs.next()) {
                DAO o = new DAO();
                Contact contact = o.getContactByID(rs.getInt(8));
                Employee e = new Employee(rs.getString(2), rs.getString(3), rs.getInt(4), contact, rs.getInt(6), rs.getDouble(7), rs.getString(5));
                e.setEmployeeID(rs.getInt(1));
                employees.add(e);
            }

            return employees;
            
        }
        catch (Exception e){
            return null;
        }

        
    }
    
    public Employee getEmployeeByID(int id){
        
        try {
            ResultSet rs = st.executeQuery(
                    "SELECT * FROM EMPLOYEE WHERE EMPLOYEEID = "+id+"");

            rs.next();
                DAO o = new DAO();
                Contact contact = o.getContactByID(rs.getInt(8));
                Employee e = new Employee(rs.getString(2), rs.getString(3), rs.getInt(4), contact, rs.getInt(6), rs.getDouble(7), rs.getString(5));
                e.setEmployeeID(rs.getInt(1));

            return e;
            
        }
        catch (Exception e){
            return null;
        }
        
    }
    
    public boolean addEmployee(Employee employee){
        
        try {
            Contact contact = employee.getContact();
            Address address = contact.getAddress();
            
            int rs_contact = st.executeUpdate(
                    "INSERT INTO CONTACT (TELEPHONE,MAIL,STREET,CITY,ZIP) VALUES ("+contact.getTelNumber()+", '"+contact.getMail()+"', '"+address.getStreet()+"', '"+address.getCity()+"', "+address.getZip()+") ");
            
            int rs = st.executeUpdate(
                    "INSERT INTO EMPLOYEE (NAME,SURNAME,AGE,PROFESSION,SALARY,SECNUMBER,CONTACTID) VALUES ('"+employee.getName()+"','"+employee.getSurname()+"',"+employee.getAge()+",'"+employee.getProfession()+"',"+employee.getSalary()+","+employee.getSecNumber()+","+rs_contact+") ");
            return true;
        }
        catch (Exception e){
            return false;
        }
        
    }

    public boolean deleteEmployee(int id){
        
        try {
            int rs = st.executeUpdate(
                    "DELETE FROM EMPLOYEE WHERE EMPLOYEEID="+id+"");
            return true;
        }
        catch (Exception e){
            return false;
        }
        
    }
    
}
