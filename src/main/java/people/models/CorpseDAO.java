/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import people.person.Corpse;
import headquarters.controller.*;
import headquarters.model.*;
import headquarters.model.db.IDatabase;

/**
 *
 * @author baji
 */
public class CorpseDAO extends DAO{

    HeadquartersController controller;
    Model model;
    IDatabase database;
    Connection connection;
    Statement st;
    
    public CorpseDAO(){
        this.model = new Model();
        this.controller = new HeadquartersController(model);
        this.database = this.controller.getDatabase();
        this.connection = this.database.getConnection();
        try{
            this.st = this.connection.createStatement();
        }catch (Exception e){}
    }
    
    public CorpseDAO(Model model, IDatabase database){
        this.model = model;
        this.controller = new HeadquartersController(model);
        this.database = database;
        this.connection = this.database.getConnection();
        try{
            this.st = this.connection.createStatement();
        }catch (Exception e){}
    }
    
    public ArrayList<Corpse> getCorpses(){
        
        try{
            ArrayList<Corpse> corpses = new ArrayList<Corpse>();
            ResultSet rs = st.executeQuery(
                    "SELECT * FROM CORPSE ");

            while (rs.next()) {
                Corpse corpse = new Corpse(rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6));
                corpse.setCorpseID(rs.getInt(1));
                corpses.add(corpse);
            }

            return corpses;
        }
        catch (Exception e){
           return null;
        }
    }
    
    public Corpse getCorpseByID(int id){
        if (this.database != null) System.out.println(id);
        try {
            ResultSet rs = st.executeQuery(
                    "SELECT * FROM CORPSE WHERE CORPSEID = "+id+"");

            rs.next();
                Corpse corpse = new Corpse(rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6));
                corpse.setCorpseID(rs.getInt(1));

            return corpse;
        }
        catch (Exception e){
            return null;
        }
    }
    
    public boolean addCorpse(Corpse corpse){
        
        try {
            int rs = st.executeUpdate(
                    "INSERT INTO CORPSE (NAME,SURNAME,AGE,DATEOFBIRTH,DATEOFEXPIRE) VALUES ('"+corpse.getName()+"','"+corpse.getSurname()+"',"+corpse.getAge()+",'"+corpse.getDateOfBirth()+"','"+corpse.getDateOfExpire()+"') ");
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    public boolean deleteCorpse(int id) {
        
        try {
            int rs = st.executeUpdate(
                    "DELETE FROM CORPSE WHERE CORPSEID="+id+"");
            return true;
        }
        catch (Exception e){
            return false;
        }
        
    }
     
}
