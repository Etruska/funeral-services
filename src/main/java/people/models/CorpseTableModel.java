/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.models;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import people.person.Corpse;

/**
 *
 * @author baji
 */
public class CorpseTableModel extends AbstractTableModel{
    
    int rowCount;
    int columnCount;
    CorpseDAO corpseDAO;
    ArrayList<Corpse> corpses;
    String[] columnNames= {"ID",
                                "Name",
                                "Last name",
                                "Age",
                                "Date of Birth",
                                "Date of Expire"};
    
    
    public CorpseTableModel(){
    
        CorpseDAO corDAO = new CorpseDAO();
        this.corpses = corDAO.getCorpses();
     
    }
    
    public CorpseTableModel(int i){

    }
    
    public void setCorpses(ArrayList<Corpse> corpses){
        
        this.corpses = corpses;
    }
    
    @Override
    public int getRowCount() {

        CorpseDAO corDAO = new CorpseDAO();
        this.corpses = corDAO.getCorpses();
        return this.corpses.size();
    }
    
    @Override
    public String getColumnName(int column){
        return columnNames[column];
    }
    
    public String[] getColumnNames(){
        
        String[] columnNames = {"ID",
                                "Name",
                                "Last name",
                                "Age",
                                "Date of Birth",
                                "Date of Expire"};
        return columnNames;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }
    
    public Corpse getRow(int id) {
        Corpse corpse; 
        return corpse = corpses.get(id);
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex){
        Corpse corpse = corpses.get(rowIndex);
        
        switch (columnIndex) {
             case 0:
                    return corpse.getCorpseID(); 
             case 1:
                    return corpse.getName();
             case 2:
                    return corpse.getSurname();
             case 3:
                    return corpse.getAge();
             case 4:
                    return corpse.getDateOfBirth();
             case 5:
                    return corpse.getDateOfExpire();
             default:
                    throw new IndexOutOfBoundsException();
             } 
    }
    
}
