/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.models;

import headquarters.controller.HeadquartersController;
import headquarters.model.db.IDatabase;
import headquarters.model.Model;
import headquarters.model.ModelObserver;
import headquarters.model.db.IDatabase;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import people.person.Address;
import people.person.Contact;

/**
 *
 * @author baji
 */
public class DAO {
    
    HeadquartersController controller;
    Model model;
    IDatabase database;
    Connection connection;
    Statement st;
    
    private List<ModelObserver> observers = new ArrayList<ModelObserver>();
    
    public DAO(){
        this.model = new Model();
        this.controller = new HeadquartersController(model);
        this.database = this.controller.getDatabase();
        this.connection = this.database.getConnection();
        try{
            this.st = this.connection.createStatement();
        }catch (Exception e){}
    }
    
    private void notifyObservers() {
        for (ModelObserver o : observers) {
            o.modelUpdated();
        }
    }

    public void attachObserver(ModelObserver observer) {
        observers.add(observer);
    }

    public void dettachObserver(ModelObserver observer) {
        observers.remove(observer);
    }
    
    public Contact getContactByID(int id){
          try{
                ResultSet rs = st.executeQuery(
                    "SELECT * FROM CONTACT WHERE CONTACTID="+id+"");
                
                rs.next();
                Address address = new Address(rs.getString(4), rs.getString(5), rs.getInt(6));
                Contact contact = new Contact(rs.getInt(2), rs.getString(3), address);
                contact.setContactID(rs.getInt(1));

                return contact;
          }
          catch(Exception e){
                return null;
          }
    }

}
