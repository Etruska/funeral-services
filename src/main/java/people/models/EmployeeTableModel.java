/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.models;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import people.person.*;

/**
 *
 * @author baji
 */
public class EmployeeTableModel extends AbstractTableModel{
    
    int rowCount;
    int columnCount;
    EmployeeDAO employeeDAO;
    ArrayList<Employee> employees;
    String[] columnNames= {"ID",
                                "Name",
                                "Last name",
                                "Age",
                                "Profession",
                                "Salary",
                                "Security Number",
                                "Telephone",
                                "Mail",
                                "Street",
                                "City",
                                "Zip"};
    
    public EmployeeTableModel(){
    
        EmployeeDAO corDAO = new EmployeeDAO();
        this.employees = corDAO.getEmployees();
     
    }
    
    public EmployeeTableModel(int i){
   
    }
    
    @Override
    public int getRowCount() {

        EmployeeDAO corDAO = new EmployeeDAO();
        this.employees = corDAO.getEmployees();
        return this.employees.size();
    }
    
    public void setEmployees(ArrayList<Employee> employees){
        
        this.employees = employees;
    }
    
    @Override
    public String getColumnName(int column){
        return columnNames[column];
    }
    
    public String[] getColumnNames(){
        
        return columnNames;
    }

    @Override
    public int getColumnCount() {
        return 12;
    }
    
    public Employee getRow(int id) {
        Employee employee = employees.get(id);
        return employee;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex){
        Employee employee = employees.get(rowIndex);
        //Contact contact = 
        
        switch (columnIndex) {
             case 0:
                    return employee.getEmployeeID(); 
             case 1:
                    return employee.getName();
             case 2:
                    return employee.getSurname();
             case 3:
                    return employee.getAge();
             case 4:
                    return employee.getSalary();
             case 5:
                    return employee.getProfession();
             case 6:
                    return employee.getSecNumber();
             case 7:
                    return employee.getContact().getTelNumber();
             case 8:
                    return employee.getContact().getMail();
             case 9:
                    return employee.getContact().getAddress().getStreet();
             case 10:
                    return employee.getContact().getAddress().getCity();
             case 11:
                    return employee.getContact().getAddress().getZip();
             default:
                    throw new IndexOutOfBoundsException();
             } 
    }
    
}
