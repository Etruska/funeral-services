/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package people.models;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import people.person.*;

/**
 *
 * @author baji
 */
public class RelativeTableModel extends AbstractTableModel{
    
    int rowCount;
    int columnCount;
    RelativeDAO relativeDAO;
    ArrayList<Relative> relatives;
    String[] columnNames= {"ID",
                                "Name",
                                "Last name",
                                "Age",
                                "Relationalship",
                                "Zesnulý",
                                "Telephone",
                                "Mail",
                                "Street",
                                "City",
                                "Zip"};
    
    public RelativeTableModel(){
    
        RelativeDAO corDAO = new RelativeDAO();
        this.relatives = corDAO.getRelatives();
        System.out.println(this.relatives.size());
     
    }
    
    public RelativeTableModel(int i){

    }
    
    @Override
    public int getRowCount() {

        RelativeDAO corDAO = new RelativeDAO();
        this.relatives = corDAO.getRelatives();
        return this.relatives.size();
    }
    
    public void setRelatives(ArrayList<Relative> relatives){
        
        this.relatives = relatives;
    }
    
    @Override
    public String getColumnName(int column){
        return columnNames[column];
    }
    
    public String[] getColumnNames(){
        
        return columnNames;
    }

    @Override
    public int getColumnCount() {
        return 11;
    }
    
    public Relative getRow(int id) {
        Relative r = relatives.get(id);
        return r;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex){
        Relative relative = relatives.get(rowIndex);
        //Contact contact = 
        
        switch (columnIndex) {
             case 0:
                    return relative.getRelativeID(); 
             case 1:
                    return relative.getName();
             case 2:
                    return relative.getSurname();
             case 3:
                    return relative.getAge();
             case 4:
                    return relative.getRelationalship();
             case 5:
                    return relative.getCorpseID();
             case 6:
                    return relative.getContact().getTelNumber();
             case 7:
                    return relative.getContact().getMail();
             case 8:
                    return relative.getContact().getAddress().getStreet();
             case 9:
                    return relative.getContact().getAddress().getCity();
             case 10:
                    return relative.getContact().getAddress().getZip();
             default:
                    throw new IndexOutOfBoundsException();
             } 
    }
    
}
