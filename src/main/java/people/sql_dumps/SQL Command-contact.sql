CREATE TABLE "CONTACT"

(

"CONTACTID" INT not null primary key
        GENERATED ALWAYS AS IDENTITY
        (START WITH 1, INCREMENT BY 1),   
"TELEPHONE" INTEGER,     
"MAIL" VARCHAR(50),
"STREET" VARCHAR(50),
"CITY" VARCHAR(50),
"ZIP" INTEGER
);