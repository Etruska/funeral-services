package headquarters.controller;

import AccountingController.IAccounting;
import PR.Controller.IPR;
import graves.controller.IGraves;
import headquarters.model.db.IDatabase;
import headquarters.view.View;
import javax.swing.JFrame;
import people.interfaces.IPeople;

/**
 * Interface providing communication and Layout manipulation for all modules
 * @author Vojtěch Petrus
 */
public interface IHeadquarters extends IController {

    /**
     * Set content view to be placed into layout  
     * @param contentPanel
     */
    public void setContentPanel(View contentPanel);

    /**
     * Shows status message on status bar at the bottom of application
     *
     * @param status message to be shown
     * @param statusType status type - one of AppLayout.STATUS_OK,
     * AppLayout.STATUS_ERROR or AppLayout.STATUS_INFO
     */
    public void showStatus(String status, int statusType);

    /**
     * Get main application window with layout (mainly for setting dialog parent)
     * @return JFrame app layout
     */
    public JFrame getAppLayout();
    
    /**
     * Get Graves Module main entry point
     *
     * @return IGraves
     */
    public IGraves getGravesModule();

    /**
     * Get People Module main entry point
     *
     * @return IPeople
     */
    public IPeople getPeopleModule();

    /**
     * Get PR Module main entry point
     *
     * @return IPR
     */
    public IPR getPRModule();

    /**
     * Get Accounting Module main entry point
     *
     * @return IAccounting
     */
    public IAccounting getAccountingModule();

    /**
     * Get Database object
     *
     * @return IDatabase
     */
    public IDatabase getDatabase();
}
