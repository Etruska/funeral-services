package headquarters.controller;

import AccountingController.IAccounting;
import PR.Controller.IPR;
import graves.controller.IGraves;
import headquarters.model.Model;
import headquarters.model.Validator;
import headquarters.model.db.IDatabase;
import headquarters.model.entities.Funeral;
import headquarters.view.FormDialog;
import headquarters.view.FuneralListView;
import headquarters.view.HeadquartersView;
import headquarters.view.FuneralMenu;
import headquarters.view.AppLayout;
import headquarters.view.TopMenu;
import headquarters.view.View;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JMenu;
import people.interfaces.ICorpseObserver;
import people.interfaces.IPeople;
import people.models.Events;
import people.person.Corpse;

/**
 * Headquarters module Controller
 *
 * @author Vojtěch Petrus
 */
public final class HeadquartersController implements IHeadquarters, ICorpseObserver {

    private Model model;
    private AppLayout appLayout;
    private FuneralListView funeralListView;
    private HeadquartersView headquartersView;

    @Override
    public IDatabase getDatabase() {
        return model.getDatabase();
    }

    @Override
    public IAccounting getAccountingModule() {
        return model.getAccountingModule();
    }

    @Override
    public IPeople getPeopleModule() {
        return model.getPeopleModule();
    }

    @Override
    public IPR getPRModule() {
        return model.getPRModule();
    }

    @Override
    public IGraves getGravesModule() {
        return model.getGravesModule();
    }

    public HeadquartersController(Model model) {
        this.model = model;
    }

    private void initViews() {
        this.headquartersView = new HeadquartersView();
        this.funeralListView = new FuneralListView(this, model);
    }

    @Override
    public JMenu getMenu() {
        return new FuneralMenu(this);
    }

    @Override
    public View getContentPanel() {
        return funeralListView;
    }

    @Override
    public JFrame getAppLayout() {
        return appLayout;
    }

    @Override
    public final void setContentPanel(View contentPanel) {
        this.appLayout.setContentPanel(contentPanel);
    }

    @Override
    public void showStatus(String status, int stausType) {
        appLayout.showStatus(status, stausType);
    }

    /**
     * Called when application layout is ready to be rendered
     *
     * @param layout main application frame
     */
    public void setAppLayout(AppLayout layout) {
        this.appLayout = layout;
        this.initViews();
        this.funeralListView.setAppLayout(layout);
        this.getPeopleModule().attachCorpseObserver(this);
    }

    public void funeralMenuSelected(ActionEvent ae, int option) {
        switch (option) {
            case FuneralMenu.LIST:
                setContentPanel(funeralListView);
                break;

        }
    }

    public void funeralHomeMenuSelected(ActionEvent ae, int option) {
        switch (option) {
            case TopMenu.ABOUT:
                appLayout.showAbout();
                break;
            case TopMenu.QUIT:
                System.exit(0);
                break;
        }
    }

    public void btnAddFuneralPressed() {
        funeralListView.addFuneralDialog();
    }

    public void btnEditFuneralPressed(Funeral f) {
        funeralListView.editFuneralDialog(f);
    }

    public void btnDeleteFuneralPressed(Funeral f) {
        if (model.deleteFuneral(f)) {
            appLayout.showStatus("Pohřeb byl úspěšně smazán.", AppLayout.STATUS_OK);
        } else {
            appLayout.showStatus("Pohřeb se nepovedlo smazat.", AppLayout.STATUS_ERROR);

        }
    }

    public void addFuneralFormSubmitted(FormDialog source, Corpse corpse, String place, String guestCount, boolean decoration, Date date) {
        Validator v = new Validator();
        if (v.validateFuneral(corpse, place, guestCount, decoration, date)) {
            if (model.addFuneral(corpse, place, Integer.parseInt(guestCount), decoration, date)) {
                source.setVisible(false);
                appLayout.showStatus("Pohřeb byl úspěšně přidán.", AppLayout.STATUS_OK);
            } else {
                appLayout.showStatus("Pohřeb se nepovedlo přidat.", AppLayout.STATUS_ERROR);
            }
        } else {
            appLayout.showStatus(v.getMessage(), AppLayout.STATUS_ERROR);
        }
    }

    public void editFuneralFormSubmitted(FormDialog source, Corpse corpse, String place, String guestCount, boolean decoration, Date date, int id) {
        Validator v = new Validator();
        if (v.validateFuneral(corpse, place, guestCount, decoration, date)) {
            if (model.editFuneral(corpse, place, Integer.parseInt(guestCount), decoration, date, id)) {
                source.setVisible(false);
                appLayout.showStatus("Pohřeb byl úspěšně upraven.", AppLayout.STATUS_OK);
            } else {
                appLayout.showStatus("Pohřeb se nepovedlo upravit.", AppLayout.STATUS_ERROR);
            }
        } else {
            appLayout.showStatus(v.getMessage(), AppLayout.STATUS_ERROR);
        }
    }

    @Override
    public void corpseUpdated(int id, int event) {
        switch (event) {
            case Events.CORPSE_DELETED:
                model.deleteFuneralsOfCorpse(id);
                break;
        }
    }

}
