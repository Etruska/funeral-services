package headquarters.controller;

import headquarters.view.View;
import javax.swing.JMenu;

/**
 * Interface common to all Controller classes
 * @author Vojtěch Petrus
 */
public interface IController {
    
    public View getContentPanel();
    public JMenu getMenu();
}
