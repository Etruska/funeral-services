package headquarters.view;

import headquarters.controller.HeadquartersController;
import headquarters.model.Model;
import headquarters.model.ModelObserver;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Main window with application layout
 * @author Vojtěch Petrus
 */
public class AppLayout extends JFrame implements ModelObserver {

    private Model model;
    private HeadquartersController controller;
    private JPanel contentBoard;
    private JLabel statusBar;
    private CardLayout boardLayout;

    public static final int STATUS_OK = 1;
    public static final int STATUS_ERROR = 2;
    public static final int STATUS_INFO = 3;

    List<View> contentPanels = new ArrayList<View>();

    public AppLayout(Model model, HeadquartersController controller) {
        try {

            this.model = model;
            this.controller = controller;

            controller.setAppLayout(this);

            this.setLayout(new BorderLayout());
            
            this.setSize(new Dimension(ViewConfig.WINDOW_WIDTH, ViewConfig.WINDOW_HEIGHT));
            int[] position = getCenterPosition(ViewConfig.WINDOW_WIDTH, ViewConfig.WINDOW_HEIGHT);
            this.setLocation(position[0], position[1]);

            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setTitle("Pohřební služba");
            this.setResizable(true);

            this.add(new TopMenu(model, controller), BorderLayout.NORTH);

            this.contentBoard = new JPanel();
            this.contentBoard.setLayout(new CardLayout());
            this.boardLayout = (CardLayout) contentBoard.getLayout();
            this.setContentPanel(controller.getContentPanel());
            this.add(contentBoard, BorderLayout.CENTER);

            this.statusBar = new JLabel("Pohřební ústav v. 0.23");
            this.statusBar.setName("stausBar");
            this.statusBar.setBackground(Color.white);
            this.statusBar.setOpaque(true);
            this.statusBar.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
            this.add(this.statusBar, BorderLayout.SOUTH);

        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }

    }

    public void modelUpdated() {
    }

    public final void setContentPanel(View contentPanel) {
        String panelName = contentPanel.getViewName();
        if (!contentPanelExists(panelName)) {
            contentPanels.add(contentPanel);
            contentBoard.add(contentPanel, panelName);
        }
        boardLayout.show(contentBoard, panelName);
    }

    public boolean contentPanelExists(String viewName) {

        for (View panel : contentPanels) {
            if (viewName.equals(panel.getViewName())) {
                return true;
            }
        }

        return false;
    }

    public void showAbout() {
        JOptionPane.showMessageDialog(this,
                "Aplikace Pohřební ústav, verze 0.23 \n"
                + "Vytvořili Vojtěch Petrus, Jiří Vycpálek, Filip Vondrášek, \n"
                + "Jakub Baierl a Václav Strnad s láskou v rámci MI-DPO. \n"
                + "Během psaní aplikace nikdo nezemřel.");
    }

    public void showStatus(String text, int type) {
        this.statusBar.setText(text);

        Font font = this.statusBar.getFont();
        Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
        this.statusBar.setFont(boldFont);

        switch (type) {
            case STATUS_OK:
                this.statusBar.setForeground(ViewConfig.COLOR_GREEN);
                break;
            case STATUS_ERROR:
                this.statusBar.setForeground(ViewConfig.COLOR_RED);
                break;
            case STATUS_INFO:
                this.statusBar.setForeground(ViewConfig.COLOR_BLUE);
                break;
        }

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                clearStatus();
            }
        }, ViewConfig.STATUS_DURATION
        );
    }

    public String getStatus(){
        return this.statusBar.getText();
    }
    
    public void clearStatus() {
        this.statusBar.setText(" ");
    }

    public static int[] getCenterPosition(int width, int height) {
        int[] position = new int[2];
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

        position[0] = (int) (screen.getWidth() / 2 - (width / 2));
        position[1] = (int) (screen.getHeight() / 2 - (height / 2));
        return position;
    }
}
