package headquarters.view;

import headquarters.controller.HeadquartersController;
import headquarters.model.entities.Funeral;
import static headquarters.view.AppLayout.getCenterPosition;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.jdesktop.swingx.JXDatePicker;
import people.person.Corpse;
/**
 *
 * Add/edit funeral dialog
 * @author Vojtěch Petrus
 */
public class FuneralDialog extends FormDialog {
    
    private JXDatePicker date;
    private JLabel labelDate;
    private JLabel labelGuestCount;
    private JTextField guestCount;
    private JLabel labelDecorations;
    private JCheckBox decorations;
    private JLabel labelPlace;
    private JTextField place;
    private JLabel labelCorpse;
    private JComboBox corpse;
    private Funeral funeral;
    private int width;
    private int height;

    public FuneralDialog(HeadquartersController controller, JFrame parent, List<Corpse> corpses) {
        super(controller, parent, "Přidat pohřeb");
        
        this.funeral = null;
        this.initComponents(corpses);
        this.setHandlers();
    }

    public FuneralDialog(HeadquartersController controller, JFrame parent, List<Corpse> corpses, Funeral funeral) {
        super(controller, parent, "Upravit pohřeb");
        this.funeral = funeral;
        this.initComponents(corpses);
        this.setValues(funeral);
        this.setHandlers();
    }

    private void initComponents(List<Corpse> corpses) {
        this.width = 300;
        this.height = 230;
        setSize(width, height);
        int[] position = getCenterPosition(width, height);
        this.setLocation(position[0], position[1]);

        this.labelCorpse = new JLabel("Zesnulý:");
        this.corpse = new JComboBox(corpses.toArray());
        this.addPair(labelCorpse, corpse);

        this.labelGuestCount = new JLabel("Počet hostů:");
        this.guestCount = new JTextField();
        this.addPair(labelGuestCount, guestCount);

        this.labelPlace = new JLabel("Místo konání:");
        this.place = new JTextField();
        this.addPair(labelPlace, place);
        
        this.labelDate = new JLabel("Datum konání:");
        this.date = new JXDatePicker();
        this.addPair(labelDate, date);

        this.labelDecorations = new JLabel("Dekorace:");
        this.decorations = new JCheckBox();
        this.addPair(labelDecorations, decorations);
        this.drawPairs();
    }

    private void setValues(Funeral f) {
        this.corpse.setSelectedItem(f.getCorpse());
        this.guestCount.setText(Integer.toString(f.getGuestCount()));
        this.place.setText(f.getPlace());
        this.decorations.setSelected(f.isDecoration());
        this.date.setDate(f.getDate());
    }

    public String getPlace(){
        return this.place.getText();
    }
    
    private void setHandlers() {
        this.CancelBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                setVisible(false);
            }
        });

        this.OkBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (funeral == null) {
                    controller.addFuneralFormSubmitted(FuneralDialog.this, (Corpse) corpse.getSelectedItem(), place.getText(), guestCount.getText(), decorations.isSelected(), date.getDate());
                } else {
                    controller.editFuneralFormSubmitted(FuneralDialog.this, (Corpse) corpse.getSelectedItem(), place.getText(), guestCount.getText(), decorations.isSelected(), date.getDate(), funeral.getId());
                }

            }
        });
    }

}
