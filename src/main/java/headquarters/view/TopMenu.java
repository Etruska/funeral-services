package headquarters.view;

import headquarters.controller.HeadquartersController;
import headquarters.model.Model;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.*;

/**
 * Application menu
 * @author Vojtěch Petrus
 */
public class TopMenu extends JMenuBar {

    private HeadquartersController controller;
    private JMenu funeralHomeMenu;
    private JMenuItem about;
    private JMenuItem quit;

    public static final int ABOUT = 1;
    public static final int QUIT = 2;

    public TopMenu(Model model, HeadquartersController c) {

        this.controller = c;
        this.funeralHomeMenu = new JMenu("Pohřební ústav");
        this.about = new JMenuItem("O aplikaci");
        this.quit = new JMenuItem("Ukončit");

        funeralHomeMenu.add(about);
        funeralHomeMenu.add(quit);

        this.about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.funeralHomeMenuSelected(ae, ABOUT);
            }
        });

        this.quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.funeralHomeMenuSelected(ae, QUIT);
            }
        });

        this.add(funeralHomeMenu);
        
        List<JMenu> menus =  model.getMenus();
        for(JMenu menu : menus){
            this.add(menu);
        }        
    }

    public void addMenu(JMenu menu) {
        this.add(menu);
    }
}
