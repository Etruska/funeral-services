package headquarters.view;

import headquarters.controller.HeadquartersController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Funeral menu which will be placed to menu bar
 * @author Vojtěch Petrus
 */
public class FuneralMenu extends JMenu {

    private final HeadquartersController controller;
    private JMenuItem list;
    public static final int LIST = 1;
    public static final int ADD_NEW = 2;

    public FuneralMenu(HeadquartersController c) {
        super("Pohřby");
        this.controller = c;
        this.list = new JMenuItem("Seznam pohřbů");

        this.list.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.funeralMenuSelected(ae, LIST);
            }
        });


        this.add(list);
    }
}
