package headquarters.view;

import javax.swing.JLabel;

/**
 * Application homepage
 * @author Vojtěch Petrus
 */
public class HeadquartersView extends View{

    public String getViewName() {
        return "Headquarters/Main";
    }

    
    public HeadquartersView() {
        JLabel label = new JLabel("Zde bude obsah ředitelství");
        this.add(label);
    }
    
}
