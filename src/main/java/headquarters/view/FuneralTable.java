package headquarters.view;

import headquarters.model.FuneralTableModel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author etruska
 */
public class FuneralTable extends JTable {

    FuneralTableModel tableModel;
    FuneralListView view;

    public FuneralTable(FuneralTableModel tableModel, FuneralListView view) {
        super(tableModel);
        this.tableModel = tableModel;
        this.view = view;
        this.setHandlers();
    }

    private void setHandlers() {
        this.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent event) {
                if (event.getValueIsAdjusting()) {
                    return;
                }
                if(FuneralTable.this.getSelectedRows().length > 0){
                    view.enableButtons();
                } else {
                    view.disableButtons();
                }
            }
        });
    }

}
