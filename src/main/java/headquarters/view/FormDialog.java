package headquarters.view;

import headquarters.controller.HeadquartersController;
import java.util.ArrayList;
import java.util.List;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.Group;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Class for popup dialog forms
 * @author Vojtěch Petrus
 */
public abstract class FormDialog extends JDialog {

    protected JPanel panel;
    protected GroupLayout layout;

    protected JButton OkBtn;
    protected JButton CancelBtn;

    protected List<JLabel> labels;
    protected List<JComponent> inputs;

    protected HeadquartersController controller;
    
    public FormDialog(HeadquartersController controller, JFrame parent, String caption) {
        super(parent, caption, true);
        this.controller = controller;

        this.panel = new JPanel();
        this.layout = new GroupLayout(this.panel);
        this.panel.setLayout(this.layout);
        this.labels = new ArrayList<JLabel>();
        this.inputs = new ArrayList<JComponent>();

        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        this.add(panel);

        JPanel buttonsPanel = new JPanel();
        this.OkBtn = new JButton("Ok");
        this.CancelBtn = new JButton("Zrušit");
        buttonsPanel.add(OkBtn);
        buttonsPanel.add(CancelBtn);
        this.add(buttonsPanel, "South");
    }

    protected void addPair(JLabel label, JComponent input) {
        label.setLabelFor(input);
        labels.add(label);
        inputs.add(input);
    }

    protected void drawPairs() {
        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
        Group hGroup1 = layout.createParallelGroup();
        Group hGroup2 = layout.createParallelGroup();
        for (JComponent l : labels) {
            hGroup1.addComponent(l);
        }
        for (JComponent i : inputs) {
            hGroup2.addComponent(i);
        }
        hGroup.addGroup(hGroup1);
        hGroup.addGroup(hGroup2);
        layout.setHorizontalGroup(hGroup);

        GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
        for (int i = 0; i < labels.size(); i++) {
            vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE).
                    addComponent(labels.get(i)).addComponent(inputs.get(i)));
        }
        layout.setVerticalGroup(vGroup);
    }
  
}
