package headquarters.view;

import javax.swing.JPanel;


/**
 *  Abstract view class tha all content views must extend
 * @author Vojtěch Petrus
 */
public abstract class View extends JPanel{
    
    public abstract String getViewName();
}
