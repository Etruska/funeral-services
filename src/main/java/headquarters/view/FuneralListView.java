package headquarters.view;

import headquarters.controller.HeadquartersController;
import headquarters.model.FuneralTableModel;
import headquarters.model.Model;
import headquarters.model.ModelObserver;
import headquarters.model.entities.Funeral;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

/**
 *
 * @author Vojtěch Petrus
 */
public class FuneralListView extends View implements ModelObserver {

    private JButton addFuneralButton;
    private JButton editFuneralButton;
    private JButton deleteFuneralButton;
    private AppLayout appLayout;
    private HeadquartersController controller;
    private Model model;
    private FuneralTable table;
    private List<Funeral> funerals;
    private FuneralTableModel tableModel;

    public void setAppLayout(AppLayout layout) {
        this.appLayout = layout;
    }

    @Override
    public String getViewName() {
        return "Headquarters/FuneralList";
    }

    public FuneralListView(HeadquartersController con, Model model) {
        this.controller = con;
        this.model = model;
        this.addFuneralButton = new JButton("Nový pohřeb");
        this.editFuneralButton = new JButton("Upravit");
        this.deleteFuneralButton = new JButton("Smazat");
        this.setLayout(new GridBagLayout());
        this.funerals = model.getFuneralDAO().getAllFunerals();

        initComponents();
        setHandlers();
        model.attachObserver(this);
    }

    @Override
    public void modelUpdated() {
        this.funerals = model.getFuneralDAO().getAllFunerals();

        if (funerals.isEmpty()) {
            hideButtons();
        } else {
            showButtons();
        }

        this.tableModel.setFunerals(funerals);
        this.tableModel.fireTableDataChanged();
        this.revalidate();
    }

    private void hideButtons() {
        this.editFuneralButton.setVisible(false);
        this.deleteFuneralButton.setVisible(false);
    }

    private void showButtons() {
        this.editFuneralButton.setVisible(true);
        this.deleteFuneralButton.setVisible(true);
    }

    public void enableButtons() {
        this.editFuneralButton.setEnabled(true);
        this.deleteFuneralButton.setEnabled(true);
    }

    public void disableButtons() {
        this.editFuneralButton.setEnabled(false);
        this.deleteFuneralButton.setEnabled(false);
    }

    private void setHandlers() {
        this.addFuneralButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.btnAddFuneralPressed();
            }
        });

        this.editFuneralButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                List<Funeral> list = new ArrayList<>();
                for (int row : table.getSelectedRows()) {
                    list.add(tableModel.getRow(row));
                }
                for (Funeral f : list) {
                    controller.btnEditFuneralPressed(f);
                }
            }
        });

        this.deleteFuneralButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                List<Funeral> list = new ArrayList<>();
                for (int row : table.getSelectedRows()) {
                    list.add(tableModel.getRow(row));
                }
                for (Funeral f : list) {
                    controller.btnDeleteFuneralPressed(f);
                }
            }
        });
    }

    private void initComponents() {
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(10, 10, 10, 10);
        c.gridx = 0;
        c.gridy = 0;
        this.add(new JLabel("Seznam pohřbů"), c);

        c.weightx = 0.001;
        c.anchor = GridBagConstraints.LINE_END;
        c.insets = new Insets(10, 0, 0, 10);
        c.gridx = 1;
        c.gridy = 0;
        this.add(this.editFuneralButton, c);

        c.weightx = 0.001;
        c.gridx = 2;
        c.gridy = 0;
        this.add(this.deleteFuneralButton, c);

        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 3;
        c.insets = new Insets(10, 10, 10, 10);
        c.fill = GridBagConstraints.HORIZONTAL;
        this.tableModel = new FuneralTableModel(model, controller);
        this.table = new FuneralTable(tableModel, this);
        this.table.setPreferredScrollableViewportSize(new Dimension(200, 200));
        table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(table);
        this.add(scrollPane, c);

        c.weightx = 0.5;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 1;
        this.add(this.addFuneralButton, c);

        if (this.funerals.isEmpty()) {
            hideButtons();
        }
        this.disableButtons();
    }

    public void addFuneralDialog() {
        if (model.getCorpses().size() < 1) {
            controller.showStatus("Nejsou žádní zesnulí, přidejte je nejdříve v modulu osob.", AppLayout.STATUS_ERROR);
        } else {
            JDialog f = new FuneralDialog(controller, appLayout, model.getCorpses());
            f.setVisible(true);
        }
    }

    public void editFuneralDialog(Funeral funeral) {

        JDialog f = new FuneralDialog(controller, appLayout, model.getCorpses(), funeral);
        f.setVisible(true);
    }

}
