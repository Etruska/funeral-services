package headquarters.model;

/**
 * Static configuration of application parameters
 * @author Vojtěch Petrus
 */
public class ModelConfig {
    public static final String LOG_FOLDER = "log";
}
