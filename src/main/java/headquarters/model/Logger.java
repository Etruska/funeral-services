package headquarters.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Logs important events to log folder
 * @author Vojtěch Petrus
 */
public class Logger {

    private String filename;
    private String folder;

    public Logger(String folder) {
        this.folder = folder;
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
        Date date = new Date();
        this.filename = folder + "/log_" + dateFormat.format(date) + ".txt";
    }

    public void logException(String message) {
        try {
            PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
            writer.println(message);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    public void logException(String message, String className, String methodName) {
        logException("Exception in " + className + "." + methodName + ": " + message);
    }

}
