package headquarters.model.entities;

import java.util.Date;
import java.util.Objects;
import people.person.Corpse;

/**
 * Funeral entity
 * @author Vojtěch Petrus
 */
public class Funeral {

    private Corpse corpse;
    private String place;
    private int guestCount;
    private boolean decoration;
    private Date date;
    private int id;
    
    public Funeral(Corpse corpse, String place, int guestCount, boolean decoration, Date date) {
        this.corpse = corpse;
        this.place = place;
        this.guestCount = guestCount;
        this.decoration = decoration;
        this.date = date;
        this.id = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Corpse getCorpse() {
        return corpse;
    }

    public String getPlace() {
        return place;
    }

    public int getGuestCount() {
        return guestCount;
    }

    public boolean isDecoration() {
        return decoration;
    }

    public Date getDate() {
        return date;
    }
    
    @Override
    public String toString(){
        return corpse.getName() + " " + corpse.getSurname() + " - " + place;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.corpse);
        hash = 67 * hash + Objects.hashCode(this.place);
        hash = 67 * hash + this.guestCount;
        hash = 67 * hash + (this.decoration ? 1 : 0);
        hash = 67 * hash + Objects.hashCode(this.date);
        hash = 67 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Funeral other = (Funeral) obj;
        if (!Objects.equals(this.corpse, other.corpse)) {
            return false;
        }
        if (!Objects.equals(this.place, other.place)) {
            return false;
        }
        if (this.guestCount != other.guestCount) {
            return false;
        }
        if (this.decoration != other.decoration) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    
    
}
