package headquarters.model;

import headquarters.controller.HeadquartersController;
import headquarters.model.entities.Funeral;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * Table model for funeral JTable
 *
 * @author Vojtěch Petrus
 */
public class FuneralTableModel extends AbstractTableModel {

    private Model model;
    private List<Funeral> funerals;
    private HeadquartersController controller;

    private String[] columnNames = {"ID",
        "Zesnulý",
        "Místo pohřbu",
        "Datum pohřbu",
        "Počet hostů",
        "Dekorace"};

    public FuneralTableModel(Model model, HeadquartersController c) {
        this.model = model;
        this.controller = c;
        this.funerals = this.model.getFuneralDAO().getAllFunerals();
    }

    @Override
    public int getRowCount() {
        return funerals.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Funeral getRow(int rowIndex) {
        if (rowIndex < funerals.size()) {
            return funerals.get(rowIndex);
        }
        return null;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Funeral f = funerals.get(rowIndex);
        Object value = "?";
        switch (columnIndex) {
            case 0:
                value = f.getId();
                break;
            case 1:
                value = f.getCorpse();
                break;
            case 2:
                value = f.getPlace();
                break;
            case 3:
                value = f.getDate();
                break;
            case 4:
                value = f.getGuestCount();
                break;
            case 5:
                value = (f.isDecoration()) ? "ano" : "ne";
                break;

        }
        return value;
    }

    public void setFunerals(List<Funeral> funerals) {
        this.funerals = funerals;
    }

}
