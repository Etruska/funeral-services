package headquarters.model;

import AccountingController.IAccounting;
import PR.Controller.IPR;
import graves.controller.IGraves;
import headquarters.model.db.Database;
import headquarters.model.db.FuneralDAO;
import headquarters.model.db.IDatabase;
import headquarters.model.entities.Funeral;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JMenu;
import people.interfaces.IPeople;
import people.person.Corpse;

/**
 * Model class, communicates with DAO classes and informs its observers about
 * changes
 *
 * @author Vojtěch Petrus
 */
public class Model {

    private List<ModelObserver> observers = new ArrayList<ModelObserver>();
    private List<JMenu> menus = new ArrayList<JMenu>();

    private IGraves gravesModule;
    private IPR PRModule;
    private IPeople peopleModule;
    private IAccounting accountingModule;
    private IDatabase database;
    private FuneralDAO funeralDAO = null;

    public Model() {
        this.database = new Database();
    }

    public FuneralDAO getFuneralDAO() {

        if (this.funeralDAO == null) {
            this.funeralDAO = new FuneralDAO(this, this.database);
        }
        return funeralDAO;
    }

    public void setFuneralDAO(FuneralDAO d) {
        this.funeralDAO = d;
    }

    public IDatabase getDatabase() {
        return database;
    }

    public IAccounting getAccountingModule() {
        return accountingModule;
    }

    public void setAccountingModule(IAccounting AccountingModule) {
        this.accountingModule = AccountingModule;
    }

    public List<ModelObserver> getObservers() {
        return observers;
    }

    public void setObservers(List<ModelObserver> observers) {
        this.observers = observers;
    }

    public IGraves getGravesModule() {
        return gravesModule;
    }

    public void setGravesModule(IGraves gravesModule) {
        this.gravesModule = gravesModule;
    }

    public IPR getPRModule() {
        return PRModule;
    }

    public void setPRModule(IPR PRModule) {
        this.PRModule = PRModule;
    }

    public IPeople getPeopleModule() {
        return peopleModule;
    }

    public void setPeopleModule(IPeople PeopleModule) {
        this.peopleModule = PeopleModule;
    }

    private void notifyObservers() {
        for (ModelObserver o : observers) {
            o.modelUpdated();
        }
    }

    public void attachObserver(ModelObserver observer) {
        observers.add(observer);
    }

    public void dettachObserver(ModelObserver observer) {
        observers.remove(observer);
    }

    public List<JMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<JMenu> menus) {
        this.menus = menus;
    }

    public ArrayList<Corpse> getCorpses() {
        return peopleModule.getCorpses();
    }

    public boolean addFuneral(Corpse corpse, String place, int guestCount, boolean decoration, Date date) {
        Funeral f = new Funeral(corpse, place, guestCount, decoration, date);
        boolean result = this.funeralDAO.addFuneral(f);
        notifyObservers();
        return result;
    }

    public boolean editFuneral(Corpse corpse, String place, int guestCount, boolean decoration, Date date, int id) {
        Funeral f = new Funeral(corpse, place, guestCount, decoration, date);
        f.setId(id);
        boolean result = this.funeralDAO.editFuneral(f);
        notifyObservers();
        return result;
    }

    public boolean deleteFuneral(Funeral f) {
        boolean result = this.funeralDAO.deleteFuneral(f);
        notifyObservers();
        return result;
    }

    public boolean deleteFuneralsOfCorpse(int id) {
        boolean result = this.funeralDAO.deleteFuneralsOfCorpse(id);
        notifyObservers();
        return result;
    }

    public void debugNotify() {
        this.notifyObservers();
    }

}
