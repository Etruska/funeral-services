
package headquarters.model;

import AccountingController.Accounting;
import AccountingController.IAccounting;
import PR.Controller.IPR;
import PR.Controller.PR;
import graves.controller.GravesController;
import graves.controller.IGraves;
import headquarters.controller.HeadquartersController;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JMenu;
import people.controllers.PeopleController;
import people.interfaces.IPeople;

/**
 * Inits modules and collects their menus for menu bar
 * @author Vojtěch Petrus
 */
public class ModuleBuilder {

    private Model model;
    private HeadquartersController hqController;
    private List<JMenu> menus = new ArrayList<>();

    public ModuleBuilder(Model model, HeadquartersController hqController) {
        this.model = model;
        this.hqController = hqController;
        menus.add(hqController.getMenu());
    }

    public void buildGravesModule() {
        IGraves gravesController = new GravesController(hqController);
        model.setGravesModule(gravesController);
        menus.add(gravesController.getMenu());
    }

    public void buildPRModule() {
        IPR controller = new PR(hqController);
        model.setPRModule(controller);
        menus.add(controller.getMenu());
    }

    public void buildPeopleModule() {
        IPeople controller = new PeopleController(hqController);
        model.setPeopleModule(controller);
        menus.add(controller.getMenu());
    }

    public void buildAccountingModule() {
        IAccounting controller = new Accounting(hqController);
        model.setAccountingModule(controller);
        menus.add(controller.getMenu());
    }

    public List<JMenu> getMenus() {
        return menus;
    }

}
