package headquarters.model;

import headquarters.controller.HeadquartersController;
import java.util.List;
import javax.swing.JMenu;

/**
 * Controls ModuleBuilder and launches application
 * @author Vojtěch Petrus
 */
public final class ApplicationDirector {
    
    private Model model;
    private HeadquartersController controller;
    private ModuleBuilder builder;
 
    public ApplicationDirector(Model model, HeadquartersController controller, ModuleBuilder builder) {
        this.model = model;
        this.controller = controller;
        this.builder = builder;
    }
    
    public void run(){
        builder.buildGravesModule();
        builder.buildPRModule();
        builder.buildPeopleModule();
        builder.buildAccountingModule();
        List<JMenu> menus = builder.getMenus();
        model.setMenus(menus);
    }
}
