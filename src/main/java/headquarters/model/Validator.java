package headquarters.model;

import java.util.Date;
import people.person.Corpse;

/**
 * Checks wheater data recieved from view make valid Funeral object
 * @author Vojtěch Petrus
 */
public class Validator {

    protected String message;

    public boolean validateFuneral(Corpse corpse, String place, String guestCount, boolean decoration, Date date) {
        try {
            int count = Integer.parseInt(guestCount);
            return validateFuneral(corpse, place, count, decoration, date);
        } catch (NumberFormatException e) {
            this.message = "Počet hostů je ve špatném formátu.";
            return false;
        }
    }

    public boolean validateFuneral(Corpse corpse, String place, int guestCount, boolean decoration, Date date) {

        if (corpse.getCorpseID() == 0) {
            this.message = "Zesnulý nebyl nalezen.";
            return false;
        }

        if (place.isEmpty()) {
            this.message = "Místo konání nebylo vyplněno.";
            return false;
        }

        if (guestCount <= 0) {
            this.message = "Počet hostů je ve špatném formátu.";
            return false;
        }
        if (date == null) {
            this.message = "Datum je ve špatném formátu.";
            return false;
        }

        this.message = "OK.";
        return true;
    }

    public String getMessage() {
        return message;
    }

}
