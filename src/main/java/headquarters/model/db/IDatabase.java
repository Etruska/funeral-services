package headquarters.model.db;

import java.sql.Connection;

/**
 * Interface providing DB access and methods (not implemented yet) for basic SQL operations
 * @author Vojtěch Petrus
 */
public interface IDatabase {
    
    public Connection getConnection();
    
}
