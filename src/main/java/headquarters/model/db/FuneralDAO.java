package headquarters.model.db;

import headquarters.model.Logger;
import headquarters.model.Model;
import headquarters.model.ModelConfig;
import headquarters.model.entities.Funeral;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import people.person.Corpse;

/**
 *
 * @author Vojtěch Petrus
 */
public class FuneralDAO {

    private IDatabase database;
    private Connection conn;
    private Model model;
    private Logger logger;

    public FuneralDAO(Model model, IDatabase database) {
        this.model = model;
        this.database = database;
        this.conn = database.getConnection();
        this.logger = new Logger(ModelConfig.LOG_FOLDER);

    }

    public boolean addFuneral(Funeral funeral) {
        String sql = "INSERT INTO funeral (corpse_id, guest_count, place, decoration, event_date) VALUES "
                + "(?, ?, ?, ?, ?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, funeral.getCorpse().getCorpseID());
            ps.setInt(2, funeral.getGuestCount());
            ps.setString(3, funeral.getPlace());
            ps.setBoolean(4, funeral.isDecoration());
            ps.setTimestamp(5, new Timestamp(funeral.getDate().getTime()));
            ps.execute();
        } catch (SQLException ex) {
            logger.logException(ex.getLocalizedMessage(), this.getClass().getSimpleName(), "addFuneral");
            return false;
        }

        return true;
    }

    public boolean editFuneral(Funeral funeral) {
        String sql = "UPDATE funeral SET corpse_id =?, guest_count=?, place=?, decoration=?, event_date=? WHERE id =?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, funeral.getCorpse().getCorpseID());
            ps.setInt(2, funeral.getGuestCount());
            ps.setString(3, funeral.getPlace());
            ps.setBoolean(4, funeral.isDecoration());
            ps.setTimestamp(5, new Timestamp(funeral.getDate().getTime()));
            ps.setInt(6, funeral.getId());
            ps.execute();
        } catch (SQLException ex) {
            logger.logException(ex.getLocalizedMessage(), this.getClass().getSimpleName(), "editFuneral");
            return false;
        }
        return true;
    }

    public boolean deleteFuneral(Funeral funeral) {
        String sql = "DELETE FROM funeral WHERE id=?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, funeral.getId());
            ps.execute();
        } catch (SQLException ex) {
            logger.logException(ex.getLocalizedMessage(), this.getClass().getSimpleName(), "deleteFuneral");
            return false;
        }
        return true;
    }

    public List<Funeral> getAllFunerals() {
        List<Funeral> list = new ArrayList<Funeral>();

        String sql = "SELECT id, place, guest_count, decoration, corpse_id, event_date FROM funeral";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int id = rs.getInt(1);
                String place = rs.getString(2);
                int guestCount = rs.getInt(3);
                boolean decoration = rs.getBoolean(4);
                int corpseId = rs.getInt(5);
                Date date = rs.getDate(6);
                Corpse corpse = model.getPeopleModule().getCorpseByID(corpseId);
                Funeral f = new Funeral(corpse, place, guestCount, decoration, date);
                f.setId(id);
                list.add(f);
            }

        } catch (SQLException ex) {
            logger.logException(ex.getLocalizedMessage(), this.getClass().getSimpleName(), "getAllFunerals");
            return list;
        }
        return list;
    }
    
    public boolean deleteFuneralsOfCorpse(int id) {
        String sql = "DELETE FROM funeral WHERE corpse_id=?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.execute();
        } catch (SQLException ex) {
            logger.logException(ex.getLocalizedMessage(), this.getClass().getSimpleName(), "deleteFuneralsOfCorpse");
            return false;
        }
        return true;
    }

}
