package headquarters.model.db;

import java.sql.*;

/**
 * Class responsible for connecting to DB
 * @author Vojtěch Petrus
 */
public class DerbyConnection {

    private static Connection connection = null;

    public static Connection getConnection() {
        if (DerbyConnection.connection == null) {
            connect();
        }
        return DerbyConnection.connection;
    }

    private static void connect() {
        try {
            String url = "jdbc:derby://localhost:1527/funeral-service";
            Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
            DerbyConnection.connection = DriverManager.getConnection(url, "fun", "1234");
        } catch (ClassNotFoundException e) {
            System.err.println(e.getMessage());
            System.exit(0);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            System.exit(0);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.exit(0);
        }
    }
}
