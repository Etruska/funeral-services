package headquarters.model.db;

import java.sql.Connection;

/**
 * Returns connection to DB
 * @author Vojtěch Petrus
 */
public class Database implements IDatabase {
    
    public Connection getConnection(){
        return DerbyConnection.getConnection();
    }
    
}
