/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AccountingModel;

import headquarters.controller.IHeadquarters;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Václav
 */
public class TextDAO {

    IHeadquarters hq;

    public TextDAO(IHeadquarters hq) {
        this.hq = hq;
    }

    public ResultSet find(String tableName, HashMap args) {
        Connection conn = hq.getDatabase().getConnection();
        Statement statement;
        String sql = "SELECT * FROM " + tableName;
        ResultSet rs = null;
        if (!args.isEmpty()) {
            sql += " WHERE ";

            Iterator it = args.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();
                sql += pairs.getKey();
                sql += " = '";
                sql += pairs.getValue();
                sql += "'";
                if (it.hasNext()) {
                    sql += " AND ";
                }
            }
        }
        try {
            statement = conn.createStatement();
            rs = statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(TextDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rs;
    }

    public void addText(String text, String title, Constants type) {
        Connection conn = hq.getDatabase().getConnection();
        PreparedStatement ps;
        String sql;

        switch (type) {
            case TRANSACTION:
                sql = "INSERT INTO Transactions (amount) VALUES (?)";
                try {
                    ps = conn.prepareStatement(sql);
                    ps.setInt(1, Integer.parseInt(text));
                    ps.execute();
                } catch (SQLException ex) {
                    Logger.getLogger(TextDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case ANNUALREPORT:
                sql = "INSERT INTO AnnualReports (title, text) VALUES (?, ?)";
                try {
                    ps = conn.prepareStatement(sql);
                    ps.setString(1, title);
                    ps.setString(2, text);
                    ps.execute();
                } catch (SQLException ex) {
                    Logger.getLogger(TextDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case BALANCESHEETS:
                sql = "INSERT INTO BalanceSheets (title, text) VALUES (?, ?)";
                try {
                    ps = conn.prepareStatement(sql);
                    ps.setString(1, title);
                    ps.setString(2, text);
                    ps.execute();
                } catch (SQLException ex) {
                    Logger.getLogger(TextDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
    }
}
