/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AccountingModel;

/**
 *
 * @author Václav
 */
public class BalanceSheets implements Text {

    String text, title;

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BalanceSheets(String title, String text) {
        this.title = title;
        this.text = text;
    }
}
