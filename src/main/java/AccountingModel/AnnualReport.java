
package AccountingModel;


public class AnnualReport implements Text {

    String text, title;
    
    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public AnnualReport(String title, String text) {
        this.title = title;
        this.text = text;
    }
}
