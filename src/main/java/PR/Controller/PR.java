/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PR.Controller;

import PR.Model.CompanyMeeting;
import PR.Model.Constants;
import static PR.Model.Constants.ARTICLE;
import static PR.Model.Constants.NEWS;
import PR.Model.DAO;
import PR.Model.Model;
import PR.View.AddAd;
import PR.View.Menu;
import PR.View.AddArticle;
import PR.View.AddMeeting;
import PR.View.AddNews;
import PR.View.ListAds;
import PR.View.ListArticles;
import PR.View.ListMeetings;
import PR.View.ListNews;
import PR.View.Main;
import headquarters.controller.IController;
import headquarters.controller.IHeadquarters;
import headquarters.view.View;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

/**
 *
 * @author Filip Vondrasek
 */
public class PR implements IPR {

    IHeadquarters hq;
    AddArticle addArticleView;
    ListArticles listArticlesView;
    AddNews addNewsView;
    ListNews listNewsView;
    AddAd addAdView;
    ListAds listAdsView;
    AddMeeting addMeetingView;
    ListMeetings listMeetingsView;
    Main mainView;
    JMenu menu;
    DAO dao;
    Model model;
    // temporary variables
    CardLayout boardLayout;
    JPanel contentBoard;
    List<View> contentPanels = new ArrayList<>();
    JFrame window;

    @Override
    public View getContentPanel() {
        return mainView;
    }

    public PR(IHeadquarters hq) {
        this.hq = hq;
        this.dao = new DAO(hq);
        this.model = new Model(this);
        this.addArticleView = new AddArticle(this, model);
        this.listArticlesView = new ListArticles(this, model);
        this.addNewsView = new AddNews(this, model);
        this.listNewsView = new ListNews(this, model);
        this.addAdView = new AddAd(this, model);
        this.listAdsView = new ListAds(this, model);
        this.addMeetingView = new AddMeeting(this, model);
        this.listMeetingsView = new ListMeetings(this, model);

        this.mainView = new Main();

        this.contentBoard = new JPanel();
        this.contentBoard.setLayout(new CardLayout());
        this.boardLayout = (CardLayout) contentBoard.getLayout();
        this.menu = new Menu(this);
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menu);

//        window = new JFrame("Pohrebni ustav");
//        window.setPreferredSize(new Dimension(640, 480));
//        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        window.setLayout(new BorderLayout());
//        window.setJMenuBar(menuBar);
//        window.add(contentBoard, BorderLayout.CENTER);
//        window.pack();
//        window.setVisible(true);
    }

    /* replace the temporary method with this one
     public void setContentPanel(View panel) {
     hq.setContentPanel(panel);
     }
     */
    @Override
    public JMenu getMenu() {
        return menu;
    }

    @Override
    public ResultSet find(String tableName, HashMap args) {
        return dao.find(tableName, args);
    }

    @Override
    public void addCompanyMeeting(ActionEvent ae, CompanyMeeting companyMeeting) {
        if (companyMeeting.getAttendees().isEmpty() || companyMeeting.getDate().isEmpty()) {
            throw new IllegalArgumentException("Date and Attendees cannot be empty.");
        }
        dao.addCompanyMeeting(companyMeeting);
        model.addCompanyMeeting(companyMeeting);
    }

    @Override
    public void addText(ActionEvent ae, String text, String title, Constants type) {
        if (((type == ARTICLE || type == NEWS) && title.isEmpty()) || text.isEmpty()) {
            throw new IllegalArgumentException("Title (in case of News and Articles) and text cannot be empty.");
        }
        dao.addText(text, title, type);
        model.addText(text, title, type);
    }

    public DAO getDAO() {
        return dao;
    }

    @Override
    public void menuSelected(ActionEvent ae, Constants action) {
        switch (action) {
            case ADD_ARTICLE:
                setContentPanel(addArticleView);
                break;
            case LIST_ARTICLES:
                setContentPanel(listArticlesView);
                break;
            case ADD_NEWS:
                setContentPanel(addNewsView);
                break;
            case LIST_NEWS:
                setContentPanel(listNewsView);
                break;
            case ADD_AD:
                setContentPanel(addAdView);
                break;
            case LIST_ADS:
                setContentPanel(listAdsView);
                break;
            case ADD_MEETING:
                setContentPanel(addMeetingView);
                break;
            case LIST_MEETINGS:
                setContentPanel(listMeetingsView);
                break;
            default:
                break;
        }
    }

    public final void setContentPanel(View panel) {
        hq.setContentPanel(panel);
    }

// TEMPORARY METHODS
    public boolean contentPanelExists(String viewName) {
        for (View panel : contentPanels) {
            if (viewName.equals(panel.getViewName())) {
                return true;
            }
        }

        return false;
    }
    public List<View> getContentPanels() {
        return contentPanels;
    }
    
//    public final void setContentPanel(View contentPanel) {
//        String panelName = contentPanel.getViewName();
//        if (!contentPanelExists(panelName)) {
//            contentPanels.add(contentPanel);
//            contentBoard.add(contentPanel, panelName);
//        }
//        boardLayout.show(contentBoard, panelName);
//    }
//
//
//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String[] args) {
//        new PR();
//    }
//
//    public PR() {
//        this(new IHeadquarters());
//    }
}
