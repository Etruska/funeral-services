/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package PR.Controller;

import PR.Model.CompanyMeeting;
import PR.Model.Constants;
import headquarters.controller.IController;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.util.HashMap;

/**
 *
 * @author Filip Vondrasek
 */
public interface IPR extends IController {
    ResultSet find(String tableName, HashMap args);
    void addCompanyMeeting(ActionEvent ae, CompanyMeeting companyMeeting);

    public void addText(ActionEvent ae, String text, String title, Constants type);

    public void menuSelected(ActionEvent ae, Constants constants);
}
