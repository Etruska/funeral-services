/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package PR.Model;

/**
 *
 * @author Filip Vondrasek
 */
public class Article implements Text {

    String text, title;
    
    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public Article(String title, String text) {
        this.title = title;
        this.text = text;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Article)) {
            return false;
        }

        Article article = (Article) o;

        return article.getText().equals(this.getText()) && article.getTitle().equals(this.getTitle());
    }
}
