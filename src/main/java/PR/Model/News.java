/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PR.Model;

/**
 *
 * @author Filip Vondrasek
 */
public class News implements Text {

    String text, title;

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public News(String title, String text) {
        this.title = title;
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof News)) {
            return false;
        }

        News news = (News) o;

        return news.getText().equals(this.getText()) && news.getTitle().equals(this.getTitle());
    }
}
