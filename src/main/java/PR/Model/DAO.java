/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PR.Model;

import headquarters.controller.IHeadquarters;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Filip Vondrasek
 */
public class DAO {

    IHeadquarters hq;

    public DAO(IHeadquarters hq) {
        this.hq = hq;
    }

    public void execute(String sql) {
        Connection conn = hq.getDatabase().getConnection();
        Statement statement;
        try {
            statement = conn.createStatement();
            statement.execute(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ResultSet find(String tableName, HashMap args) {
        Connection conn = hq.getDatabase().getConnection();
        Statement statement;
        String sql = "SELECT * FROM " + tableName;
        ResultSet rs = null;
        if (!args.isEmpty()) {
            sql += " WHERE ";

            Iterator it = args.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();
                sql += pairs.getKey();
                sql += " = '";
                sql += pairs.getValue();
                sql += "'";
                if (it.hasNext()) {
                    sql += " AND ";
                }
            }
        }
        try {
            statement = conn.createStatement();
            rs = statement.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rs;
    }

    public void addText(String text, String title, Constants type) {
        Connection conn = hq.getDatabase().getConnection();
        PreparedStatement ps;
        String sql;

        switch (type) {
            case AD:
                sql = "INSERT INTO Advertisements (text) VALUES (?)";
                try {
                    ps = conn.prepareStatement(sql);
                    ps.setString(1, text);
                    ps.execute();
                } catch (SQLException ex) {
                    Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case ARTICLE:
                sql = "INSERT INTO Articles (title, text) VALUES (?, ?)";
                try {
                    ps = conn.prepareStatement(sql);
                    ps.setString(1, title);
                    ps.setString(2, text);
                    ps.execute();
                } catch (SQLException ex) {
                    Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case NEWS:
                sql = "INSERT INTO News (title, text) VALUES (?, ?)";
                try {
                    ps = conn.prepareStatement(sql);
                    ps.setString(1, title);
                    ps.setString(2, text);
                    ps.execute();
                } catch (SQLException ex) {
                    Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
    }

    public void addCompanyMeeting(CompanyMeeting companyMeeting) {
        Connection conn = hq.getDatabase().getConnection();
        PreparedStatement ps;
        String sql = "INSERT INTO Meetings (date, attendees) VALUES (?, ?)";

        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, companyMeeting.getDate());
            ps.setString(2, companyMeeting.getAttendees());
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
