/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package PR.Model;

/**
 *
 * @author Filip Vondrasek
 */
public interface ModelObserver {
    public void modelUpdated();
}
