/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PR.Model;

import PR.Controller.IPR;
import PR.View.ListAds;
import PR.View.ListArticles;
import PR.View.ListMeetings;
import PR.View.ListNews;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Filip Vondrasek
 */
public class Model {

    private List<ModelObserver> observers;
    List<Article> articles;
    List<CompanyMeeting> companyMeetings;
    List<News> news;
    List<Advertisement> ads;
    IPR controller;

    public Model(IPR controller) {
        this.controller = controller;
        observers = new ArrayList<>();
        this.news = getNewsFromDB();
        this.articles = getArticlesFromDB();
        this.ads = getAdsFromDB();
        this.companyMeetings = getMeetingsFromDB();
    }

    public final List<Advertisement> getAdsFromDB() {
        ResultSet rs = controller.find("Advertisements", new HashMap());
        List<Advertisement> adsList = new ArrayList<>();
        try {
            while (rs.next()) {
                Advertisement ad = new Advertisement(rs.getString(1));
                adsList.add(ad);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListAds.class.getName()).log(Level.SEVERE, null, ex);
        }

        return adsList;
    }

    public final List<Article> getArticlesFromDB() {
        HashMap args = new HashMap();
        ResultSet rs = controller.find("Articles", args);
        List<Article> articlesList = new ArrayList<>();
        try {
            while (rs.next()) {
                Article article = new Article(rs.getString(1), rs.getString(2));
                articlesList.add(article);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListArticles.class.getName()).log(Level.SEVERE, null, ex);
        }

        return articlesList;
    }

    public final List<News> getNewsFromDB() {
        HashMap args = new HashMap();
        ResultSet rs = controller.find("News", args);
        List<News> newsList = new ArrayList<>();
        try {
            while (rs.next()) {
                News newsItem = new News(rs.getString(1), rs.getString(2));
                newsList.add(newsItem);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListNews.class.getName()).log(Level.SEVERE, null, ex);
        }

        return newsList;
    }

    public final List<CompanyMeeting> getMeetingsFromDB() {
        ResultSet rs = controller.find("Meetings", new HashMap());
        List<CompanyMeeting> companyMeetingsList = new ArrayList<>();
        try {
            while (rs.next()) {
                CompanyMeeting meeting = new CompanyMeeting(rs.getDate(1).toString(), rs.getString(2));
                companyMeetingsList.add(meeting);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListMeetings.class.getName()).log(Level.SEVERE, null, ex);
        }

        return companyMeetingsList;
    }

    public void registerObserver(ModelObserver observer) {
        observers.add(observer);
    }

    public void notifyObservers() {
        for (ModelObserver observer : observers) {
            observer.modelUpdated();
        }
    }

    public List<News> getNews() {
        return news;
    }

    public List<CompanyMeeting> getMeetings() {
        return companyMeetings;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public List<Advertisement> getAds() {
        return ads;
    }

    public void addCompanyMeeting(CompanyMeeting companyMeeting) {
        companyMeetings.add(companyMeeting);
        notifyObservers();
    }

    public void addText(String text, String title, Constants type) {
        switch (type) {
            case AD:
                ads.add(new Advertisement(text));
                notifyObservers();
                break;
            case ARTICLE:
                articles.add(new Article(title, text));
                notifyObservers();
                break;
            case NEWS:
                news.add(new News(title, text));
                notifyObservers();
                break;
            default:
                break;
        }
    }
}
