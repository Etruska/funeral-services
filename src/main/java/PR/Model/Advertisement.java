/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PR.Model;

/**
 *
 * @author Filip Vondrasek
 */
public class Advertisement implements Text {

    private String text;

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    public Advertisement(String text) {
        this.text = text;
    }
    
    @Override
    public String toString() {
        return this.text;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Advertisement)) {
            return false;
        }
        
        Advertisement advertisement = (Advertisement)o;
                
        return advertisement.getText().equals(this.getText());
    }
}
