/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package PR.Model;

/**
 *
 * @author Filip Vondrasek
 */
public class CompanyMeeting {
    private String date;
    private String attendees;
    
    public CompanyMeeting(String date, String attendees) {
        this.date = date;
        this.attendees = attendees;
    }
    
    public String getAttendees() {
        return attendees;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setAttendees(String attendees) {
        this.attendees = attendees;
    }
    
    public String getDate() {
        return date;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof CompanyMeeting)) {
            return false;
        }

        CompanyMeeting meeting = (CompanyMeeting) o;

        return meeting.getDate().equals(this.getDate()) && meeting.getAttendees().equals(this.getAttendees());
    }    
}
