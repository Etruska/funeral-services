/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package PR.Model;

/**
 *
 * @author Filip Vondrasek
 */
public enum Constants {
    LIST_ARTICLES, LIST_ADS, LIST_NEWS, LIST_MEETINGS, ADD_ARTICLE, ADD_NEWS, ADD_AD, ADD_MEETING, AD, NEWS, ARTICLE;
}
