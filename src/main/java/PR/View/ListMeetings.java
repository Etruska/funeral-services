/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PR.View;

import PR.Controller.IPR;
import PR.Model.CompanyMeeting;
import PR.Model.Model;
import PR.Model.ModelObserver;
import headquarters.view.View;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Filip Vondrasek
 */
public class ListMeetings extends View implements ModelObserver {

    IPR controller;
    DefaultTableModel meetingsModel;
    JTable meetingsList;
    Model model;

    @Override
    public String getViewName() {
        return "PR/ListMeetings";
    }

    public ListMeetings(IPR controller, Model model) {
        this.controller = controller;
        this.model = model;
        this.setLayout(new GridBagLayout());
        model.registerObserver(this);
        meetingsModel = new DefaultTableModel(null, new Object[]{"Date", "Attendees"});
        meetingsList = new JTable(meetingsModel);
        modelUpdated(); 
        
        JScrollPane meetingsScroll = new JScrollPane(meetingsList);

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.fill = GridBagConstraints.BOTH;
        this.add(new JLabel("Seznam meetingů."));
        c.gridy = 1;
        c.gridwidth = GridBagConstraints.REMAINDER;
        this.add(meetingsScroll, c);
    }

    @Override
    public final void modelUpdated() {
        DefaultTableModel dm = (DefaultTableModel)meetingsList.getModel();
        while (dm.getRowCount() > 0) {
            dm.removeRow(0);
        }
        for (CompanyMeeting meeting : model.getMeetings()) {          
            meetingsModel.addRow(new Object[]{meeting.getDate(), meeting.getAttendees()});
        }
    }
}
