/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PR.View;

import PR.Controller.IPR;
import PR.Model.Advertisement;
import PR.Model.Model;
import PR.Model.ModelObserver;
import headquarters.view.View;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;

/**
 *
 * @author Filip Vondrasek
 */
public class ListAds extends View implements ModelObserver {

    IPR controller;
    DefaultListModel adsModel;
    Model model;

    @Override
    public String getViewName() {
        return "PR/ListAds";
    }

    public ListAds(IPR controller, Model model) {
        this.controller = controller;
        this.setLayout(new GridBagLayout());
        this.model = model;
        model.registerObserver(this);
        
        adsModel = new DefaultListModel();

        JList adsList = new JList(adsModel);
        JScrollPane adsScroll = new JScrollPane(adsList);
        
        modelUpdated();
        
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.fill = GridBagConstraints.BOTH;
        this.add(new JLabel("Seznam reklam."));
        c.gridy = 1;
        c.gridwidth = GridBagConstraints.REMAINDER;
        this.add(adsScroll, c);
    }

    @Override
    public final void modelUpdated() {
        adsModel.clear();
        for (Advertisement ad : model.getAds()) {
            adsModel.addElement(ad);
        }
    }
}
