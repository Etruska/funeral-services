/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PR.View;

import headquarters.view.View;
import javax.swing.JLabel;

/**
 *
 * @author Filip Vondrasek
 */
public class Main extends View {

    @Override
    public String getViewName() {
        return "PR/View";
    }
    
    public Main() {
        this.add(new JLabel("PR."));
    }

}
