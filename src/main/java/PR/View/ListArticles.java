/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PR.View;

import PR.Controller.IPR;
import PR.Model.Article;
import PR.Model.Model;
import PR.Model.ModelObserver;
import headquarters.view.View;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Filip Vondrasek
 */
public class ListArticles extends View implements ModelObserver {

    IPR controller;
    JTextArea textarea;
    List<Article> articles;
    Model model;
    JComboBox articlesList;

    @Override
    public String getViewName() {
        return "PR/ListArticles";
    }

    public ListArticles(IPR controller, Model model) {
        this.controller = controller;
        this.model = model;
        this.setLayout(new GridBagLayout());
        model.registerObserver(this);

        textarea = new JTextArea(5, 15);
        textarea.setEnabled(false);
        textarea.setLineWrap(true); // wrap text so it doesn't overflow to the right
        textarea.setWrapStyleWord(true); // wrap it by words, not by single characters
        JScrollPane scrollText = new JScrollPane(textarea);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 1;
        this.add(new JLabel("Seznam článků: "), c);
        c.gridx = 1;
        c.weightx = 1.0;
        articlesList = new JComboBox();
        this.add(articlesList, c);
        c.weighty = 1.0;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = GridBagConstraints.REMAINDER;
        this.add(scrollText, c);

        modelUpdated();

        articlesList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JComboBox cb = (JComboBox) ae.getSource();
                String title = (String) cb.getSelectedItem();
                String articleText = getText(title);
                textarea.setText(articleText);
            }
        });
    }

    private String getText(String title) {
        for (Article article : model.getArticles()) {
            if (article.getTitle().equals(title)) {
                return article.getText();
            }
        }

        return null;
    }

    @Override
    public final void modelUpdated() {
        articlesList.setModel(new DefaultComboBoxModel());
        articlesList.addItem("");
        for (Article article : model.getArticles()) {
            articlesList.addItem(article.getTitle());
        }
    }
}
