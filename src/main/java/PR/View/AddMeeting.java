/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PR.View;

import PR.Controller.IPR;
import PR.Model.CompanyMeeting;
import PR.Model.Model;
import headquarters.view.View;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author Filip Vondrasek
 */
public class AddMeeting extends View {

    IPR controller;
    Model model;

    @Override
    public String getViewName() {
        return "PR/AddMeeting";
    }

    public AddMeeting(final IPR controller, Model model) {
        this.controller = controller;
        this.model = model;
        this.setLayout(new GridBagLayout());

        JButton submit = new JButton("Přidat Meeting");
        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final JXDatePicker date = new JXDatePicker(new Date());
        date.setFormats(dateFormat);
        final JTextArea attendees = new JTextArea(5, 15);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        this.add(new JLabel("Přidat Meeting."), c);
        c.gridy = 1;
        this.add(new JLabel("Datum: "), c);
        c.gridx = 1;
        this.add(date, c);
        c.gridx = 0;
        c.gridy = 2;
        this.add(new JLabel("Účastníci: "), c);
        c.gridx = 1;
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.weightx = 1.0;
        c.weighty = 1.0;
        this.add(attendees, c);
        c.gridx = 0;
        c.gridy = 3;
        c.weighty = 0.0;
        this.add(submit, c);

        submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.addCompanyMeeting(ae, new CompanyMeeting(dateFormat.format(date.getDate()), attendees.getText()));
            }
        });
    }

}
