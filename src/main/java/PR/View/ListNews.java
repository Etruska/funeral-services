/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PR.View;

import PR.Controller.IPR;
import PR.Model.Model;
import PR.Model.ModelObserver;
import PR.Model.News;
import headquarters.view.View;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Filip Vondrasek
 */
public class ListNews extends View implements ModelObserver {

    IPR controller;
    JTextArea textarea;
    Model model;
    JComboBox newsComboBox;

    @Override
    public String getViewName() {
        return "PR/ListNews";
    }

    public ListNews(IPR controller, Model model) {
        this.controller = controller;
        this.model = model;
        this.setLayout(new GridBagLayout());
        model.registerObserver(this);

        textarea = new JTextArea(5, 15);
        textarea.setEnabled(false);
        textarea.setLineWrap(true); // wrap text so it doesn't overflow to the right
        textarea.setWrapStyleWord(true); // wrap it by words, not by single characters
        JScrollPane scrollText = new JScrollPane(textarea);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        this.add(new JLabel("Seznam novinek."));
        c.gridx = 0;
        c.gridy = 1;
        this.add(new JLabel("Seznam novinek: "), c);
        c.gridx = 1;
        c.weightx = 1.0;
        newsComboBox = new JComboBox();
        newsComboBox.addItem("");
        this.add(newsComboBox, c);
        c.weighty = 1.0;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = GridBagConstraints.REMAINDER;
        this.add(scrollText, c);

        modelUpdated();
        newsComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JComboBox cb = (JComboBox) ae.getSource();
                String title = (String) cb.getSelectedItem();
                String articleText = getText(title);
                textarea.setText(articleText);
            }
        });
    }

    private String getText(String title) {
        for (News news : model.getNews()) {
            if (news.getTitle().equals(title)) {
                return news.getText();
            }
        }

        return null;
    }

    @Override
    public final void modelUpdated() {
        newsComboBox.setModel(new DefaultComboBoxModel());
        newsComboBox.addItem("");
        for (News news : model.getNews()) {
            newsComboBox.addItem(news.getTitle());
        }
        
    }
}
