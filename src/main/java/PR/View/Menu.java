/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PR.View;

import PR.Controller.IPR;
import static PR.Model.Constants.ADD_AD;
import static PR.Model.Constants.ADD_ARTICLE;
import static PR.Model.Constants.ADD_MEETING;
import static PR.Model.Constants.ADD_NEWS;
import static PR.Model.Constants.LIST_ADS;
import static PR.Model.Constants.LIST_ARTICLES;
import static PR.Model.Constants.LIST_MEETINGS;
import static PR.Model.Constants.LIST_NEWS;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 *
 * @author Filip Vondrasek
 */
public class Menu extends JMenu {

    private JMenu articlesMenu, newsMenu, adsMenu, meetingsMenu;
    private JMenuItem addArticleMenu, listArticlesMenu, addNewsMenu, listNewsMenu, addAdMenu, listAdsMenu, addMeetingMenu, listMeetingsMenu;
    private IPR prController;    
    
    public Menu(final IPR prController) {
        super("PR");
        this.prController = prController;
        articlesMenu = new JMenu("Články");
        newsMenu = new JMenu("Novinky");
        adsMenu = new JMenu("Reklamy");
        meetingsMenu = new JMenu("Meetingy");
        
        addArticleMenu = new JMenuItem("Přidat");
        listArticlesMenu = new JMenuItem("Seznam");
        
        addNewsMenu = new JMenuItem("Přidat");
        listNewsMenu = new JMenuItem("Seznam");

        addAdMenu = new JMenuItem("Přidat");
        listAdsMenu = new JMenuItem("Seznam");

        addMeetingMenu = new JMenuItem("Přidat");
        listMeetingsMenu = new JMenuItem("Seznam");
       
        
        articlesMenu.add(addArticleMenu);
        articlesMenu.add(listArticlesMenu);
        newsMenu.add(addNewsMenu);
        newsMenu.add(listNewsMenu);
        adsMenu.add(addAdMenu);
        adsMenu.add(listAdsMenu);
        meetingsMenu.add(addMeetingMenu);
        meetingsMenu.add(listMeetingsMenu);
        

        this.addArticleMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                prController.menuSelected(ae, ADD_ARTICLE);
            }
        });

        this.listArticlesMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                prController.menuSelected(ae, LIST_ARTICLES);
            }
        });

        this.addNewsMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                prController.menuSelected(ae, ADD_NEWS);
            }
        });

        this.listNewsMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                prController.menuSelected(ae, LIST_NEWS);
            }
        });

        this.addAdMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                prController.menuSelected(ae, ADD_AD);
            }
        });

        this.listAdsMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                prController.menuSelected(ae, LIST_ADS);
            }
        });

        this.addMeetingMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                prController.menuSelected(ae, ADD_MEETING);
            }
        });

        this.listMeetingsMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                prController.menuSelected(ae, LIST_MEETINGS);
            }
        });

        this.add(articlesMenu);
        this.add(newsMenu);
        this.add(adsMenu);
        this.addSeparator();
        this.add(meetingsMenu);
    }

}
