/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PR.View;

import PR.Controller.IPR;
import static PR.Model.Constants.NEWS;
import PR.Model.Model;
import headquarters.view.View;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Filip Vondrasek
 */
public class AddNews extends View {

    IPR controller;
    Model model;

    @Override
    public String getViewName() {
        return "PR/AddNews";
    }

    public AddNews(final IPR controller, Model model) {
        this.controller = controller;
        this.model = model;
        this.setLayout(new GridBagLayout());
        JButton submit = new JButton("Přidat novinku");
        final JTextField title = new JTextField();
        final JTextArea text = new JTextArea(5, 15);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        this.add(new JLabel("Přidat novinku."), c);
        c.gridy = 1;
        this.add(new JLabel("Titulek:"), c);
        c.gridx = 1;
        this.add(title, c);
        c.gridx = 0;
        c.gridy = 2;
        this.add(new JLabel("Text:"), c);
        c.gridx = 1;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.gridwidth = GridBagConstraints.REMAINDER;
        this.add(text, c);
        c.gridx = 0;
        c.gridy = 3;
        c.weighty = 0.0;
        this.add(submit, c);

        submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.addText(ae, text.getText(), title.getText(), NEWS);
            }
        });
    }
}
