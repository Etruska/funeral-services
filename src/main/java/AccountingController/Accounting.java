/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AccountingController;

import AccountingModel.Constants;
import static AccountingModel.Constants.ADD_ANNUALREPORT;
import static AccountingModel.Constants.ADD_BALANCESHEET;
import static AccountingModel.Constants.ADD_TRANSACTION;
import static AccountingModel.Constants.ANNUALREPORT;
import static AccountingModel.Constants.BALANCESHEETS;
import static AccountingModel.Constants.LIST_ANNUALREPORTS;
import static AccountingModel.Constants.LIST_BALANCESHEETS;
import static AccountingModel.Constants.LIST_TRANSACTIONS;
import static AccountingModel.Constants.TRANSACTION;
import AccountingModel.Text;
import AccountingModel.TextDAO;
import AccountingView.AddAnnualReport;
import AccountingView.AddBalanceSheet;
import AccountingView.AddTransaction;
import AccountingView.ListAnnualReports;
import AccountingView.ListBalanceSheets;
import AccountingView.ListTransactions;
import AccountingView.Main;
import AccountingView.Menu;
import headquarters.controller.IHeadquarters;
import headquarters.view.View;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JMenu;



/**
 *
 * @author Václav
 */
public class Accounting implements IAccounting {

    List<Text> texts;
    IHeadquarters hq;
    AddAnnualReport addAnnualReportView;
    ListAnnualReports listAnnualReportsView;
    AddBalanceSheet addBalanceSheetView;
    ListBalanceSheets listBalanceSheetsView;
    AddTransaction addTransactionView;
    ListTransactions listTransactionsView;

    Main mainView;
    JMenu menu;
    TextDAO textDAO;
    View contentPanel;
    
    JFrame window;

    public Accounting(IHeadquarters hq) {
        this.hq = hq;
        textDAO = new TextDAO(hq);

        this.addAnnualReportView = new AddAnnualReport(this);
        this.listAnnualReportsView = new ListAnnualReports(this);
        this.addBalanceSheetView = new AddBalanceSheet(this);
        this.listBalanceSheetsView = new ListBalanceSheets(this);
        this.addTransactionView = new AddTransaction(this);
        this.listTransactionsView = new ListTransactions(this);

        this.menu = new Menu(this);
        this.mainView = new Main();
    }

    /* replace the temporary method with this one
     public void setContentPanel(View panel) {
     hq.setContentPanel(panel);
     }
     */
    @Override
    public JMenu getMenu() {
        return menu;
    }

    @Override
    public ResultSet find(String tableName, HashMap args) {
        return textDAO.find(tableName, args);
    }

    @Override
    public void addText(ActionEvent ae, String text, String title, Constants type) {
        textDAO.addText(text, title, type);
        switch (type) {
            case TRANSACTION:
                menuSelected(new ActionEvent(this, 0, null), Constants.LIST_TRANSACTIONS);
                break;
            case ANNUALREPORT:
                menuSelected(new ActionEvent(this, 0, null), Constants.LIST_ANNUALREPORTS);
                break;
            case BALANCESHEETS:
                menuSelected(new ActionEvent(this, 0, null), Constants.LIST_BALANCESHEETS);
                break;                   
        }        
    }

    @Override
    public void menuSelected(ActionEvent ae, Constants action) {
        switch (action) {
            case ADD_ANNUALREPORT:
                setContentPanel(addAnnualReportView);
                break;
            case LIST_ANNUALREPORTS:
                listAnnualReportsView = new ListAnnualReports(this);
                setContentPanel(listAnnualReportsView);
                break;
            case ADD_BALANCESHEET:
                setContentPanel(addBalanceSheetView);
                break;
            case LIST_BALANCESHEETS:
                listBalanceSheetsView = new ListBalanceSheets(this);
                setContentPanel(listBalanceSheetsView);
                break;
            case ADD_TRANSACTION:
                setContentPanel(addTransactionView);
                break;
            case LIST_TRANSACTIONS:
                listTransactionsView = new ListTransactions(this);
                setContentPanel(listTransactionsView);
                break;
            default:
                break;
        }
    }

    public final void setContentPanel(View contentPanel) {
        this.contentPanel = contentPanel;
        hq.setContentPanel(contentPanel);
    }

    @Override
    public View getContentPanel() {
        return contentPanel;
    }
}
