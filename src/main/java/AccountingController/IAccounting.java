/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package AccountingController;

import AccountingModel.Constants;
import headquarters.controller.IController;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.util.HashMap;

/**
 *
 * @author Václav
 */
public interface IAccounting extends IController {
    ResultSet find(String tableName, HashMap args);
        
    public void addText(ActionEvent ae, String text, String title, Constants type);

    public void menuSelected(ActionEvent ae, Constants constants);
}
