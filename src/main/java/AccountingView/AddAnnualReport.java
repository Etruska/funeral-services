/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AccountingView;

import AccountingController.IAccounting;
import static AccountingModel.Constants.ANNUALREPORT;
import headquarters.view.View;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Václav
 */
public class AddAnnualReport extends View {

    IAccounting controller;

    @Override
    public String getViewName() {
        return "Accounting/AddAnnualReport";
    }

    public AddAnnualReport(final IAccounting controller) {
        this.controller = controller;
        this.setLayout(new GridBagLayout());
        
        JButton submit = new JButton("Přidat výroční zprávu");
        final JTextField title = new JTextField();
        final JTextArea text = new JTextArea(5, 15);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        this.add(new JLabel("Přidat výroční zprávu."), c);
        c.gridy = 1;
        this.add(new JLabel("Title:"), c);
        c.gridx = 1;
        this.add(title, c);
        c.gridx = 0;
        c.gridy = 2;
        this.add(new JLabel("Text:"), c);
        c.gridx = 1;
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.weightx = 1.0;
        c.weighty = 1.0;
        this.add(text, c);
        c.gridx = 0;
        c.gridy = 3;
        c.weighty = 0.0;
        this.add(submit, c);

        submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.addText(ae, text.getText(), title.getText(), ANNUALREPORT);
            }
        });
    }
}
