/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AccountingView;

import AccountingController.IAccounting;
import AccountingModel.BalanceSheets;
import headquarters.view.View;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Václav
 */
public class ListBalanceSheets extends View {

    IAccounting controller;
    JTextArea textarea;
    List<BalanceSheets> balanceSheetsList;

    @Override
    public String getViewName() {
        return "Accounting/ListBalanceSheets";
    }

    public ListBalanceSheets(IAccounting controller) {
        this.controller = controller;
        this.setLayout(new GridBagLayout());
        textarea = new JTextArea(5, 15);
        textarea.setEnabled(false);
        textarea.setLineWrap(true); 
        textarea.setWrapStyleWord(true);
        JScrollPane scrollText = new JScrollPane(textarea);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        this.add(new JLabel("Zobrazit rozvahy"));
        c.gridx = 0;
        c.gridy = 1;
        this.add(new JLabel("Seznam rozvah: "), c);
        c.gridx = 1;
        c.weightx = 1.0;
        JComboBox balanceSheetsComboBox = new JComboBox();
        balanceSheetsComboBox.addItem("");
        this.add(balanceSheetsComboBox, c);
        c.weighty = 1.0;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = GridBagConstraints.REMAINDER;
        this.add(scrollText, c);

        getBalanceSheets();
        for (BalanceSheets balanceSheets : balanceSheetsList) {
            balanceSheetsComboBox.addItem(balanceSheets.getTitle());
        }

        balanceSheetsComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JComboBox cb = (JComboBox) ae.getSource();
                String title = (String) cb.getSelectedItem();
                String annualReportText = getText(title);
                textarea.setText(annualReportText);
            }
        });
    }

    private void getBalanceSheets() {
        HashMap args = new HashMap();
      
        ResultSet rs = controller.find("BalanceSheets", args);
        balanceSheetsList = new ArrayList<>();
        try {
            while (rs.next()) {
                balanceSheetsList.add(new BalanceSheets(rs.getString(1), rs.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListAnnualReports.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String getText(String title) {
        for (BalanceSheets balanceSheets : balanceSheetsList) {
            if (balanceSheets.getTitle().equals(title)) {
                return balanceSheets.getText();
            }
        }

        return null;
    }
}
