/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AccountingView;

import AccountingController.IAccounting;
import static AccountingModel.Constants.TRANSACTION;
import headquarters.view.View;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Václav
 */
public class AddTransaction extends View {

    IAccounting controller;

    @Override
    public String getViewName() {
        return "Accounting/AddTransaction";
    }

    public AddTransaction(final IAccounting controller) {
        this.controller = controller;
        this.setLayout(new GridBagLayout());
        JButton submit = new JButton("Přidat transakci");
        final JTextField text = new JTextField();
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        
        c.gridx = 0;
        c.gridy = 0;
        this.add(new JLabel("Přidat transakci."), c);
        c.gridy = 1;
        this.add(new JLabel("Částka:"), c);
        c.weightx = 1.0;
        c.weighty = 0.0;
        c.gridx = 1;
        c.gridwidth = GridBagConstraints.REMAINDER;
        this.add(text, c);
        c.weighty = 0.0;
        c.gridx = 0;
        c.gridy = 2;
        this.add(submit, c);

        submit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                controller.addText(ae, text.getText(), null, TRANSACTION);
            }
        });
    }
}
