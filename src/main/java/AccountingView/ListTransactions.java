/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AccountingView;

import AccountingController.IAccounting;
import headquarters.view.View;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;

/**
 *
 * @author Václav
 */
public class ListTransactions extends View {

    IAccounting controller;
    DefaultListModel transactionsModel;

    @Override
    public String getViewName() {
        return "Accounting/ListTransactions";
    }

    public ListTransactions(IAccounting controller) {
        this.controller = controller;
        this.setLayout(new GridBagLayout());
        
        transactionsModel = new DefaultListModel();

        JList transactionsList = new JList(transactionsModel);
        JScrollPane transactionsScroll = new JScrollPane(transactionsList);
        
        getTransactions();
        
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.fill = GridBagConstraints.BOTH;
        this.add(new JLabel("Zobrazit transakce."));
        c.gridy = 1;
        c.gridwidth = GridBagConstraints.REMAINDER;
        this.add(transactionsScroll, c);
    }

    private void getTransactions() {
        ResultSet rs = controller.find("Transactions", new HashMap());
        try {
            while (rs.next()) {                
                transactionsModel.addElement(rs.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListTransactions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
