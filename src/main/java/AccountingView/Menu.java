/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AccountingView;

import AccountingController.IAccounting;
import static AccountingModel.Constants.ADD_ANNUALREPORT;
import static AccountingModel.Constants.ADD_BALANCESHEET;
import static AccountingModel.Constants.ADD_TRANSACTION;
import static AccountingModel.Constants.LIST_ANNUALREPORTS;
import static AccountingModel.Constants.LIST_BALANCESHEETS;
import static AccountingModel.Constants.LIST_TRANSACTIONS;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 *
 * @author Václav
 */
public class Menu extends JMenu {

    private JMenu annualReportsMenu, balanceSheetsMenu, transactionsMenu;
    private JMenuItem addAnnualReportMenu, listAnnualReportsMenu, addBalanceSheetMenu, listBalanceSheetsMenu, addTransactionMenu, listTransactionsMenu;
    private IAccounting accountingController;

    public Menu(final IAccounting accountingController) {
        super("Účtárna");
        this.accountingController = accountingController;
        annualReportsMenu = new JMenu("Výroční zprávy");
        balanceSheetsMenu = new JMenu("Rozvahy");
        transactionsMenu = new JMenu("Bankovní transakce");
    

        addAnnualReportMenu = new JMenuItem("Přidat výroční zprávu");
        listAnnualReportsMenu = new JMenuItem("Zobrazit výroční zprávy");

        addBalanceSheetMenu = new JMenuItem("Přidat rozvahu");
        listBalanceSheetsMenu = new JMenuItem("Zobrazit rozvahy");

        addTransactionMenu = new JMenuItem("Přidat bankovní transakci");
        listTransactionsMenu = new JMenuItem("Zobrazit bankovní transakce");
        

        annualReportsMenu.add(addAnnualReportMenu);
        annualReportsMenu.add(listAnnualReportsMenu);
        balanceSheetsMenu.add(addBalanceSheetMenu);
        balanceSheetsMenu.add(listBalanceSheetsMenu);
        transactionsMenu.add(addTransactionMenu);
        transactionsMenu.add(listTransactionsMenu);

        this.addAnnualReportMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                accountingController.menuSelected(ae, ADD_ANNUALREPORT);
            }
        });

        this.listAnnualReportsMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                accountingController.menuSelected(ae, LIST_ANNUALREPORTS);
            }
        });

        this.addBalanceSheetMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                accountingController.menuSelected(ae, ADD_BALANCESHEET);
            }
        });

        this.listBalanceSheetsMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                accountingController.menuSelected(ae, LIST_BALANCESHEETS);
            }
        });

        this.addTransactionMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                accountingController.menuSelected(ae, ADD_TRANSACTION);
            }
        });

        this.listTransactionsMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                accountingController.menuSelected(ae, LIST_TRANSACTIONS);
            }
        });

        this.add(annualReportsMenu);
        this.add(balanceSheetsMenu);
        this.add(transactionsMenu);        
    }

}
