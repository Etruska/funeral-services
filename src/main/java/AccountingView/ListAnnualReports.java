/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AccountingView;

import AccountingController.IAccounting;
import AccountingModel.AnnualReport;
import headquarters.view.View;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class ListAnnualReports extends View {

    IAccounting controller;
    JTextArea textarea;
    List<AnnualReport> annualReports;

    @Override
    public String getViewName() {
        return "Accounting/ListAnnualReports";
    }

    public ListAnnualReports(IAccounting controller) {
        this.controller = controller;
        this.setLayout(new GridBagLayout());
        textarea = new JTextArea(5, 15);
        textarea.setEnabled(false);
        textarea.setLineWrap(true); // 
        textarea.setWrapStyleWord(true); 
        JScrollPane scrollText = new JScrollPane(textarea);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 1;
        this.add(new JLabel("Výpis výročních zpráv: "), c);
        c.gridx = 1;
        c.weightx = 1.0;
        JComboBox annualReportsList = new JComboBox();
        annualReportsList.addItem("");
        this.add(annualReportsList, c);
        c.weighty = 1.0;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = GridBagConstraints.REMAINDER;
        this.add(scrollText, c);

        
        getAnnualReports();
        for (AnnualReport annualReport : annualReports) {
            annualReportsList.addItem(annualReport.getTitle());
        }

        annualReportsList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                JComboBox cb = (JComboBox)ae.getSource();
                String title = (String)cb.getSelectedItem();
                String annualReportText = getText(title);
                textarea.setText(annualReportText);
            }
        });
    }

    private void getAnnualReports() {
        HashMap args = new HashMap();
        //args.put("Title", "Titulek");
        ResultSet rs = controller.find("AnnualReports", args);
        annualReports = new ArrayList<>();
        try {
            while (rs.next()) {
                annualReports.add(new AnnualReport(rs.getString(1), rs.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListAnnualReports.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String getText(String title) {
        for (AnnualReport annualReport : annualReports) {
            if (annualReport.getTitle().equals(title)) {
                return annualReport.getText();
            }
        }
        
        return null;
    }
}
