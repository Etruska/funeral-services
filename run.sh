#! /bin/sh

line=`mvn clean install -q | tail -2`
tests=`echo $line | sed 's/[^0-9]*\([0-9]*\)[^0-9]*\([0-9]*\).*/\1/'`
fails=`echo $line | sed 's/[^0-9]*\([0-9]*\)[^0-9]*\([0-9]*\).*/\2/'`
coverage=`cat target/site/jacoco-ut/jacoco.csv | awk -F "," '{ sum1 += $4; sum2 += $4 + $5 } END { print (1 - sum1/sum2) * 100 "%" }'`

if [ $fails=0 ]; then
    rating="OK"
else
    rating="FAIL"
fi
echo $tests $coverage $rating
